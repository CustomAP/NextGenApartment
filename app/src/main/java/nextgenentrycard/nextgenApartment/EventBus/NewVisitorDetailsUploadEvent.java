package nextgenentrycard.nextgenApartment.EventBus;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 22/10/17.
 */

public class NewVisitorDetailsUploadEvent {
    private boolean isUploaded;
    private int visitorID;

    public NewVisitorDetailsUploadEvent(boolean isUploaded, int visitorID) {
        this.isUploaded = isUploaded;
        this.visitorID = visitorID;
    }

    public boolean isUploaded() {
        return isUploaded;
    }

    public int getVisitorID() {
        return visitorID;
    }
}
