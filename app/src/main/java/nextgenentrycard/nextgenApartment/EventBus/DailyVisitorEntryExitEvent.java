package nextgenentrycard.nextgenApartment.EventBus;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 2/3/18.
 */

public class DailyVisitorEntryExitEvent {
    private boolean isExitedEntered;

    public boolean isExitedEntered() {
        return isExitedEntered;
    }

    public DailyVisitorEntryExitEvent(boolean isExitedEntered) {

        this.isExitedEntered = isExitedEntered;
    }
}
