package nextgenentrycard.nextgenApartment.EventBus;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 21/10/17.
 */

public class ResidentsFetchEvent {
    private boolean employeesFetch;

    public boolean isEmployeesFetch() {
        return employeesFetch;
    }

    public ResidentsFetchEvent(boolean employeesFetch) {

        this.employeesFetch = employeesFetch;
    }
}
