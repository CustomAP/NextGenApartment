package nextgenentrycard.nextgenApartment.EventBus;

import nextgenentrycard.nextgenApartment.Models.ResidentItem;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 22/10/17.
 */

public class PersonToMeetSetEvent {
    private ResidentItem residentItem;

    public ResidentItem getResidentItem() {
        return residentItem;
    }

    public PersonToMeetSetEvent(ResidentItem residentItem) {

        this.residentItem = residentItem;
    }
}
