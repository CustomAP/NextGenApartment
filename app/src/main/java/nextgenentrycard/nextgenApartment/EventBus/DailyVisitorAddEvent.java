package nextgenentrycard.nextgenApartment.EventBus;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 29/10/17.
 */

public class DailyVisitorAddEvent {
    private boolean dailyVisitorAdded;
    int dailyVisitorID;

    public int getDailyVisitorID() {
        return dailyVisitorID;
    }

    public DailyVisitorAddEvent(boolean dailyVisitorAdded, int dailyVisitorID) {

        this.dailyVisitorAdded = dailyVisitorAdded;
        this.dailyVisitorID = dailyVisitorID;
    }

    public boolean isDailyVisitorAdded() {
        return dailyVisitorAdded;
    }
}
