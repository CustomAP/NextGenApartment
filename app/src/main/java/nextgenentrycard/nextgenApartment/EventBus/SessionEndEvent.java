package nextgenentrycard.nextgenApartment.EventBus;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 20/10/17.
 */

public class SessionEndEvent {
    private boolean endSession;

    public boolean isEndSession() {
        return endSession;
    }

    public SessionEndEvent(boolean endSession) {

        this.endSession = endSession;
    }
}
