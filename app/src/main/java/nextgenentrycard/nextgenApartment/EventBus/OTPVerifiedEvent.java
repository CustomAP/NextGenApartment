package nextgenentrycard.nextgenApartment.EventBus;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 18/10/17.
 */

public class OTPVerifiedEvent {
    public boolean isOTPVerified() {
        return isOTPVerified;
    }

    public OTPVerifiedEvent(boolean isOTPVerified) {

        this.isOTPVerified = isOTPVerified;
    }

    private boolean isOTPVerified;
}
