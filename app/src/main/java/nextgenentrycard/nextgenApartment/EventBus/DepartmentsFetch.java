package nextgenentrycard.nextgenApartment.EventBus;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 21/10/17.
 */

public class DepartmentsFetch {
    private boolean fetchComplete;

    public boolean isFetchComplete() {
        return fetchComplete;
    }

    public DepartmentsFetch(boolean fetchComplete) {

        this.fetchComplete = fetchComplete;
    }
}
