package nextgenentrycard.nextgenApartment.EventBus;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 18/10/17.
 */

public class OTPSentEvent {
    private boolean otpSuccess;
    private String mobNum;
    private String sessionID;
    public boolean isOtpSuccess() {
        return otpSuccess;
    }

    public String getMobNum() {
        return mobNum;
    }

    public String getSessionID() {
        return sessionID;
    }

    public OTPSentEvent(boolean otpSuccess, String mobNum, String sessionID) {

        this.otpSuccess = otpSuccess;
        this.mobNum = mobNum;
        this.sessionID = sessionID;
    }
}
