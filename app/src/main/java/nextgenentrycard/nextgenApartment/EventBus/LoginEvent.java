package nextgenentrycard.nextgenApartment.EventBus;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 19/10/17.
 */

public class LoginEvent {
    private int ID;
    private String sessionID;
    private boolean success;

    public boolean isSuccess() {
        return success;
    }

    public int getID() {
        return ID;
    }

    public String getSessionID() {
        return sessionID;
    }

    public LoginEvent(int ID, String sessionID, boolean success) {
        this.ID = ID;
        this.sessionID = sessionID;
        this.success = success;
    }
}
