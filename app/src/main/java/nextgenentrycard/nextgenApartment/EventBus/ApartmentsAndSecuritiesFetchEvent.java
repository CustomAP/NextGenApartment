package nextgenentrycard.nextgenApartment.EventBus;

import java.util.ArrayList;

import nextgenentrycard.nextgenApartment.Models.ApartmentItem;
import nextgenentrycard.nextgenApartment.Models.SecurityItem;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 19/10/17.
 */

public class ApartmentsAndSecuritiesFetchEvent {
    private boolean fetchComplete;
    private ArrayList<ApartmentItem> apartmentItems;
    private ArrayList<SecurityItem> securityItems;

    public boolean isFetchComplete() {
        return fetchComplete;
    }

    public ArrayList<ApartmentItem> getApartmentItems() {
        return apartmentItems;
    }

    public ArrayList<SecurityItem> getSecurityItems() {
        return securityItems;
    }

    public ApartmentsAndSecuritiesFetchEvent(boolean fetchComplete, ArrayList<ApartmentItem> apartmentItems, ArrayList<SecurityItem> securityItems) {

        this.fetchComplete = fetchComplete;
        this.apartmentItems = apartmentItems;
        this.securityItems = securityItems;
    }
}
