package nextgenentrycard.nextgenApartment.Login;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import de.greenrobot.event.EventBus;
import nextgenentrycard.nextgenApartment.EventBus.ApartmentsAndSecuritiesFetchEvent;
import nextgenentrycard.nextgenApartment.EventBus.LoginEvent;
import nextgenentrycard.nextgenApartment.Models.ApartmentItem;
import nextgenentrycard.nextgenApartment.Models.SecurityItem;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 19/10/17.
 */

public class LoginServerConnection {
    private AQuery aQuery;
    private Context context;
    private ArrayList<ApartmentItem> apartmentItems;
    private ArrayList<SecurityItem> securityItems;

    private String login = "http://www.accesspoint.in/index.php/Login/securityLogin";
    private String getApartmentsAndSecurities ="http://www.accesspoint.in/index.php/Login/getApartmentsAndSecurities";

    public LoginServerConnection(Context context){
        this.context=context;
        aQuery=new AQuery(context);
    }

    public void login(int apartmentID,int securityID,String password) {
        HashMap<String,Object> loginParams=new HashMap<>();
        loginParams.put("userID",apartmentID);
        loginParams.put("securityID",securityID);
        loginParams.put("password",password);

        aQuery.ajax(login,loginParams, JSONObject.class,new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                int id=0;
                String sessionID=null;

                if(object!=null){
                    Log.d("login","object!=null");
                    try {
                        id = object.getInt("ID");
                        sessionID=object.getString("sessionID");
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }else{
                    Log.d("login","object = null status:" + status.getCode());
                    Toast.makeText(context,"Failed to login try again",Toast.LENGTH_SHORT).show();
                    EventBus.getDefault().post(new LoginEvent(id,null,false));
                }

                if(status.getCode()==200){
                    if(id == 0) {
                        Toast.makeText(context, "Wrong password", Toast.LENGTH_SHORT).show();
                        EventBus.getDefault().post(new LoginEvent(id,sessionID,false));
                    }else{
                        EventBus.getDefault().post(new LoginEvent(id,sessionID,true));
                    }
                }
            }
        });
    }




    public void getApartmentsAndSecurities() {

        apartmentItems =new ArrayList<>();
        securityItems =new ArrayList<>();
        aQuery.ajax(getApartmentsAndSecurities,null,JSONObject.class,new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                if(object!=null){
                    Log.d("apartments securities","object!=null");
                    String companyName,securityName;
                    int companyID,securityID,companiesNum,securitiesNum;

                    try{
                        companiesNum=object.getInt("apartmentNum");
                        securitiesNum=object.getInt("securitiesNum");

                        String companies=object.getString("apartments");
                        String securities=object.getString("securities");

                        JSONObject companiesReader=new JSONObject(companies);
                        JSONObject securitiesReader=new JSONObject(securities);

                        for(int i=0;i<companiesNum;i++){
                            JSONObject obj=companiesReader.getJSONObject(String.valueOf(i));
                            companyName=obj.getString("apartmentName");
                            companyID=obj.getInt("apartmentID");

                            apartmentItems.add(new ApartmentItem(companyName,companyID));
                        }

                        for(int i =0;i<securitiesNum;i++){
                            JSONObject obj=securitiesReader.getJSONObject(String.valueOf(i));
                            securityName=obj.getString("securityName");
                            securityID=obj.getInt("securityID");

                            securityItems.add(new SecurityItem(securityName,securityID));
                        }
                    }catch (Exception e ){
                        e.printStackTrace();
                    }
                } else {
                    Log.d("apartments securities","object=null status: " + status.getCode());
                    EventBus.getDefault().post(new ApartmentsAndSecuritiesFetchEvent(false,null,null));
                }

                if(status.getCode()==200){
                    EventBus.getDefault().post(new ApartmentsAndSecuritiesFetchEvent(true, apartmentItems, securityItems));
                }
            }
        });
    }
}
