package nextgenentrycard.nextgenApartment.Login;

import  android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.rengwuxian.materialedittext.MaterialEditText;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.Arrays;
import java.util.List;

import de.greenrobot.event.EventBus;
import nextgenentrycard.nextgenApartment.Database.DBHelper;
import nextgenentrycard.nextgenApartment.EventBus.ApartmentsAndSecuritiesFetchEvent;
import nextgenentrycard.nextgenApartment.EventBus.ResidentsFetchEvent;
import nextgenentrycard.nextgenApartment.EventBus.LoginEvent;
import nextgenentrycard.nextgenApartment.MainActivity2;
import nextgenentrycard.nextgenApartment.Models.ApartmentItem;
import nextgenentrycard.nextgenApartment.Models.SecurityItem;
import nextgenentrycard.nextgenApartment.R;
import nextgenentrycard.nextgenApartment.Sync.Sync;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 19/10/17.
 */

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    TextView tvApartmentName, tvSecurityName;
    Spinner sApartmentName, sSecurityName;
    MaterialEditText etPassword;
    TextView bLogin;
    LoginServerConnection loginServerConnection;
    AVLoadingIndicatorView aviLogin;
    List<ApartmentItem> apartmentItems;
    List<SecurityItem> securityItems;
    String[] apartments, securities;
    String apartmentName, securityName;
    int apartmentID, securityID;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setTitle("Access Point");

        initialize();

        loginServerConnection.getApartmentsAndSecurities();
    }

    private void initialize() {
        /*
            Initialize views
         */
        tvApartmentName = (TextView) findViewById(R.id.tvCompanyName_Login);
        tvSecurityName = (TextView) findViewById(R.id.tvSecurityName_Login);
        sApartmentName = (Spinner) findViewById(R.id.sCompanyName_Login);
        sSecurityName = (Spinner) findViewById(R.id.sSecurityName_Login);
        etPassword = (MaterialEditText) findViewById(R.id.etPassword_Login);
        bLogin = (TextView) findViewById(R.id.bLogin_Login);
        aviLogin = (AVLoadingIndicatorView) findViewById(R.id.aviLogin);

        tvApartmentName.setOnClickListener(this);
        tvSecurityName.setOnClickListener(this);
        bLogin.setOnClickListener(this);

        /*
            initializing list
         */
        loginServerConnection = new LoginServerConnection(this);

        /*
            showing loading indicator to fetch company and security names
         */
        aviLogin.setVisibility(View.VISIBLE);
        aviLogin.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvCompanyName_Login:
                tvApartmentName.setVisibility(View.GONE);
                sApartmentName.setVisibility(View.VISIBLE);
                sApartmentName.performClick();
                break;
            case R.id.tvSecurityName_Login:
                tvSecurityName.setVisibility(View.GONE);
                sSecurityName.setVisibility(View.VISIBLE);
                sSecurityName.performClick();
                break;
            case R.id.bLogin_Login:
                if (tvApartmentName.getVisibility() == View.VISIBLE) {
                    /*
                        if company name not selected
                     */
                    Toast.makeText(this, "Select Apartment Name", Toast.LENGTH_SHORT).show();
                } else {

                    if (tvSecurityName.getVisibility() == View.VISIBLE) {
                        /*
                            if security name not selected
                         */
                        Toast.makeText(this, "Select Security Name", Toast.LENGTH_SHORT).show();
                    } else {

                        if (etPassword.getText().toString().equals("")) {
                            /*
                                if password not filled
                            */
                            Toast.makeText(this, "Password cannot be blank", Toast.LENGTH_SHORT).show();
                        } else {
                            aviLogin.setVisibility(View.VISIBLE);
                            aviLogin.show();
                            bLogin.setEnabled(false);
                            bLogin.setClickable(false);
                            bLogin.setFocusable(false);

                            apartmentName = sApartmentName.getSelectedItem().toString();
                            securityName = sSecurityName.getSelectedItem().toString();

                            apartmentID = getCompanyID(apartmentName);
                            securityID = getSecurityID(securityName);

                            String password = etPassword.getText().toString();
                            loginServerConnection.login(apartmentID, securityID, password);
                            break;
                        }
                    }
                }
        }
    }

    private int getCompanyID(String companyName) {
        for (int i = 0; i < apartmentItems.size(); i++) {
            if (apartmentItems.get(i).getCompanyName().equals(companyName))
                return apartmentItems.get(i).getCompanyID();
        }

        return 0;
    }

    private int getSecurityID(String securityName) {
        for (int i = 0; i < securityItems.size(); i++) {
            if (securityItems.get(i).getSecurityName().equals(securityName))
                return securityItems.get(i).getSecurityID();
        }

        return 0;
    }

    public void onEvent(LoginEvent loginEvent) {

        bLogin.setEnabled(true);
        bLogin.setClickable(true);
        bLogin.setFocusable(true);
        bLogin.setFocusableInTouchMode(true);
        aviLogin.hide();
        aviLogin.setVisibility(View.GONE);

        if (loginEvent.isSuccess()) {
        /*
            saving credentials and going to main activity
         */
            int ID = loginEvent.getID();
            String sessionID = loginEvent.getSessionID();

            SharedPreferences preferences = getSharedPreferences("LoginDetails", MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putInt("ID", ID);
            editor.putString("sessionID", sessionID);
            editor.putBoolean("isLoggedIn", true);
            editor.putString("apartmentName", apartmentName);
            editor.putString("securityName", securityName);
            editor.putInt("apartmentID", apartmentID);
            editor.putInt("securityID", securityID);
            editor.apply();

        /*
            Initializing db
         */
            DBHelper dbHelper = new DBHelper(this);

        /*
            getting departments and employees
         */
            Sync sync = new Sync(getApplicationContext());
            sync.getResidents();
        }

    }

    public void onEvent(ResidentsFetchEvent residentsFetchEvent) {
//        if (residentsFetchEvent.isEmployeesFetch()) {
            /*
                adding initial designations
             */
            SharedPreferences preferences = getSharedPreferences("Designations", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("0", "Milkman");
            editor.putString("1", "NewsPaper Boy");
            editor.putInt("num", 2);
            editor.apply();

            Log.d("onevent","employeesfetch");

            Intent i = new Intent(this, MainActivity2.class);
            startActivity(i);
//        } else {
//            aviLogin.setVisibility(View.GONE);
//            aviLogin.hide();
//            Toast.makeText(this, "Login failed Try again", Toast.LENGTH_SHORT).show();
//        }
    }

    public void onEvent(ApartmentsAndSecuritiesFetchEvent apartmentsAndSecuritiesFetchEvent) {
        if (apartmentsAndSecuritiesFetchEvent.isFetchComplete()) {
            /*
                hiding the loading indicator
             */
            aviLogin.hide();
            aviLogin.setVisibility(View.GONE);
            /*
                getting model arraylists
             */
            apartmentItems = apartmentsAndSecuritiesFetchEvent.getApartmentItems();
            securityItems = apartmentsAndSecuritiesFetchEvent.getSecurityItems();

            /*
                initializing arrays
             */
            apartments = new String[apartmentItems.size()];
            securities = new String[securityItems.size()];

            /*
                copying arraylist model strings to arrays
             */
            for (int i = 0; i < apartmentItems.size(); i++)
                apartments[i] = apartmentItems.get(i).getCompanyName();

            for (int i = 0; i < securityItems.size(); i++)
                securities[i] = securityItems.get(i).getSecurityName();

            /*
                sorting arrays
             */
            Arrays.sort(apartments);
            Arrays.sort(securities);

            /*
                creating adapters
             */
            ArrayAdapter<String> companiesAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, apartments);
            ArrayAdapter<String> securitiesAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, securities);

            /*
                setting adapters for spinners
             */
            sSecurityName.setAdapter(securitiesAdapter);
            sApartmentName.setAdapter(companiesAdapter);
        } else {
            Toast.makeText(this, "Failed to fetch Companies and Securities.\nTry again", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        SharedPreferences preferences = getSharedPreferences("LoginDetails", MODE_PRIVATE);
        if (preferences.getBoolean("isLoggedIn", false))
            finish();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
         /*
            registering eventbus
         */
        EventBus.getDefault().register(this);
    }
}
