package nextgenentrycard.nextgenApartment.PreBook;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import nextgenentrycard.nextgenApartment.Database.DBHelper;
import nextgenentrycard.nextgenApartment.Models.PreBookItem;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 20/10/17.
 */

public class PreBookFetchServerConnection {
    private AQuery aQuery;
    private Context context;

    private String fetchPreBookVisitorList = "http://www.accesspoint.in/index.php/PreBookVisitor/fetchPreBookVisitorList";
    private ArrayList<PreBookItem> preBookItems;

    public PreBookFetchServerConnection(Context context) {
        this.context = context;
        aQuery = new AQuery(context);
    }

    public void fetchPreBookVisitorList() {
        SharedPreferences preferences = context.getSharedPreferences("PreBook", Context.MODE_PRIVATE);
        int IDMax = preferences.getInt("IDMax", 0);

        SharedPreferences preferences1 = context.getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        String sessionID = preferences1.getString("sessionID", null);

        HashMap<String, Object> preBookVisitorListParams = new HashMap<>();
        preBookVisitorListParams.put("IDMax", IDMax);
        preBookVisitorListParams.put("sessionID", sessionID);

        aQuery.ajax(fetchPreBookVisitorList, preBookVisitorListParams, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                preBookItems = new ArrayList<PreBookItem>();

                int prebookID, employeeID, num = 0, visitorID;
                String visitorName, time, purposeOfMeet, organisation, employeeName, IDProof, IDNum, type, mobNum, date, flatNum, wing;

                if (object != null) {
                    try {
                        Log.d("prebooklist", "object!=null");
                        String preBookList = object.getString("PreBookList");

                        JSONObject reader = new JSONObject(preBookList);

                        num = object.getInt("num");

                        for (int i = 0; i < num; i++) {
                            JSONObject obj = reader.getJSONObject(String.valueOf(i));
                            prebookID = obj.getInt("prebookID");
                            employeeID = obj.getInt("userID");
                            visitorName = obj.getString("visitorName");
                            time = obj.getString("time");
                            purposeOfMeet = obj.getString("purposeOfMeet");
                            organisation = obj.getString("organisation");
                            employeeName = obj.getString("userName");
                            IDProof = obj.getString("IDproof");
                            IDNum = obj.getString("IDNo");
                            type = obj.getString("type");
                            mobNum = obj.getString("mobNum");
                            visitorID = obj.getInt("visitorID");
                            date = obj.getString("date");
                            flatNum = obj.getString("flatNum");
                            wing = obj.getString("wing");

                            preBookItems.add(new PreBookItem(visitorName, time, purposeOfMeet, organisation, prebookID, employeeID, employeeName, IDProof, IDNum, type, mobNum, visitorID, date, flatNum, wing));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.d("prebookList", "object=null");
                }

                if (status.getCode() == 200) {
                    DBHelper dbHelper = new DBHelper(context);

                    if (num > 0) {
                        SharedPreferences preferences = context.getSharedPreferences("PreBook", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putInt("IDMax", preBookItems.get(num - 1).getPrebookID());
                        editor.apply();
                    }

                    for (int i = 0; i < num; i++)
                        dbHelper.addPreBookVisitor(preBookItems.get(i));
                }
            }
        });
    }
}
