package nextgenentrycard.nextgenApartment.PreBook;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import nextgenentrycard.nextgenApartment.Database.DBHelper;
import nextgenentrycard.nextgenApartment.Models.PreBookItem;
import nextgenentrycard.nextgenApartment.R;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 18/10/17.
 */

public class PreBookedVisitorListFragment extends Fragment {
    RecyclerView rvPrebook;
    private ArrayList<PreBookItem> prebookItems;
    PrebookAdapter prebookAdapter;
    CardView cvNoPrebookings;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_prebooked_visitors, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rvPrebook = (RecyclerView) view.findViewById(R.id.rvPrebook);
        cvNoPrebookings=(CardView)view.findViewById(R.id.cvNoPreBookings);

        prebookItems = new ArrayList<>();

        DBHelper helper = new DBHelper(getContext());

        prebookItems = helper.getPreBookVisitors();

        if(prebookItems.size()!=0){
            cvNoPrebookings.setVisibility(View.GONE);
        }

        prebookAdapter = new PrebookAdapter(prebookItems);

        rvPrebook.setAdapter(prebookAdapter);
        rvPrebook.setItemAnimator(new DefaultItemAnimator());
        rvPrebook.setLayoutManager(new LinearLayoutManager(getContext()));

    }

//    public void onEvent(PreBookListFetchEvent preBookListFetchEvent){
//        prebookItems=preBookListFetchEvent.getPreBookItems();
//
//        prebookAdapter.update(prebookItems);
//    }
}
