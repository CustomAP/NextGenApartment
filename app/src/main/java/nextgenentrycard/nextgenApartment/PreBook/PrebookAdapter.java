package nextgenentrycard.nextgenApartment.PreBook;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import nextgenentrycard.nextgenApartment.Models.NewVisitorItem;
import nextgenentrycard.nextgenApartment.Models.PreBookItem;
import nextgenentrycard.nextgenApartment.R;
import nextgenentrycard.nextgenApartment.VisitorDetails.VisitorDetailsActivity;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 18/10/17.
 */

public class PrebookAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<PreBookItem> preBookItems;
    private Context context;
    NewVisitorItem newVisitorItem;

    public PrebookAdapter(ArrayList<PreBookItem> preBookItems){
        this.preBookItems=preBookItems;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.prebook_single_item,parent,false);
        return new PrebookHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        PrebookHolder prebookHolder = (PrebookHolder)holder;
        context=prebookHolder.llPrebook.getContext();

        /*
            setting texts
         */
        prebookHolder.tvTime.setText(preBookItems.get(position).getTime());
        prebookHolder.tvName.setText(preBookItems.get(position).getName());

        /*
            setting onclick listener
         */
        prebookHolder.llPrebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newVisitorItem = new NewVisitorItem();
                newVisitorItem.setName(preBookItems.get(position).getName());
                newVisitorItem.setPurposeOfMeet(preBookItems.get(position).getPurposeOfMeet());
                newVisitorItem.setOrganisation(preBookItems.get(position).getOrganisation());
                newVisitorItem.setResidentID(preBookItems.get(position).getUserID());
                newVisitorItem.setResidentName(preBookItems.get(position).getUserName());
                newVisitorItem.setIDImage(preBookItems.get(position).getIDNum());
                newVisitorItem.setGovtID(preBookItems.get(position).getIDProof());
                newVisitorItem.setType(preBookItems.get(position).getType());
                newVisitorItem.setMobNum(preBookItems.get(position).getMobNum());
                newVisitorItem.setVisitorID(preBookItems.get(position).getVisitorID());
                newVisitorItem.setWing(preBookItems.get(position).getWing());
                newVisitorItem.setFlatNum(preBookItems.get(position).getFlatNum());
                
                Intent i = new Intent(context,VisitorDetailsActivity.class);
                i.putExtra("origin", "PreBookVisitor");
                i.putExtra("NewVisitorItem", newVisitorItem);
                i.putExtra("prebookID", preBookItems.get(position).getPrebookID());
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return preBookItems.size();
    }


    private class PrebookHolder extends RecyclerView.ViewHolder{
        TextView tvName, tvTime;
        LinearLayout llPrebook;
        public PrebookHolder(View itemView) {
            super(itemView);
            tvName=(TextView)itemView.findViewById(R.id.tvName_PreBook);
            tvTime=(TextView)itemView.findViewById(R.id.tvTime_Prebook);
            llPrebook=(LinearLayout)itemView.findViewById(R.id.llPrebook);
        }
    }

    public void update(ArrayList<PreBookItem> preBookItems) {
        this.preBookItems.clear();
        this.preBookItems=preBookItems;
        this.notifyDataSetChanged();
    }
}
