package nextgenentrycard.nextgenApartment.PreBook;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import de.greenrobot.event.EventBus;
import nextgenentrycard.nextgenApartment.EventBus.SessionEndEvent;
import nextgenentrycard.nextgenApartment.Models.NewVisitorItem;
import nextgenentrycard.nextgenApartment.Models.PreBookItem;
import nextgenentrycard.nextgenApartment.OTP.OTPFragment;
import nextgenentrycard.nextgenApartment.R;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 19/10/17.
 */

public class PreBookOTPActivity extends AppCompatActivity {

    private static FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;

    PreBookItem preBookItem;
    NewVisitorItem newVisitorItem;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prebook_otp_fragment);
        setTitle("OTP Verification");

        EventBus.getDefault().register(this);

        /*
            getting extras
         */
        preBookItem = (PreBookItem) getIntent().getSerializableExtra("PreBook");

        /*
            copying data in new Visitor model
         */
        newVisitorItem = new NewVisitorItem();
        newVisitorItem.setName(preBookItem.getName());
        newVisitorItem.setPurposeOfMeet(preBookItem.getPurposeOfMeet());
        newVisitorItem.setOrganisation(preBookItem.getOrganisation());
        newVisitorItem.setResidentID(preBookItem.getUserID());
        newVisitorItem.setResidentName(preBookItem.getUserName());
        newVisitorItem.setIDImage(preBookItem.getIDNum());
        newVisitorItem.setGovtID(preBookItem.getIDProof());
        newVisitorItem.setType(preBookItem.getType());
        newVisitorItem.setMobNum(preBookItem.getMobNum());
        newVisitorItem.setVisitorID(preBookItem.getVisitorID());

        /*
            If prebooked for the second time directly go to hostdetails activity
         */
        if (!newVisitorItem.getMobNum().equals("")) {
        }

        /*
            setting arguments to pass
         */
        OTPFragment otpFragment = new OTPFragment();
        Bundle args = new Bundle();
        args.putSerializable("NewVisitorItem", newVisitorItem);
        args.putString("origin", "PreBookVisitor");
        args.putInt("prebookID", preBookItem.getPrebookID());
        otpFragment.setArguments(args);

        /*
            adding fragment to the activity
         */
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.prebook_container, otpFragment);
        fragmentTransaction.commit();
    }

    public void onEvent(SessionEndEvent sessionEndEvent) {
        if (sessionEndEvent.isEndSession()) {
            finish();
        }
    }
}
