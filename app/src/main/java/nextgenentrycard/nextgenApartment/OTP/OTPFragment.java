package nextgenentrycard.nextgenApartment.OTP;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.animation.AccelerateInterpolator;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.florent37.materialtextfield.MaterialTextField;

import de.greenrobot.event.EventBus;
import nextgenentrycard.nextgenApartment.DailyVisitor.AddDailyVisitorDetailsActivity;
import nextgenentrycard.nextgenApartment.EventBus.DailyVisitorAddEvent;
import nextgenentrycard.nextgenApartment.EventBus.NewVisitorDetailsUploadEvent;
import nextgenentrycard.nextgenApartment.EventBus.OTPSentEvent;
import nextgenentrycard.nextgenApartment.EventBus.OTPVerifiedEvent;
import nextgenentrycard.nextgenApartment.EventBus.SessionEndEvent;
import nextgenentrycard.nextgenApartment.Models.DSPItem;
import nextgenentrycard.nextgenApartment.Models.NewVisitorItem;
import nextgenentrycard.nextgenApartment.R;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 18/10/17.
 */

public class OTPFragment extends Fragment implements View.OnClickListener {

    EditText etMobNum, etOTP;
    ImageButton bsendOTP, bSubmit, bBack, bResend;
    String mobNum, sessionID, otpString;
    OTPServerConnection otpServerConnection;
    NewVisitorItem newVisitorItem;
    DSPItem DSPItem;
    String origin = null;
    int prebookID = 0;
    MaterialTextField mtfOTP;
    TextView tvMobileNum;


    public final static float SCALE_FACTOR = 13f;
    public final static int ANIMATION_DURATION = 300;
    public final static int MINIMUN_X_DISTANCE = 200;
    float startX;
    float startY;

    private boolean mRevealFlag;
    private float mFabSize;
    private FrameLayout flOTP;
    private LinearLayout llOTPFragment, llButtons;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_otp, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        /*
            getting passed arguments
         */
        origin = getArguments().getString("origin");

        if (origin.equals("NewVisitor") || origin.equals("PreBookVisitor")) {
            newVisitorItem = (NewVisitorItem) getArguments().getSerializable("NewVisitorItem");
        } else if (origin.equals("DailyVisitor")) {
            DSPItem = (DSPItem) getArguments().getSerializable("DSPItem");
        }

        if (origin.equals("PreBookVisitor"))
            prebookID = getArguments().getInt("prebookID");

        /*
            Initializing views
         */
        etMobNum = (EditText) view.findViewById(R.id.etMobileNum_otp);
        etOTP = (EditText) view.findViewById(R.id.etOTP_otp);
        bsendOTP = (ImageButton) view.findViewById(R.id.bSendOtp_otp);
        bSubmit = (ImageButton) view.findViewById(R.id.bSubmit_otp);
        flOTP = (FrameLayout) view.findViewById(R.id.flOTPFragment);
        llOTPFragment = (LinearLayout) view.findViewById(R.id.llOTPFragment);
        llButtons = (LinearLayout) view.findViewById(R.id.llButtons_otp);
        bBack = (ImageButton) view.findViewById(R.id.bBack_otp);
        bResend = (ImageButton) view.findViewById(R.id.bResend_otp);
        mtfOTP = (MaterialTextField)view.findViewById(R.id.mtfOTP_OTPFragment);
        tvMobileNum = (TextView)view.findViewById(R.id.tvMobileNumber_OTPFragment);

        /*
            setting on click listeners
         */
        bSubmit.setOnClickListener(this);
        bsendOTP.setOnClickListener(this);
        bBack.setOnClickListener(this);
        bResend.setOnClickListener(this);

        otpServerConnection = new OTPServerConnection(getContext());

        EventBus.getDefault().register(this);

        /*
            setting company and security name
         */
        SharedPreferences preferences = getContext().getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bSendOtp_otp:
                /*
                    sending otp
                 */
                mobNum = etMobNum.getText().toString();
                if (mobNum.length() != 10) {
                    Snackbar.make(getView(), "Mobile Number should be of 10 Characters", Snackbar.LENGTH_SHORT).show();
                } else {
                    tvMobileNum.setText(mobNum);
                    onOTPSendPressed();
                    otpServerConnection.sendOTP(mobNum);
                }
                break;

            case R.id.bSubmit_otp:
                /*
                    Verify if the otp matches
                 */
                otpString = etOTP.getText().toString();
                if (mobNum != null) {
                    if (otpString.length() == 0) {
                        Snackbar.make(getView(), "Please Enter OTP", Snackbar.LENGTH_SHORT).show();
                    } else {
                        Snackbar.make(getView(), "Verifying OTP...", Snackbar.LENGTH_LONG).show();
                        otpServerConnection.verifyOTP(sessionID, otpString);
                    }
                } else {
                    Snackbar.make(getView(), "Verify Mobile Number first", Snackbar.LENGTH_SHORT).show();
                }

                break;
            case R.id.bBack_otp:
                etMobNum.setText("");
                sessionID = null;
                otpString = null;
                mobNum = null;
                onBackPressed();
                break;
            case R.id.bResend_otp:
                otpServerConnection.sendOTP(mobNum);
                Snackbar.make(getView(),"Sending OTP again to "+mobNum,Snackbar.LENGTH_SHORT).show();
                break;
        }
    }

    public void onEvent(OTPSentEvent otpSentEvent) {
        boolean otpSuccess = otpSentEvent.isOtpSuccess();
        if (otpSuccess) {
            sessionID = otpSentEvent.getSessionID();
            mobNum = otpSentEvent.getMobNum();
            Log.d("otp sent", "success");
            Log.d("mobNum", mobNum);
            Log.d("sesssionID", sessionID);
            Snackbar.make(getView(), "OTP Sent", Snackbar.LENGTH_SHORT).show();
        } else {
            Snackbar.make(getView(), "Failed to send OTP", Snackbar.LENGTH_SHORT).show();
        }
    }

    public void onEvent(OTPVerifiedEvent otpVerifiedEvent) {
        boolean isOTPVerified = otpVerifiedEvent.isOTPVerified();
//
        if (isOTPVerified) {
            if (origin.equals("NewVisitor") || origin.equals("PreBookVisitor")) {

            } else if (origin.equals("DailyVisitor")) {
                DSPItem.setMobileNum(mobNum);

                Intent i = new Intent(getContext(), AddDailyVisitorDetailsActivity.class);
                i.putExtra("DSPItem", DSPItem);
                startActivity(i);
            }
        } else {
            Snackbar.make(getView(), "OTP verification failed", Snackbar.LENGTH_SHORT).show();
        }
    }


    public void onEvent(SessionEndEvent sessionEndEvent) {
        if (sessionEndEvent.isEndSession()) {
            mobNum = null;
            etOTP.setText("");
            etMobNum.setText("");
            tvMobileNum.setText("");
            sessionID = null;
            otpString = null;

            newVisitorItem = new NewVisitorItem();
            onBackPressed();
        }
    }

    public void onEvent(NewVisitorDetailsUploadEvent newVisitorDetailsUploadEvent) {
        if (newVisitorDetailsUploadEvent.isUploaded()) {
            mobNum = null;
            etOTP.setText("");
            etMobNum.setText("");
            tvMobileNum.setText("");
            sessionID = null;
            otpString = null;

            newVisitorItem = new NewVisitorItem();
        }
    }

    public void onEvent(DailyVisitorAddEvent dailyVisitorAddEvent) {
        if (dailyVisitorAddEvent.isDailyVisitorAdded()) {
            mobNum = null;
            etOTP.setText("");
            etMobNum.setText("");
            sessionID = null;
            otpString = null;

            DSPItem = new DSPItem();
        }
    }


    public void onOTPSendPressed() {
        bsendOTP.setImageResource(0);
        mtfOTP.setVisibility(View.VISIBLE);
        llButtons.setVisibility(View.VISIBLE);

        startX = bsendOTP.getX();
        startY = bsendOTP.getY();

        AnimatorPath path = new AnimatorPath();
        path.moveTo(0, 0);
        path.curveTo(-200, 200, -400, 100, -600, 50);

        final ObjectAnimator anim = ObjectAnimator.ofObject(this, "fabLoc",
                new PathEvaluator(), path.getPoints().toArray());

        anim.setInterpolator(new AccelerateInterpolator());
        anim.setDuration(ANIMATION_DURATION);
        anim.start();

        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                if (Math.abs(startX - bsendOTP.getX()) > MINIMUN_X_DISTANCE) {
                    if (!mRevealFlag) {
                        flOTP.setY(flOTP.getY() + mFabSize / 2);

                        bsendOTP.animate()
                                .scaleXBy(SCALE_FACTOR)
                                .scaleYBy(SCALE_FACTOR)
                                .setListener(mEndRevealListener)
                                .setDuration(ANIMATION_DURATION);

                        mRevealFlag = true;
                    }
                }
            }
        });

        etMobNum.setVisibility(View.GONE);
    }

    private AnimatorListenerAdapter mEndRevealListener = new AnimatorListenerAdapter() {

        @Override
        public void onAnimationEnd(Animator animation) {
            super.onAnimationEnd(animation);

            bsendOTP.setVisibility(View.INVISIBLE);

            flOTP.setBackgroundColor(getResources()
                    .getColor(R.color.colorAccent));

            for (int i = 0; i < llOTPFragment.getChildCount(); i++) {
                View v = llOTPFragment.getChildAt(i);
                ViewPropertyAnimator animator = v.animate()
                        .scaleX(1).scaleY(1)
                        .setDuration(ANIMATION_DURATION);

                animator.setStartDelay(i * 50);
                animator.start();
            }

            for (int i = 0; i < llButtons.getChildCount(); i++) {
                View v = llButtons.getChildAt(i);
                ViewPropertyAnimator animator = v.animate()
                        .scaleX(1).scaleY(1)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                llButtons.setVisibility(View.VISIBLE);
                            }
                        })
                        .setDuration(ANIMATION_DURATION);

                animator.setStartDelay(i * 50);
                animator.start();
            }
        }
    };

    /**
     * We need this setter to translate between the information the animator
     * produces (a new "PathPoint" describing the current animated location)
     * and the information that the button requires (an xy location). The
     * setter will be called by the ObjectAnimator given the 'fabLoc'
     * property string.
     */
    public void setFabLoc(PathPoint newLoc) {
        bsendOTP.setTranslationX(newLoc.mX);

        if (mRevealFlag)
            bsendOTP.setTranslationY(newLoc.mY - (mFabSize / 2));
        else
            bsendOTP.setTranslationY(newLoc.mY);
    }

    public static OTPFragment newInstance() {
        return new OTPFragment();
    }

    public void onBackPressed() {
        bsendOTP.setVisibility(View.VISIBLE);
        etMobNum.setVisibility(View.VISIBLE);


        bsendOTP.setImageResource(R.drawable.send);

        bsendOTP.setScaleY(1);
        bsendOTP.setScaleX(1);

        bsendOTP.setY(startY);
        bsendOTP.setX(startX);

        flOTP.setBackgroundColor(getResources()
                .getColor(android.R.color.transparent));

        for (int i = 0; i < llOTPFragment.getChildCount(); i++) {
            View v = llOTPFragment.getChildAt(i);
            ViewPropertyAnimator animator = v.animate()
                    .scaleX(0).scaleY(0)
                    .setDuration(ANIMATION_DURATION);

            animator.setStartDelay(i * 50);
            animator.start();
        }

        for (int i = 0; i < llButtons.getChildCount(); i++) {
            View v = llButtons.getChildAt(i);
            ViewPropertyAnimator animator = v.animate()
                    .scaleX(0).scaleY(0)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            llButtons.setVisibility(View.GONE);
                        }
                    })
                    .setDuration(ANIMATION_DURATION);

            animator.setStartDelay(i * 50);
            animator.start();
        }


        mtfOTP.setVisibility(View.GONE);


        mRevealFlag = false;
    }
}
