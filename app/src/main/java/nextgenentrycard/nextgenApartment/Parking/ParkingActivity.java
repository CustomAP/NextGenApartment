package nextgenentrycard.nextgenApartment.Parking;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import nextgenentrycard.nextgenApartment.R;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 18/2/18.
 */

public class ParkingActivity extends AppCompatActivity {
    TextView tvCounter, tvTotal;
    FloatingActionButton fabMinus, fabPlus;
    LinearLayout llParking;

    AQuery aQuery;

    int count, total;
    String updateParking = "http://www.accesspoint.in/index.php/Parking/updateCount";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parking);
        setTitle("Parking");

        initialize();
    }

    private void initialize() {
        tvCounter = (TextView) findViewById(R.id.tvCounter_parkingActivity);
        tvTotal = (TextView) findViewById(R.id.tvTotal_parkingActivity);
        fabMinus = (FloatingActionButton) findViewById(R.id.fabMinus_parkingActivity);
        fabPlus = (FloatingActionButton) findViewById(R.id.fabPlus_parkingActivity);
        llParking = (LinearLayout) findViewById(R.id.llParkingActivity);

        aQuery = new AQuery(getApplicationContext());

        SharedPreferences preferences = getSharedPreferences("Parking", Context.MODE_PRIVATE);
        total = preferences.getInt("total", 10);
        count = preferences.getInt("count", 0);

        tvTotal.setText("/" + total);
        tvCounter.setText(String.valueOf(count));

        fabPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (count < total)
                    updateCounter(++count);
                else
                    Snackbar.make(llParking, "Parking already full", Snackbar.LENGTH_SHORT).show();
            }
        });

        fabMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (count > 0)
                    updateCounter(--count);
                else
                    Snackbar.make(llParking, "Parking already empty", Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    private void updateCounter(final int count) {
        Snackbar.make(llParking, "Updating Counter", Snackbar.LENGTH_SHORT).show();

        final SharedPreferences preferences = getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        String sessionID = preferences.getString("sessionID", null);

        HashMap<String, Object> parkingParams = new HashMap<>();
        parkingParams.put("sessionID", sessionID);
        parkingParams.put("count", count);

        aQuery.ajax(updateParking, parkingParams, JSONObject.class, new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {

                int result = 0;
                if(object != null){
                    try {
                        result = object.getInt("result");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    Log.d("status",status.getCode() + " " + status.getError());
                    Snackbar.make(llParking, "Failed to update Counter", Snackbar.LENGTH_SHORT).show();
                }

                if(status.getCode() == 200){
                    if(result == 0){
                        tvCounter.setText(String.valueOf(count));

                        SharedPreferences sharedPreferences = getSharedPreferences("Parking", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putInt("count", count);
                        editor.apply();
                    }
                }
            }
        });
    }
}
