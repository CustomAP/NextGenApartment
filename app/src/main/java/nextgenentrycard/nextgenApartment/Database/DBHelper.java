package nextgenentrycard.nextgenApartment.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

import nextgenentrycard.nextgenApartment.Models.DSPItem;
import nextgenentrycard.nextgenApartment.Models.DailyVisitorEntryItem;
import nextgenentrycard.nextgenApartment.Models.ResidentItem;
import nextgenentrycard.nextgenApartment.Models.NewVisitorItem;
import nextgenentrycard.nextgenApartment.Models.PreBookItem;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 19/10/17.
 */

public class DBHelper extends SQLiteOpenHelper {

    private static final String DBName = "Visitors";
    private static final int DBVersion = 1;

    private static final String newVisitorsTable = "NewVisitors";
    private static final String prebookTable = "PreBook";
    private static final String residentsTable = "Residents";
    private static final String dailyVisitorTable = "DailyVisitors";
    private static final String dailyVisitorEntryTable = "DailyVisitorsEntry";

    private static final String column_prebookID = "prebookID";
    private static final String column_visitorID = "visitorID";
    private static final String column_inTime = "inTime";
    private static final String column_outTime = "outTime";
    private static final String column_purposeOfMeet = "purposeOfMeet";
    private static final String column_IDProof = "IDProof";
    private static final String column_IDNum = "IDNum";
    private static final String column_residentName = "residentName";
    private static final String column_visitorName = "visitorName";
    private static final String column_prebookTime = "prebookTime";
    private static final String column_organisation = "organisation";
    private static final String column_type = "type";
    private static final String column_residentID = "residentID";
    private static final String column_mobNum = "mobileNum";
    private static final String column_date = "date";
    private static final String column_dailyVisitorID = "dailyVisitorID";
    private static final String column_designation = "designation";
    private static final String column_dailyVisitorEntryID = "dailyVisitorEntryID";
    private static final String column_flatNum = "flatNum";
    private static final String column_wing = "wing";
    private static final String column_address = "address";
    private static final String column_spouseName = "spouseName";
    private static final String column_spouseOccupation = "spouseOccupation";
    private static final String column_familyMembers = "familyMembers";

    private Context context;

    public DBHelper(Context context) {
        super(context, DBName, null, DBVersion);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createNewVisitorTable = "CREATE TABLE " + newVisitorsTable + " ("
                + column_visitorID + " INTEGER PRIMARY KEY, "
                + column_visitorName + " VARCHAR(255), "
                + column_inTime + " TIME, "
                + column_outTime + " TIME, "
                + column_purposeOfMeet + " VARCHAR(255), "
                + column_IDProof + " VARCHAR(255), "
                + column_IDNum + " VARCHAR(255), "
                + column_organisation + " VARCHAR(255), "
                + column_type + " VARCHAR(255), "
                + column_residentName + " VARCHAR(255), "
                + column_residentID + " INT, "
                + column_mobNum + " VARCHAR(255), "
                + column_flatNum + " VARCHAR(255), "
                + column_wing + " VARCHAR(255), "
                + column_date + " VARCHAR(255));";

        String createPreBookTable = "CREATE TABLE " + prebookTable + " ("
                + column_prebookID + " INTEGER PRIMARY KEY, "
                + column_visitorName + " VARCHAR(255), "
                + column_prebookTime + " TIME, "
                + column_purposeOfMeet + " VARCHAR(255), "
                + column_organisation + " VARCHAR(255), "
                + column_residentName + " VARCHAR(255), "
                + column_residentID + " INT, "
                + column_IDProof + " VARCHAR(255), "
                + column_IDNum + " VARCHAR(255), "
                + column_type + " VARCHAR(255), "
                + column_mobNum + " VARCHAR(255), "
                + column_visitorID + " INT, "
                + column_flatNum + " VARCHAR(255), "
                + column_wing + " VARCHAR(255), "
                + column_date + " VARCHAR(255));";

        String createEmployeeTable = "CREATE TABLE " + residentsTable + " ("
                + column_residentID + " INTEGER PRIMARY KEY, "
                + column_wing + " VARCHAR(255), "
                + column_flatNum + " VARCHAR(255), "
                + column_residentName + " VARCHAR(255));";

        String createDailyVisitorTable = "CREATE TABLE " + dailyVisitorTable + " ("
                + column_dailyVisitorID + " INTEGER PRIMARY KEY, "
                + column_visitorName + " VARCHAR(255), "
                + column_IDProof + " VARCHAR(255), "
                + column_IDNum + " VARCHAR(255), "
                + column_mobNum + " VARCHAR(255), "
                + column_address + " TEXT, "
                + column_spouseName + " VARCHAR(255), "
                + column_spouseOccupation + " VARCHAR(255), "
                + column_familyMembers + " TEXT, "
                + column_organisation + " VARCHAR(255), "
                + column_designation + " VARCHAR(255));";

        String createDailyVisitorEntryTable = "CREATE TABLE " + dailyVisitorEntryTable + " ("
                + column_dailyVisitorEntryID + " INTEGER PRIMARY KEY, "
                + column_dailyVisitorID + " INT, "
                + column_date + " VARCHAR(255), "
                + column_inTime + " TIME, "
                + column_outTime + " TIME);";

        try {
            db.execSQL(createNewVisitorTable);
            db.execSQL(createPreBookTable);
            db.execSQL(createEmployeeTable);
            db.execSQL(createDailyVisitorTable);
            db.execSQL(createDailyVisitorEntryTable);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String dropNewVisitorTable = "DROP TABLE IF EXISTS " + newVisitorsTable;
        String dropPrebookTable = "DROP TABLE IF EXISTS " + prebookTable;

        try {
            db.execSQL(dropNewVisitorTable);
            db.execSQL(dropPrebookTable);
            onCreate(db);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addPreBookVisitor(PreBookItem preBookItem) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(column_prebookID, preBookItem.getPrebookID());
        contentValues.put(column_visitorName, preBookItem.getName());
        contentValues.put(column_prebookTime, preBookItem.getTime());
        contentValues.put(column_purposeOfMeet, preBookItem.getPurposeOfMeet());
        contentValues.put(column_organisation, preBookItem.getOrganisation());
        contentValues.put(column_residentName, preBookItem.getUserName());
        contentValues.put(column_residentID, preBookItem.getUserID());
        contentValues.put(column_IDProof, preBookItem.getIDProof());
        contentValues.put(column_IDNum, preBookItem.getIDNum());
        contentValues.put(column_type, preBookItem.getType());
        contentValues.put(column_mobNum, preBookItem.getMobNum());
        contentValues.put(column_visitorID, preBookItem.getVisitorID());
        contentValues.put(column_date, preBookItem.getDate());
        contentValues.put(column_wing, preBookItem.getWing());
        contentValues.put(column_flatNum, preBookItem.getFlatNum());

        try {
            sqLiteDatabase.insert(prebookTable, null, contentValues);
        } catch (Exception e) {
            e.printStackTrace();
        }

        sqLiteDatabase.close();
    }

    public ArrayList<PreBookItem> getPreBookVisitors() {
        ArrayList<PreBookItem> preBookItems = new ArrayList<>();

        SQLiteDatabase database = this.getWritableDatabase();

        String selectQuery = "SELECT * FROM " + prebookTable;

        Cursor cursor = database.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                preBookItems.add(new PreBookItem(cursor.getString(cursor.getColumnIndex(column_visitorName)),
                        cursor.getString(cursor.getColumnIndex(column_prebookTime)),
                        cursor.getString(cursor.getColumnIndex(column_purposeOfMeet)),
                        cursor.getString(cursor.getColumnIndex(column_organisation)),
                        cursor.getInt(cursor.getColumnIndex(column_prebookID)),
                        cursor.getInt(cursor.getColumnIndex(column_residentID)),
                        cursor.getString(cursor.getColumnIndex(column_residentName)),
                        cursor.getString(cursor.getColumnIndex(column_IDProof)),
                        cursor.getString(cursor.getColumnIndex(column_IDNum)),
                        cursor.getString(cursor.getColumnIndex(column_type)),
                        cursor.getString(cursor.getColumnIndex(column_mobNum)),
                        cursor.getInt(cursor.getColumnIndex(column_visitorID)),
                        cursor.getString(cursor.getColumnIndex(column_date)),
                        cursor.getString(cursor.getColumnIndex(column_flatNum)),
                        cursor.getString(cursor.getColumnIndex(column_wing))
                ));
            } while (cursor.moveToNext());
        }

        cursor.close();
        ;
        database.close();

        return preBookItems;
    }

    public void addEmployee(ResidentItem residentItem) {
        SQLiteDatabase database = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(column_residentID, residentItem.getResidentID());
        contentValues.put(column_residentName, residentItem.getResidentName());
        contentValues.put(column_flatNum, residentItem.getFlatNum());
        contentValues.put(column_wing, residentItem.getWing());

        try {
            database.insert(residentsTable, null, contentValues);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<ResidentItem> getEmployees(String name) {
        ArrayList<ResidentItem> residentItems = new ArrayList<>();

        SQLiteDatabase database = this.getWritableDatabase();

        String selectQuery = "SELECT * FROM " + residentsTable + " WHERE " + column_residentName + " LIKE ? ORDER BY LENGTH(" + column_residentName + ") LIMIT 0,10";
        String[] args = {name + "%"};

        Cursor cursor = database.rawQuery(selectQuery, args);

        if (cursor.moveToFirst()) {
            do {
                residentItems.add(new ResidentItem(cursor.getInt(cursor.getColumnIndex(column_residentID)),
                        cursor.getString(cursor.getColumnIndex(column_residentName)),
                        cursor.getString(cursor.getColumnIndex(column_wing)),
                        cursor.getString(cursor.getColumnIndex(column_flatNum))
                ));
            } while (cursor.moveToNext());
        }

        database.close();
        cursor.close();

        return residentItems;
    }

public ArrayList<ResidentItem> getAllEmployees() {
        ArrayList<ResidentItem> residentItems = new ArrayList<>();

        SQLiteDatabase database = this.getWritableDatabase();

        String selectQuery = "SELECT * FROM " + residentsTable;

        Cursor cursor = database.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                residentItems.add(new ResidentItem(cursor.getInt(cursor.getColumnIndex(column_residentID)),
                        cursor.getString(cursor.getColumnIndex(column_residentName)),
                        cursor.getString(cursor.getColumnIndex(column_wing)),
                        cursor.getString(cursor.getColumnIndex(column_flatNum))
                ));
            } while (cursor.moveToNext());
        }

        database.close();
        cursor.close();

        return residentItems;
    }

    public void addVisitor(NewVisitorItem newVisitorItem) {
        SQLiteDatabase database = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        contentValues.put(column_visitorID, newVisitorItem.getVisitorID());
        contentValues.put(column_visitorName, newVisitorItem.getName());
        contentValues.put(column_inTime, newVisitorItem.getInTime());
        contentValues.put(column_purposeOfMeet, newVisitorItem.getPurposeOfMeet());
        contentValues.put(column_IDProof, newVisitorItem.getGovtID());
        Log.d("db idNum",newVisitorItem.getIDNum());
        contentValues.put(column_IDNum, newVisitorItem.getIDNum());
        contentValues.put(column_organisation, newVisitorItem.getOrganisation());
        contentValues.put(column_type, newVisitorItem.getType());
        contentValues.put(column_residentName, newVisitorItem.getResidentName());
        contentValues.put(column_residentID, newVisitorItem.getResidentID());
        contentValues.put(column_flatNum, newVisitorItem.getFlatNum());
        contentValues.put(column_wing, newVisitorItem.getWing());
        contentValues.put(column_mobNum, newVisitorItem.getMobNum());
        Log.d("date db", newVisitorItem.getDate());
        contentValues.put(column_date, newVisitorItem.getDate());

        try {
            database.insert(newVisitorsTable, null, contentValues);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<NewVisitorItem> getAllVisitors() {
        ArrayList<NewVisitorItem> newVisitorItems = new ArrayList<>();

        SQLiteDatabase database = this.getWritableDatabase();

        String selectQuery = "SELECT * FROM " + newVisitorsTable;

        Cursor cursor = database.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                newVisitorItems.add(new NewVisitorItem(cursor.getString(cursor.getColumnIndex(column_mobNum)),
                        cursor.getString(cursor.getColumnIndex(column_visitorName)),
                        cursor.getString(cursor.getColumnIndex(column_purposeOfMeet)),
                        cursor.getString(cursor.getColumnIndex(column_organisation)),
                        cursor.getString(cursor.getColumnIndex(column_IDProof)),
                        cursor.getString(cursor.getColumnIndex(column_IDNum)),
                        cursor.getString(cursor.getColumnIndex(column_type)),
                        cursor.getString(cursor.getColumnIndex(column_residentName)),
                        cursor.getString(cursor.getColumnIndex(column_inTime)),
                        cursor.getString(cursor.getColumnIndex(column_outTime)),
                        cursor.getInt(cursor.getColumnIndex(column_residentID)),
                        cursor.getInt(cursor.getColumnIndex(column_visitorID)),
                        cursor.getString(cursor.getColumnIndex(column_flatNum)),
                        cursor.getString(cursor.getColumnIndex(column_wing))
                ));
            } while (cursor.moveToNext());
        }

        cursor.close();
        database.close();

        return newVisitorItems;
    }

    public void setOutTime(int visitorID, String outTime) {
        SQLiteDatabase database = this.getWritableDatabase();

        String updateQuery = "UPDATE " + newVisitorsTable + " SET " + column_outTime + " =? WHERE " + column_visitorID + " = " + visitorID;

        String[] args = {outTime};

        try {
            database.execSQL(updateQuery, args);
        } catch (Exception e) {
            e.printStackTrace();
        }

        database.close();
    }

    public void deletePreBookVisitor(int prebookID) {
        SQLiteDatabase database = this.getWritableDatabase();

        String deleteQuery = "DELETE FROM " + prebookTable + " WHERE " + column_prebookID + " = " + prebookID;

        try {
            database.execSQL(deleteQuery);
        } catch (Exception e) {
            e.printStackTrace();
        }

        database.close();
    }

    public void deleteOldGuests(String date) {
        SQLiteDatabase database = this.getWritableDatabase();

        String selectQuery = "select * from " + newVisitorsTable + " where " + column_date + " != ?";

        Cursor cursor = database.rawQuery(selectQuery, new String[]{date});

        if (cursor.moveToFirst())
            Log.d("asdfas", cursor.getInt(cursor.getColumnIndex(column_visitorID)) + " " + cursor.getString(cursor.getColumnIndex(column_date)));

        cursor.close();

        String deleteOldVisitorsQuery = "DELETE FROM " + newVisitorsTable + " WHERE " + column_date + " != ?";
        String deleteUnvisitedQuery = "DELETE FROM " + prebookTable + " WHERE " + column_date + " != ?";

        database.execSQL(deleteOldVisitorsQuery, new String[]{date});
        database.execSQL(deleteUnvisitedQuery, new String[]{date});

        database.close();
    }

    public void addDailyVisitor(DSPItem DSPItem) {
        SQLiteDatabase database = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        contentValues.put(column_dailyVisitorID, DSPItem.getDspID());
        contentValues.put(column_visitorName, DSPItem.getName());
        contentValues.put(column_IDProof, DSPItem.getIDProof());
        contentValues.put(column_IDNum, DSPItem.getIDNum());
        contentValues.put(column_mobNum, DSPItem.getMobileNum());
        contentValues.put(column_address, DSPItem.getAddress());
        contentValues.put(column_spouseName, DSPItem.getSpouseName());
        contentValues.put(column_spouseOccupation, DSPItem.getSpouseOccupation());
        contentValues.put(column_familyMembers, DSPItem.getFamilyMembers());

        try {
            database.insert(dailyVisitorTable, null, contentValues);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addDailyVisitorEntry(DailyVisitorEntryItem dailyVisitorEntryItem) {
        SQLiteDatabase database = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(column_date, dailyVisitorEntryItem.getDate());
        contentValues.put(column_dailyVisitorEntryID, dailyVisitorEntryItem.getDailyVisitorEntryID());
        contentValues.put(column_dailyVisitorID, dailyVisitorEntryItem.getDailyVisitorID());
        contentValues.put(column_inTime, dailyVisitorEntryItem.getInTime());

        if (dailyVisitorEntryItem.getOutTime() != null)
            if (!dailyVisitorEntryItem.getOutTime().equals("null"))
                contentValues.put(column_outTime, dailyVisitorEntryItem.getOutTime());

        try {
            int id = (int) database.insertWithOnConflict(dailyVisitorEntryTable, null, contentValues, SQLiteDatabase.CONFLICT_IGNORE);
            if (id == -1)
                database.update(dailyVisitorEntryTable, contentValues, column_dailyVisitorEntryID + " = " + dailyVisitorEntryItem.getDailyVisitorEntryID(), null);
        } catch (Exception e) {
            e.printStackTrace();
        }

        database.close();
    }

    public ArrayList<DSPItem> getAllDailyVisitors() {
        ArrayList<DSPItem> DSPItems = new ArrayList<>();

        SQLiteDatabase database = this.getWritableDatabase();

        String selectQuery = "SELECT * FROM " + dailyVisitorTable;

        Cursor cursor = database.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                DSPItems.add(new DSPItem(
                        cursor.getInt(cursor.getColumnIndex(column_dailyVisitorID)),
                        cursor.getString(cursor.getColumnIndex(column_visitorName)),
                        cursor.getString(cursor.getColumnIndex(column_IDProof)),
                        cursor.getString(cursor.getColumnIndex(column_IDNum)),
                        cursor.getString(cursor.getColumnIndex(column_mobNum)),
                        cursor.getString(cursor.getColumnIndex(column_address)),
                        cursor.getString(cursor.getColumnIndex(column_spouseName)),
                        cursor.getString(cursor.getColumnIndex(column_spouseOccupation)),
                        cursor.getString(cursor.getColumnIndex(column_familyMembers))
                ));
            } while (cursor.moveToNext());
        }

        cursor.close();
        database.close();

        return DSPItems;
    }

    public int getDailyVisitorEntryID(int dailyVisitorID) {
        SQLiteDatabase database = this.getWritableDatabase();

        int dailyVisitorEntryID = 0;

        String selectQuery = "SELECT " + column_dailyVisitorEntryID + " FROM " + dailyVisitorEntryTable + " WHERE " + column_dailyVisitorID + " = " + dailyVisitorID + " AND " + column_outTime + " IS NULL";

        Cursor cursor = database.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            dailyVisitorEntryID = cursor.getInt(cursor.getColumnIndex(column_dailyVisitorEntryID));
        }

        Log.d("dailyVisitorEntryID", dailyVisitorEntryID + "");

        cursor.close();
        database.close();

        return dailyVisitorEntryID;
    }

    public void outDailyVisitor(int dailyVisitorEntryID, String time) {
        SQLiteDatabase database = this.getWritableDatabase();

        String updateQuery = "UPDATE " + dailyVisitorEntryTable + " SET " + column_outTime + " = ? WHERE " + column_dailyVisitorEntryID + " = " + dailyVisitorEntryID;

        String[] args = {time};

        database.execSQL(updateQuery, args);

        database.close();
    }


    public ArrayList<DailyVisitorEntryItem> getDailyVisitorsForToday(String date) {
        ArrayList<DailyVisitorEntryItem> dailyVisitorEntryItems = new ArrayList<>();

        SQLiteDatabase database = this.getReadableDatabase();

        String selectQuery = "SELECT " + dailyVisitorEntryTable + ".* , " + dailyVisitorTable + "." + column_visitorName + " FROM " + dailyVisitorEntryTable + " INNER JOIN " + dailyVisitorTable + " ON " + dailyVisitorEntryTable + "." + column_dailyVisitorID + " = " + dailyVisitorTable + "." + column_dailyVisitorID + " WHERE " + column_date + " =  ? ";

        String[] args = {date};

        Cursor cursor = database.rawQuery(selectQuery, args);

        if (cursor.moveToFirst()) {
            do {
                dailyVisitorEntryItems.add(new DailyVisitorEntryItem(cursor.getString(cursor.getColumnIndex(column_visitorName)), cursor.getInt(cursor.getColumnIndex(column_dailyVisitorID)), cursor.getInt(cursor.getColumnIndex(column_dailyVisitorEntryID)), cursor.getString(cursor.getColumnIndex(column_inTime)), cursor.getString(cursor.getColumnIndex(column_outTime)), cursor.getString(cursor.getColumnIndex(column_date))));
            } while (cursor.moveToNext());
        }

        cursor.close();
        database.close();

        return dailyVisitorEntryItems;
    }

    public DSPItem getDailyVisitor(int dailyVisitorID) {
        SQLiteDatabase database = this.getWritableDatabase();

        String selectQuery = "SELECT * FROM " + dailyVisitorTable + " WHERE " + column_dailyVisitorID + " = " + dailyVisitorID;


        Cursor cursor = database.rawQuery(selectQuery, null);

        DSPItem DSPItem = new DSPItem();

        if (cursor.moveToFirst()) {
            DSPItem =
                    new DSPItem(
                            cursor.getInt(cursor.getColumnIndex(column_dailyVisitorID)),
                            cursor.getString(cursor.getColumnIndex(column_visitorName)),
                            cursor.getString(cursor.getColumnIndex(column_IDProof)),
                            cursor.getString(cursor.getColumnIndex(column_IDNum)),
                            cursor.getString(cursor.getColumnIndex(column_mobNum)),
                            cursor.getString(cursor.getColumnIndex(column_address)),
                            cursor.getString(cursor.getColumnIndex(column_spouseName)),
                            cursor.getString(cursor.getColumnIndex(column_spouseOccupation)),
                            cursor.getString(cursor.getColumnIndex(column_familyMembers))
                    );
        }

        cursor.close();
        database.close();

        return DSPItem;
    }

    public void updateNewVisitorList(NewVisitorItem newVisitorItem) {
        SQLiteDatabase database = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        contentValues.put(column_visitorID, newVisitorItem.getVisitorID());
        contentValues.put(column_visitorName, newVisitorItem.getName());
        contentValues.put(column_inTime, newVisitorItem.getInTime());
        contentValues.put(column_purposeOfMeet, newVisitorItem.getPurposeOfMeet());
        contentValues.put(column_IDProof, newVisitorItem.getGovtID());
        contentValues.put(column_IDNum, newVisitorItem.getIDImage());
        contentValues.put(column_organisation, newVisitorItem.getOrganisation());
        contentValues.put(column_type, newVisitorItem.getType());
        contentValues.put(column_residentName, newVisitorItem.getResidentName());
        contentValues.put(column_residentID, newVisitorItem.getResidentID());
        contentValues.put(column_flatNum, newVisitorItem.getFlatNum());
        contentValues.put(column_wing, newVisitorItem.getWing());
        contentValues.put(column_mobNum, newVisitorItem.getMobNum());
        contentValues.put(column_date, newVisitorItem.getDate());

        /*
            to avoid showing null in my visitors
         */
        if (!newVisitorItem.getOutTime().equals("null"))
            contentValues.put(column_outTime, newVisitorItem.getOutTime());

        try {
            int id = (int) database.insertWithOnConflict(newVisitorsTable, null, contentValues, SQLiteDatabase.CONFLICT_IGNORE);
            Log.d("update new visitors", id + "");
            if (id == -1)
                database.update(newVisitorsTable, contentValues, column_visitorID + " = " + newVisitorItem.getVisitorID(), null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        database.close();
    }
}
