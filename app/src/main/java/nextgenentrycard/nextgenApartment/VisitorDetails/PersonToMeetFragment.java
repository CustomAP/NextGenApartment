package nextgenentrycard.nextgenApartment.VisitorDetails;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SimpleAdapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.greenrobot.event.EventBus;
import nextgenentrycard.nextgenApartment.Database.DBHelper;
import nextgenentrycard.nextgenApartment.EventBus.PersonToMeetSetEvent;
import nextgenentrycard.nextgenApartment.Models.ResidentItem;
import nextgenentrycard.nextgenApartment.R;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 2/8/17.
 */

public class PersonToMeetFragment extends Fragment {
    AutoCompleteTextView atvPersonToMeet;
    ArrayList<ResidentItem> residentItems;
    List<HashMap<String,String>> searchList;
    ImageView ivDelete;
    ResidentItem residentItem;
    String origin;
    LinearLayout  llPersonToMeet;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_person_to_meet,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        /*
            initializing auto complete text view
         */
        atvPersonToMeet =(CustomAutoCompleteTextView)view.findViewById(R.id.atvPersonToMeet);
        ivDelete =(ImageView) view.findViewById(R.id.ivDelete_personToMeet);
        llPersonToMeet = (LinearLayout)view.findViewById(R.id.llPersonToMeet_personToMeet);

        atvPersonToMeet.setHint("Person To Meet");

        /*
            initializing variables
         */
        residentItems =new ArrayList<>();
        residentItem =new ResidentItem(0,"","","");
        searchList=new ArrayList<>();

        /*
            getting passed arguments and setting the fields if prebooked
         */
        origin=getArguments().getString("origin");

        if(origin.equals("PreBookVisitor")){
            atvPersonToMeet.setText(getArguments().getString("residentName"));
            Log.d("personToMeet",getArguments().getInt("residentID",0)+"");
            atvPersonToMeet.setClickable(false);
            atvPersonToMeet.setFocusable(false);
            residentItem.setResidentID(getArguments().getInt("residentID", 0));
            residentItem.setResidentName(getArguments().getString("residentName"));
            residentItem.setWing(getArguments().getString("wing"));
            residentItem.setFlatNum(getArguments().getString("flatNum"));

            EventBus.getDefault().post(new PersonToMeetSetEvent(residentItem));
        }


        DBHelper dbHelper = new DBHelper(getContext());
        residentItems = dbHelper.getAllEmployees();
        /*
            getting data from database based on input and adding to arraylist
         */

//        atvPersonToMeet.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                residentItems.clear();
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                residentItems.clear();
//
//                final String input= atvPersonToMeet.getText().toString();
//
//                final DBHelper helper=new DBHelper(getContext());
//
//                Thread personNameThread=new Thread(){
//                  public void run(){
//                      residentItems =helper.getEmployees(input);
//
//                      updateAdapter(residentItems.size());
//                  }
//                };
//
//                personNameThread.run();
//
//            }
//        });

        ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*
                    reset all fields
                 */
                atvPersonToMeet.setText("");
                atvPersonToMeet.setClickable(true);
                atvPersonToMeet.setFocusable(true);
                atvPersonToMeet.setFocusableInTouchMode(true);

                ivDelete.setVisibility(View.GONE);

                /*
                    reset employeeitem
                 */
                residentItem.setResidentName("");
                residentItem.setResidentID(0);
                residentItem.setWing("");
                residentItem.setFlatNum("");

                /*
                    sending blank employeeitem to hostdetails activity
                 */
                EventBus.getDefault().post(new PersonToMeetSetEvent(residentItem));
            }
        });

        for(int i=0;i<residentItems.size();i++){
            HashMap<String, String> hm = new HashMap<String,String>();
            hm.put("residentName", residentItems.get(i).getResidentName());
            hm.put("flatNum", residentItems.get(i).getFlatNum());
            hm.put("wing", residentItems.get(i).getWing());
            hm.put("residentID",String.valueOf(residentItems.get(i).getResidentID()));
            searchList.add(hm);
        }

        /*
            Keys used in Hashmap
          */
        final String[] from = { "residentName","flatNum","wing","residentID"};

        /*
            Ids of views in listview_layout
          */
        int[] to = { R.id.tvEmployeeName_personToMeet,R.id.tvFlatNum_personToMeet,R.id.tvWing_personToMeet,R.id.ivPhoto_personToMeet};

        /*
            setting adapter
         */
        final Drawable defaultProfPic=getContext().getResources().getDrawable(R.drawable.generic_profile);

        SimpleAdapter adapter = new SimpleAdapter(getContext(), searchList, R.layout.person_to_meet_single_item, from, to){
            @Override
            public void setViewImage(final ImageView v, String value) {
                Glide.with(getContext()).load("http://www.accesspoint.in/employees/"+value+".JPG").asBitmap().centerCrop().error(defaultProfPic).into(new BitmapImageViewTarget(v) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        try {
                            RoundedBitmapDrawable circularBitmapDrawable =
                                    RoundedBitmapDrawableFactory.create(getContext().getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            v.setImageDrawable(circularBitmapDrawable);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
            }
        };
        atvPersonToMeet.setAdapter(adapter);
        adapter.notifyDataSetChanged();     //removed the bug of not showing some users!!!!!!!!!!!

        atvPersonToMeet.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                HashMap<String, String> item = (HashMap<String, String>) parent.getItemAtPosition(position);  //bug removed...previous was set to zero
                Log.d("position", position +" ");
                Log.d("residentName", item.get("residentName"));

                /*
                    person to meet text set
                 */
                atvPersonToMeet.setText(item.get("residentName"));

                /*
                    delete button visible
                 */
                ivDelete.setVisibility(View.VISIBLE);

                /*
                    disabling edit text
                 */
                atvPersonToMeet.setClickable(false);
                atvPersonToMeet.setFocusable(false);

                /*
                     Hiding the keyboard
                */
                InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);//hide keyboard
                imm.hideSoftInputFromWindow(atvPersonToMeet.getWindowToken(), 0);//hide keyboard

                /*
                    setting employee item fields from the selected item
                 */
                residentItem.setResidentName(item.get("residentName"));
                residentItem.setResidentID(Integer.parseInt(item.get("residentID")));
                residentItem.setFlatNum(item.get("flatNum"));
                residentItem.setWing(item.get("wing"));



                //  Toast.makeText(getContext(),item.get("employeeDepartment"),Toast.LENGTH_SHORT).show();

                /*
                    sending event with employee item set after clicking the suggestions
                 */
                EventBus.getDefault().post(new PersonToMeetSetEvent(residentItem));

            }
        });


        // Toast.makeText(getContext(),"changed",Toast.LENGTH_SHORT).show();

        atvPersonToMeet.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
/*
            setting employee name to employee item's employee name if not selected from suggestions and entered manually
         */
                residentItem.setResidentName(atvPersonToMeet.getText().toString());

        /*
            sending person to meet details to host details activity
         */
                EventBus.getDefault().post(new PersonToMeetSetEvent(residentItem));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });



    }

//    private void updateAdapter(int num) {
//        searchList.clear();
//        for(int i=0;i<num;i++){
//            Log.d("num",""+num);
//            HashMap<String, String> hm = new HashMap<String,String>();
//            hm.put("residentName", residentItems.get(i).getResidentName());
//            hm.put("flatNum", residentItems.get(i).getFlatNum());
//            hm.put("wing", residentItems.get(i).getWing());
//            hm.put("residentID",String.valueOf(residentItems.get(i).getResidentID()));
//            searchList.add(hm);
//        }
//
//        /*
//            Keys used in Hashmap
//          */
//        final String[] from = { "residentName","flatNum","wing","residentID"};
//
//        /*
//            Ids of views in listview_layout
//          */
//        int[] to = { R.id.tvEmployeeName_personToMeet,R.id.tvFlatNum_personToMeet,R.id.tvWing_personToMeet,R.id.ivPhoto_personToMeet};
//
//        /*
//            setting adapter
//         */
//        final Drawable defaultProfPic=getContext().getResources().getDrawable(R.drawable.generic_profile);
//
//        SimpleAdapter adapter = new SimpleAdapter(getContext(), searchList, R.layout.person_to_meet_single_item, from, to){
//            @Override
//            public void setViewImage(final ImageView v, String value) {
//                Glide.with(getContext()).load("http://www.accesspoint.in/employees/"+value+".JPG").asBitmap().centerCrop().error(defaultProfPic).into(new BitmapImageViewTarget(v) {
//                    @Override
//                    protected void setResource(Bitmap resource) {
//                        try {
//                            RoundedBitmapDrawable circularBitmapDrawable =
//                                    RoundedBitmapDrawableFactory.create(getContext().getResources(), resource);
//                            circularBitmapDrawable.setCircular(true);
//                            v.setImageDrawable(circularBitmapDrawable);
//                        }catch (Exception e){
//                            e.printStackTrace();
//                        }
//                    }
//                });
//            }
//        };
//        atvPersonToMeet.setAdapter(adapter);
//        adapter.notifyDataSetChanged();     //removed the bug of not showing some users!!!!!!!!!!!
//
//        atvPersonToMeet.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                HashMap<String, String> item = (HashMap<String, String>) parent.getItemAtPosition(position);  //bug removed...previous was set to zero
//                Log.d("position", position +" ");
//                Log.d("residentName", item.get("residentName"));
//
//                /*
//                    person to meet text set
//                 */
//                atvPersonToMeet.setText(item.get("residentName"));
//
//                /*
//                    delete button visible
//                 */
//                ivDelete.setVisibility(View.VISIBLE);
//
//                /*
//                    disabling edit text
//                 */
//                atvPersonToMeet.setClickable(false);
//                atvPersonToMeet.setFocusable(false);
//
//                /*
//                     Hiding the keyboard
//                */
//                InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);//hide keyboard
//                imm.hideSoftInputFromWindow(atvPersonToMeet.getWindowToken(), 0);//hide keyboard
//
//                /*
//                    setting employee item fields from the selected item
//                 */
//                residentItem.setResidentName(item.get("residentName"));
//                residentItem.setResidentID(Integer.parseInt(item.get("residentID")));
//                residentItem.setFlatNum(item.get("flatNum"));
//                residentItem.setWing(item.get("wing"));
//
//
//
//              //  Toast.makeText(getContext(),item.get("employeeDepartment"),Toast.LENGTH_SHORT).show();
//
//                /*
//                    sending event with employee item set after clicking the suggestions
//                 */
//                EventBus.getDefault().post(new PersonToMeetSetEvent(residentItem));
//
//            }
//        });
//
//
//       // Toast.makeText(getContext(),"changed",Toast.LENGTH_SHORT).show();
//
//        /*
//            setting employee name to employee item's employee name if not selected from suggestions and entered manually
//         */
//        residentItem.setResidentName(atvPersonToMeet.getText().toString());
//
//        /*
//            sending person to meet details to host details activity
//         */
//        EventBus.getDefault().post(new PersonToMeetSetEvent(residentItem));
//    }
}
