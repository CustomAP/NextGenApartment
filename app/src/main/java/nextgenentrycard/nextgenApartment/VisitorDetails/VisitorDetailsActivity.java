package nextgenentrycard.nextgenApartment.VisitorDetails;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.github.jorgecastilloprz.FABProgressCircle;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.yarolegovich.lovelydialog.LovelyCustomDialog;
import com.yarolegovich.lovelydialog.LovelyProgressDialog;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;

import de.greenrobot.event.EventBus;
import nextgenentrycard.nextgenApartment.Database.DBHelper;
import nextgenentrycard.nextgenApartment.EventBus.NewVisitorDetailsUploadEvent;
import nextgenentrycard.nextgenApartment.EventBus.OTPSentEvent;
import nextgenentrycard.nextgenApartment.EventBus.OTPVerifiedEvent;
import nextgenentrycard.nextgenApartment.EventBus.PersonToMeetSetEvent;
import nextgenentrycard.nextgenApartment.EventBus.SessionEndEvent;
import nextgenentrycard.nextgenApartment.Models.NewVisitorItem;
import nextgenentrycard.nextgenApartment.OTP.OTPServerConnection;
import nextgenentrycard.nextgenApartment.Print.PrintNewVisitorActivity;
import nextgenentrycard.nextgenApartment.R;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 18/10/17.
 */

@RuntimePermissions
public class VisitorDetailsActivity extends AppCompatActivity implements View.OnClickListener {
    TextView tvIDProof;
    Spinner sIDProof;
    MaterialEditText etVisitorName, etOrganisation, etMobNum, etOTP, etWing, etFlatNum;
    ImageView ivPhoto;
    FloatingActionButton bSubmit, fabVerifyMobile;
    FABProgressCircle fpcSubmit;
    String idProofString, organisationString, idNumString, nameString, mobNum, sessionID;
    private static final int PHOTO_REQUEST = 1888;
    private static final int IDPHOTO_REQUEST = 1889;
    Uri photoImage = null;
    Uri idImage = null;
    NewVisitorItem newVisitorItem;
    String origin = null;
    int preBookID = 0;
    String[] purposesOfMeet;
    String[] govtIDProofs;
    CoordinatorLayout clNewVisitorDetails;
    Bitmap bPhotoImage, bIDImage;
    PersonToMeetFragment personToMeetFragment;
    OTPServerConnection otpServerConnection;
    Dialog otpDialog;
    TextView tvBack, tvConfirm;
    LinearLayout llOtpDialog;
    ImageView ivPersonToMeet;
    FABProgressCircle fpcVerifyOtp;
    Dialog progressDailog;
    ImageView ivIDProof;
    TextView tvPersonal, tvOfficial, tvVendor;

    AQuery aQuery;

    String checkRepeatVisitor = "http://www.accesspoint.in/index.php/NewVisitor/checkRepeatedVisitor";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_visitor_details);
        setTitle("Visitor Details");
        EventBus.getDefault().register(this);
        initialize();
    }

    private void initialize() {
        /*
            getting passed arguments
         */
        newVisitorItem = new NewVisitorItem();
        origin = getIntent().getStringExtra("origin");
        if (origin.equals("PreBookVisitor")) {
            preBookID = getIntent().getIntExtra("prebookID", 0);
            newVisitorItem = (NewVisitorItem) getIntent().getSerializableExtra("NewVisitorItem");
        }
        /*
            Initializing views
         */
//        tvPurposeOfMeet = (TextView) findViewById(R.id.tvPurposeOfMeet_visitorDetails);
//        sPurposeOfMeet = (Spinner) findViewById(R.id.sPurposeOfMeet_visitorDetails);
        tvIDProof = (TextView) findViewById(R.id.tvIDProof_visitorDetails);
        sIDProof = (Spinner) findViewById(R.id.sIDProof_visitorDetails);
        etOrganisation = (MaterialEditText) findViewById(R.id.etOrganisation_visitorDetails);
        ivPhoto = (ImageView) findViewById(R.id.ivPhoto_visitorDetails);
        bSubmit = (FloatingActionButton) findViewById(R.id.bSubmit_visitorDetails);
        etVisitorName = (MaterialEditText) findViewById(R.id.etName_visitorDetails);
//        tvGuest = (TextView) findViewById(R.id.tvGuest_visitorDetails);
//        tvVendor = (TextView) findViewById(R.id.tvVendor_visitorDetails);
        etMobNum = (MaterialEditText) findViewById(R.id.etMobNum_visitorDetails);
        clNewVisitorDetails = (CoordinatorLayout) findViewById(R.id.clNewVisitorDetails);
        fpcSubmit = (FABProgressCircle) findViewById(R.id.fpcSubmit);
        fabVerifyMobile = (FloatingActionButton) findViewById(R.id.fabVerifyMobile_vistiorDetails);
        ivPersonToMeet = (ImageView) findViewById(R.id.ivPersonToMeet_visitorDetails);
        fpcVerifyOtp = (FABProgressCircle) findViewById(R.id.fpcVerifyOtp);
        ivIDProof = (ImageView) findViewById(R.id.ivIDProof_visitorDetails);
        tvPersonal = (TextView) findViewById(R.id.tvPersonal_visitorDetails);
        tvVendor = (TextView) findViewById(R.id.tvVendor_visitorDetails);
        tvOfficial = (TextView) findViewById(R.id.tvOfficial_visitorDetails);
        etFlatNum = (MaterialEditText) findViewById(R.id.etFlatNum_visitorDetails);
        etWing = (MaterialEditText) findViewById(R.id.etWing_visitorDetails);

        /*
            setting on click listeners
         */
//        tvPurposeOfMeet.setOnClickListener(this);
        tvIDProof.setOnClickListener(this);
        ivPhoto.setOnClickListener(this);
        bSubmit.setOnClickListener(this);
//        tvGuest.setOnClickListener(this);
//        tvVendor.setOnClickListener(this);
        fabVerifyMobile.setOnClickListener(this);
        ivIDProof.setOnClickListener(this);
        tvPersonal.setOnClickListener(this);
        tvVendor.setOnClickListener(this);
        tvOfficial.setOnClickListener(this);


        /*
            ------------------------------visitor details start here----------------------------------
         */


        /*
            setting purpose of meet spinner
         */
//        purposesOfMeet = new String[]{"Official", "Conference", "Personal", "Interview", "Social", "Vendor", "Promotional Activity", "Government"};
//
//        Arrays.sort(purposesOfMeet);

//        ArrayAdapter<String> purposeOfMeetAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, purposesOfMeet);

//        sPurposeOfMeet.setAdapter(purposeOfMeetAdapter);

        /*
            setting govt ID proof spinner
         */

        govtIDProofs = new String[]{"Aadhar", "Driving License", "Pan Card", "Passport", "Election Card", "Ration Card"};

        Arrays.sort(govtIDProofs);

        ArrayAdapter<String> govtIDProofsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, govtIDProofs);

        sIDProof.setAdapter(govtIDProofsAdapter);


        /*
            Hiding the keyboard
         */
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);//hide keyboard
        imm.hideSoftInputFromWindow(etOrganisation.getWindowToken(), 0);
        /*
            Setting generic profile bPhotoImage resource
         */
        final Drawable defaultProfPic = getApplicationContext().getResources().getDrawable(R.drawable.generic_profile);

        Glide.with(getApplicationContext()).load("http://www.accesspoint.in/newVisitors/" + newVisitorItem.getVisitorID() + ".JPG").asBitmap().centerCrop().error(defaultProfPic).into(new BitmapImageViewTarget(ivPhoto) {
            @Override
            protected void setResource(Bitmap resource) {
                try {
                    ivPhoto.setImageBitmap(resource);
                    photoImage = getImageUri(getApplicationContext(), resource);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        /*
            setting name, purpose of meet, organisation
         */
        if (origin.equals("PreBookVisitor") || newVisitorItem.getVisitorID() != 0) {
            etOrganisation.setText(newVisitorItem.getOrganisation());
            etVisitorName.setText(newVisitorItem.getName());
            if (origin.equals("PreBookVisitor")) {
                if (newVisitorItem.getPurposeOfMeet().equals("Official")) {
                    tvOfficial.setTextColor(getResources().getColor(R.color.colorPrimary));
                    tvVendor.setTextColor(getResources().getColor(android.R.color.darker_gray));
                    tvPersonal.setTextColor(getResources().getColor(android.R.color.darker_gray));

                    newVisitorItem.setPurposeOfMeet("Official");
                } else if (newVisitorItem.getPurposeOfMeet().equals("Personal")) {
                    tvPersonal.setTextColor(getResources().getColor(R.color.colorPrimary));
                    tvVendor.setTextColor(getResources().getColor(android.R.color.darker_gray));
                    tvOfficial.setTextColor(getResources().getColor(android.R.color.darker_gray));

                    newVisitorItem.setPurposeOfMeet("Personal");
                } else if (newVisitorItem.getPurposeOfMeet().equals("Vendor")) {
                    tvVendor.setTextColor(getResources().getColor(R.color.colorPrimary));
                    tvPersonal.setTextColor(getResources().getColor(android.R.color.darker_gray));
                    tvOfficial.setTextColor(getResources().getColor(android.R.color.darker_gray));

                    newVisitorItem.setPurposeOfMeet("Vendor");
                }
            }
//            if (origin.equals("PreBookVisitor")) {
//                sPurposeOfMeet.setSelection(getSelectionIDForPurpose(newVisitorItem.getPurposeOfMeet()));
//                tvPurposeOfMeet.setVisibility(View.GONE);
//                sPurposeOfMeet.setVisibility(View.VISIBLE);
//            }
            /*
                if prebooked for the second time or rebook checked via mob
             */
            if (!newVisitorItem.getIDImage().equals("")) {
                sIDProof.setVisibility(View.VISIBLE);
                tvIDProof.setVisibility(View.GONE);
                sIDProof.setSelection(getSelectionIDForIDProof(newVisitorItem.getGovtID()));
                etMobNum.setText(newVisitorItem.getMobNum());
                mobNum = newVisitorItem.getMobNum();
                fabVerifyMobile.setVisibility(View.GONE);
            }
        }

        /*
            setting visitor type as guest by default
         */
        newVisitorItem.setType("Guest");

        /*
            setting IDNum as '-'
         */
        newVisitorItem.setIDNum("-");
        newVisitorItem.setPurposeOfMeet("Official");

        /*
            --------------------------------Host details start here------------------------------------
         */

        /*
            Initializing variables
         */
        DBHelper dbHelper = new DBHelper(this);
        personToMeetFragment = new PersonToMeetFragment();

        /*
            replacing person to meet fragment.
         */
        personToMeetFragment = new PersonToMeetFragment();

        Bundle args = new Bundle();
        args.putString("origin", origin);
        if (origin.equals("PreBookVisitor")) {
            args.putInt("residentID", newVisitorItem.getResidentID());
            args.putString("residentName", newVisitorItem.getResidentName());
            args.putString("wing", newVisitorItem.getWing());
            args.putString("flatNum", newVisitorItem.getFlatNum());
        }
        personToMeetFragment.setArguments(args);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.llPersonToMeet_vistorDetails, personToMeetFragment);
        transaction.commit();

        /*
            setting fields
         */
        if (origin.equals("PreBookVisitor")) {
            etFlatNum.setText(newVisitorItem.getFlatNum());
            etWing.setText(newVisitorItem.getWing());

            etFlatNum.setClickable(false);
            etFlatNum.setFocusable(false);

            etWing.setClickable(false);
            etWing.setFocusable(false);
        }

        otpServerConnection = new OTPServerConnection(getApplicationContext());
        aQuery = new AQuery(getApplicationContext());
    }

    private int getSelectionIDForPurpose(String purpose) {
        for (int i = 0; i < purposesOfMeet.length; i++) {
            if (purposesOfMeet[i].equals(purpose))
                return i;
        }
        return 0;
    }

    private int getSelectionIDForIDProof(String IDProof) {
        for (int i = 0; i < govtIDProofs.length; i++) {
            if (govtIDProofs[i].equals(IDProof))
                return i;
        }
        return 0;
    }

    private Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "title", null);
        return Uri.parse(path);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//            case R.id.tvPurposeOfMeet_visitorDetails:
//                tvPurposeOfMeet.setVisibility(View.GONE);
//                sPurposeOfMeet.setVisibility(View.VISIBLE);
//                sPurposeOfMeet.performClick();
//                break;

            case R.id.tvIDProof_visitorDetails:
                tvIDProof.setVisibility(View.GONE);
                sIDProof.setVisibility(View.VISIBLE);
                sIDProof.performClick();
                break;

            case R.id.ivPhoto_visitorDetails:
                //getPhoto();
                VisitorDetailsActivityPermissionsDispatcher.getPhotoWithPermissionCheck(this);
                break;

            case R.id.bSubmit_visitorDetails:
                /*
                    Getting the details
                 */
//                purposeOfMeetString = sPurposeOfMeet.getSelectedItem().toString();
                organisationString = etOrganisation.getText().toString();
                idProofString = sIDProof.getSelectedItem().toString();
                //idNumString = etIDNum.getText().toString();
                nameString = etVisitorName.getText().toString();

                if (newVisitorItem.getResidentID() == 0) {
                    newVisitorItem.setFlatNum(etFlatNum.getText().toString().trim());
                    newVisitorItem.setWing(etWing.getText().toString().trim());
                }

                /*
                    Numerous checks
                 */
                if (newVisitorItem.getMobNum() != null) {
                    if (!nameString.equals("")) {
                        if (!organisationString.equals("")) {
                            if (photoImage != null) {
                                if (!newVisitorItem.getFlatNum().equals("")) {
                                    if (!newVisitorItem.getWing().equals("")) {
                                        if (!newVisitorItem.getFlatNum().equals("")) {
                                            fpcSubmit.show();
                                            bSubmit.setEnabled(false);
                                            bSubmit.setClickable(false);
                                            bSubmit.setFocusable(false);
//                                        aviVisitorDetails.setVisibility(View.VISIBLE);
//                                        aviVisitorDetails.show();
                                    /*
                                         upload details and save in local db
                                      */
                                            newVisitorItem.setOrganisation(organisationString);
                                            newVisitorItem.setGovtID(idProofString);
                                            newVisitorItem.setName(nameString);

                                            Log.d("newvisitoritem", newVisitorItem.getName() + " " + newVisitorItem.getMobNum() + " " + newVisitorItem.getPreBookID() + " " + newVisitorItem.getResidentID() + newVisitorItem.getType() + " " + newVisitorItem.getDate() + " " + newVisitorItem.getGovtID() + " " + newVisitorItem.getIDImage() + " " + newVisitorItem.getInTime() + " " + newVisitorItem.getOrganisation() + " " + newVisitorItem.getPurposeOfMeet());

                                            Date date = new Date();
                                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                                            newVisitorItem.setDate(sdf.format(date));

                                            Log.d("date", newVisitorItem.getDate());

                                            if (origin.equals("PreBookVisitor"))
                                                newVisitorItem.setPreBookID(getIntent().getIntExtra("prebookID", 0));

                                            try {
                                            /*
                                                encoding bPhotoImage to base64
                                             */
                                                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), photoImage);


                                                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                                                bitmap.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
                                                String encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);

                                                newVisitorItem.setImage(encodedImage);

                                            /*
                                                uploading visitor details
                                             */


                                                if (idImage != null) {
                                                    try {
                                                /*
                                                encoding bPhotoImage to base64
                                             */
                                                        bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), idImage);


                                                        byteArrayOutputStream = new ByteArrayOutputStream();
                                                        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
                                                        encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);

                                                        newVisitorItem.setIDImage(encodedImage);
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                }

                                                VisitorDetailsServerConnection visitorDetailsServerConnection = new VisitorDetailsServerConnection(this);
                                                visitorDetailsServerConnection.uploadVisitorDetails(newVisitorItem);
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }
                                        } else {
                                            Snackbar.make(clNewVisitorDetails, "Flat Number cannot be empty", Snackbar.LENGTH_SHORT).show();
                                        }
                                    } else {
                                        Snackbar.make(clNewVisitorDetails, "Wing cannot be empty", Snackbar.LENGTH_SHORT).show();
                                    }
                                } else {
                                    Snackbar.make(clNewVisitorDetails, "Person to meet cannot be empty", Snackbar.LENGTH_SHORT).show();
                                }
                            } else {
                                Snackbar.make(clNewVisitorDetails, "Please click a photo", Snackbar.LENGTH_SHORT).show();
//                                        Toast.makeText(this, "Please click a photo", Toast.LENGTH_SHORT).show();
                            }

                        } else {
                            Snackbar.make(clNewVisitorDetails, "Organisation field cannot be empty", Snackbar.LENGTH_SHORT).show();
//                            Toast.makeText(this, "Organisation field cannot be empty", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Snackbar.make(clNewVisitorDetails, "Name cannot be empty", Snackbar.LENGTH_SHORT).show();
//                    Toast.makeText(this, "Name cannot be empty", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Snackbar.make(clNewVisitorDetails, "Please verify mobile number first", Snackbar.LENGTH_SHORT).show();
                }
                break;

//            case R.id.tvGuest_visitorDetails:
//                newVisitorItem.setType("Guest");
//                /*
//                    setting colors
//                 */
//                tvGuest.setTextColor(getResources().getColor(R.color.colorPrimary));
//                tvVendor.setTextColor(getResources().getColor(android.R.color.darker_gray));
//                break;
//
//            case R.id.tvVendor_visitorDetails:
//                newVisitorItem.setType("Vendor");
//                /*
//                    setting colors
//                 */
//                tvVendor.setTextColor(getResources().getColor(R.color.colorPrimary));
//                tvGuest.setTextColor(getResources().getColor(android.R.color.darker_gray));
//                break;

            case R.id.fabVerifyMobile_vistiorDetails:
                /*
                    sending otp
                 */
                mobNum = etMobNum.getText().toString();
                if (mobNum.length() != 10) {
                    Snackbar.make(clNewVisitorDetails, "Mobile Number should be of 10 Characters", Snackbar.LENGTH_SHORT).show();
                } else {
                    fpcVerifyOtp.show();
                    otpServerConnection.sendOTP(etMobNum.getText().toString());
                }
                break;

            case R.id.ivIDProof_visitorDetails:
                VisitorDetailsActivityPermissionsDispatcher.getIDPhotoWithPermissionCheck(this);
                break;

            case R.id.tvPersonal_visitorDetails:
                tvPersonal.setTextColor(getResources().getColor(R.color.colorPrimary));
                tvVendor.setTextColor(getResources().getColor(android.R.color.darker_gray));
                tvOfficial.setTextColor(getResources().getColor(android.R.color.darker_gray));

                newVisitorItem.setPurposeOfMeet("Personal");
                break;

            case R.id.tvVendor_visitorDetails:
                tvVendor.setTextColor(getResources().getColor(R.color.colorPrimary));
                tvPersonal.setTextColor(getResources().getColor(android.R.color.darker_gray));
                tvOfficial.setTextColor(getResources().getColor(android.R.color.darker_gray));

                newVisitorItem.setPurposeOfMeet("Vendor");
                break;

            case R.id.tvOfficial_visitorDetails:
                tvOfficial.setTextColor(getResources().getColor(R.color.colorPrimary));
                tvVendor.setTextColor(getResources().getColor(android.R.color.darker_gray));
                tvPersonal.setTextColor(getResources().getColor(android.R.color.darker_gray));

                newVisitorItem.setPurposeOfMeet("Official");
                break;
        }
    }

    public void onEvent(OTPSentEvent otpSentEvent) {
        boolean otpSuccess = otpSentEvent.isOtpSuccess();
        if (otpSuccess) {
            fpcVerifyOtp.hide();
            sessionID = otpSentEvent.getSessionID();
            mobNum = otpSentEvent.getMobNum();
//            Log.d("otp sent", "success");
//            Log.d("mobNum", mobNum);
//            Log.d("sesssionID", sessionID);

            otpDialog = new LovelyCustomDialog(this, R.style.myDialog)
                    .setCancelable(false)
                    .setView(R.layout.otp_dialog)
                    .configureView(new LovelyCustomDialog.ViewConfigurator() {
                        @Override
                        public void configureView(View v) {
                            etOTP = (MaterialEditText) v.findViewById(R.id.etOtp_otpDialog);
                            tvBack = (TextView) v.findViewById(R.id.tvBack_otpDialog);
                            tvConfirm = (TextView) v.findViewById(R.id.tvConfirm_otpDialog);
                            llOtpDialog = (LinearLayout) v.findViewById(R.id.llOtpDialog);
                        }
                    })
                    .setTopColorRes(R.color.colorPrimary)
                    .setIcon(R.drawable.otp)
                    .setListener(R.id.tvBack_otpDialog, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            otpDialog.dismiss();
                        }
                    })
                    .setListener(R.id.tvConfirm_otpDialog, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Snackbar.make(llOtpDialog, "Verifying OTP", Snackbar.LENGTH_SHORT).show();
                            otpServerConnection.verifyOTP(sessionID, etOTP.getText().toString());
                        }
                    })
                    .show();
        } else {
            Snackbar.make(clNewVisitorDetails, "Failed to send OTP", Snackbar.LENGTH_SHORT).show();
        }
    }

    public void onEvent(OTPVerifiedEvent otpVerifiedEvent) {
        boolean isOTPVerified = otpVerifiedEvent.isOTPVerified();

        if (isOTPVerified) {
            newVisitorItem.setMobNum(mobNum);
            otpDialog.dismiss();

            /*
                show progress dialog and check for repeat visitor based on mobile num.
             */
            progressDailog = new LovelyProgressDialog(this, R.style.myDialog)
                    .setIcon(R.drawable.person)
                    .setTitle("Please wait")
                    .setTopColorRes(R.color.colorPrimary)
                    .setCancelable(false)
                    .show();

            /*
                            checking if the visitor is repeated with mobile number
                         */

            SharedPreferences preferences = getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
            String sessionID = preferences.getString("sessionID", null);

            HashMap<String, Object> checkRepeatVisitorParams = new HashMap<>();
            checkRepeatVisitorParams.put("sessionID", sessionID);
            Log.d("mobNum", newVisitorItem.getMobNum());
            checkRepeatVisitorParams.put("mobileNum", newVisitorItem.getMobNum());

            aQuery.ajax(checkRepeatVisitor, checkRepeatVisitorParams, JSONObject.class, new AjaxCallback<JSONObject>() {
                @Override
                public void callback(String url, JSONObject object, AjaxStatus status) {
                    String visitorName = null, organisation = null, govtID = null, IDNum = null, visitorType = null;
                    int visitorID = 0;

                    if (object != null) {
                        Log.d("checkRepeatVisitor", "object != null");
                        try {
                                        /*
                                            if visitor is not repeated exception thrown and new activity starts
                                         */
                            visitorName = object.getString("visitorName");
                            organisation = object.getString("organisation");
                            govtID = object.getString("IDProof");
                            IDNum = object.getString("IDNo");
                            visitorType = object.getString("type");
                            visitorID = object.getInt("newVisitorID");

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        progressDailog.hide();
                    }
                    if (status.getCode() == 200) {
                        progressDailog.hide();
                        newVisitorItem.setName(visitorName);
                        newVisitorItem.setOrganisation(organisation);
                        newVisitorItem.setGovtID(govtID);
                        Log.d("IDNum", IDNum);
                        newVisitorItem.setIDNum(IDNum);
                        newVisitorItem.setType(visitorType);
                        newVisitorItem.setVisitorID(visitorID);

                        etVisitorName.setText(newVisitorItem.getName());
                        etOrganisation.setText(newVisitorItem.getOrganisation());
                        sIDProof.setSelection(getSelectionIDForIDProof(newVisitorItem.getGovtID()));
                        tvIDProof.setVisibility(View.GONE);
                        ivIDProof.setImageResource(R.drawable.tick);
                        sIDProof.setVisibility(View.VISIBLE);
//                    if(newVisitorItem.getType().equals("Guest")){
//                        tvGuest.setTextColor(getResources().getColor(R.color.colorPrimary));
//                        tvVendor.setTextColor(getResources().getColor(android.R.color.darker_gray));
//                    }else if(newVisitorItem.getType().equals("Vendor")){
//                        tvVendor.setTextColor(getResources().getColor(R.color.colorPrimary));
//                        tvGuest.setTextColor(getResources().getColor(android.R.color.darker_gray));
//                    }

                        final Drawable defaultProfPic = getApplicationContext().getResources().getDrawable(R.drawable.generic_profile);

                        Glide.with(getApplicationContext()).load("http://www.accesspoint.in/newVisitors/" + newVisitorItem.getVisitorID() + ".JPG").asBitmap().centerCrop().error(defaultProfPic).into(new BitmapImageViewTarget(ivPhoto) {
                            @Override
                            protected void setResource(Bitmap resource) {
                                super.setResource(resource);
                                try {
                                    ivPhoto.setImageBitmap(resource);
                                    photoImage = getImageUri(getApplicationContext(), resource);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                }
            });

            etMobNum.setFocusable(false);
            etMobNum.setClickable(false);
            fabVerifyMobile.setVisibility(View.GONE);
            fpcVerifyOtp.setVisibility(View.GONE);
        } else {
            Snackbar.make(llOtpDialog, "OTP verification failed", Snackbar.LENGTH_SHORT).show();
        }
    }


    public void onEvent(PersonToMeetSetEvent personToMeetSetEvent) {

        /*
            getting fields from person to meet fragment
         */
        newVisitorItem.setResidentName(personToMeetSetEvent.getResidentItem().getResidentName());
        newVisitorItem.setResidentID(personToMeetSetEvent.getResidentItem().getResidentID());
        newVisitorItem.setFlatNum(personToMeetSetEvent.getResidentItem().getFlatNum());
        newVisitorItem.setWing(personToMeetSetEvent.getResidentItem().getWing());

        etWing.setText(newVisitorItem.getWing());
        etFlatNum.setText(newVisitorItem.getFlatNum());

        //Toast.makeText(this,personToMeetSetEvent.getResidentItem().getDepartmentName(),Toast.LENGTH_SHORT).show();

            /*
                if person to visit is selected from the suggestions
             */
        final Drawable defaultProfPic = getApplicationContext().getResources().getDrawable(R.drawable.generic_profile);

        Glide.with(getApplicationContext()).load("http://www.accesspoint.in/employees/" + newVisitorItem.getResidentID() + ".JPG").asBitmap().centerCrop().error(defaultProfPic).into(new BitmapImageViewTarget(ivPersonToMeet));


            /*
                setting bPhotoImage view
             */

        //to be done

    }


    @NeedsPermission({Manifest.permission.WRITE_EXTERNAL_STORAGE})
    public void getIDPhoto() {
        /*
           opening camera
           */
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, IDPHOTO_REQUEST);
    }

    @NeedsPermission({Manifest.permission.WRITE_EXTERNAL_STORAGE})
    public void getPhoto() {
        /*
           opening camera
           */
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, PHOTO_REQUEST);
    }

    @OnPermissionDenied(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    void showDeniedForReadContacts() {
        Toast.makeText(this, "Permission was denied\nPlease give the required permissions and restart", Toast.LENGTH_SHORT).show();
    }

    @OnNeverAskAgain(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    void showNeverAskForReadContacts() {
        Toast.makeText(this, "Please give required Permissions from Settings App", Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        VisitorDetailsActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    private void showRationaleDialog(@StringRes int messageResId, final PermissionRequest request) {
        new AlertDialog.Builder(this)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(@NonNull DialogInterface dialog, int which) {
                        request.proceed();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(@NonNull DialogInterface dialog, int which) {
                        request.cancel();
                    }
                })
                .setCancelable(false)
                .setMessage(messageResId)
                .show();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("in here", "got");
        /*
            Image received from the camera
         */
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PHOTO_REQUEST && resultCode == Activity.RESULT_OK) {
            // photoImage = data.getData();
            photoImage = getImageUri(this, (Bitmap) data.getExtras().get("data"));
            Log.d("int ", photoImage + "");
            Glide.with(this).load(photoImage).asBitmap().centerCrop().diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true).into(new BitmapImageViewTarget(ivPhoto));
        } else if (requestCode == IDPHOTO_REQUEST && resultCode == Activity.RESULT_OK) {
            idImage = getImageUri(this, (Bitmap) data.getExtras().get("data"));
            Log.d("int ", idImage + "");
            ivIDProof.setImageResource(R.drawable.tick);
        }
    }

    public void onEvent(NewVisitorDetailsUploadEvent newVisitorDetailsUploadEvent) {

        bSubmit.setEnabled(true);
        bSubmit.setClickable(true);
        bSubmit.setFocusable(true);
        bSubmit.setFocusableInTouchMode(true);
        fpcSubmit.hide();
//        aviVisitorDetails.hide();
//        aviVisitorDetails.setVisibility(View.GONE);

        if (newVisitorDetailsUploadEvent.isUploaded()) {

            if (origin.equals("PreBookVisitor")) {
                DBHelper helper = new DBHelper(this);
                helper.deletePreBookVisitor(preBookID);
            }

            try {
                bPhotoImage = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), photoImage);

                createDirectoryAndSaveFile(bPhotoImage, newVisitorDetailsUploadEvent.getVisitorID() + "");

                if (idImage != null) {
                    bIDImage = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), idImage);
                    createDirectoryAndSaveIDProof(bIDImage, newVisitorDetailsUploadEvent.getVisitorID() + "");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


            new LovelyStandardDialog(this, R.style.myDialog)
                    .setTopColorRes(R.color.colorPrimary)
                    .setButtonsColorRes(R.color.black)
                    .setIcon(R.mipmap.done)
                    .setTitle(R.string.info)
                    .setMessage(R.string.new_visitor_add_feedback)
                    .setCancelable(false)
                    .setPositiveButton("Print", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
//                            finish();
                            Intent i = new Intent(getApplicationContext(), PrintNewVisitorActivity.class);
                            i.putExtra("NewVisitor", newVisitorItem);
                            i.putExtra("Photo", photoImage);
                            startActivity(i);
                        }
                    })
                    .show();

        } else {
            fpcVerifyOtp.hide();
            bSubmit.setEnabled(true);
            bSubmit.setClickable(true);
            bSubmit.setFocusable(true);
        }

    }

    private void createDirectoryAndSaveFile(final Bitmap imageToSave, final String fileName) {

        Thread run = new Thread() {

            public void run() {
                File direct = new File(Environment.getExternalStorageDirectory() + "/DirName");

                if (!direct.exists()) {
                    File wallpaperDirectory = new File(Environment.getExternalStorageDirectory().getPath() + "/NextGen/NewVisitor/");
                    wallpaperDirectory.mkdirs();
                }

                File file = new File(new File(Environment.getExternalStorageDirectory().getPath() + "/NextGen/NewVisitor/"), fileName + ".jpg");
                if (file.exists()) {
                    file.delete();
                }
                try {
                    FileOutputStream out = new FileOutputStream(file);
                    imageToSave.compress(Bitmap.CompressFormat.JPEG, 100, out);
                    out.flush();
                    out.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        run.run();
    }

    private void createDirectoryAndSaveIDProof(final Bitmap imageToSave, final String fileName) {

        Thread run = new Thread() {

            public void run() {
                File direct = new File(Environment.getExternalStorageDirectory() + "/DirName");

                if (!direct.exists()) {
                    File wallpaperDirectory = new File(Environment.getExternalStorageDirectory().getPath() + "/NextGen/IDProof/");
                    wallpaperDirectory.mkdirs();
                }

                File file = new File(new File(Environment.getExternalStorageDirectory().getPath() + "/NextGen/IDProof/"), fileName + ".jpg");
                if (file.exists()) {
                    file.delete();
                }
                try {
                    FileOutputStream out = new FileOutputStream(file);
                    imageToSave.compress(Bitmap.CompressFormat.JPEG, 100, out);
                    out.flush();
                    out.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        run.run();
    }

    public void onEvent(SessionEndEvent sessionEndEvent) {
        /*
            received after clicked leave
         */
        if (sessionEndEvent.isEndSession())
            finish();
    }
}
