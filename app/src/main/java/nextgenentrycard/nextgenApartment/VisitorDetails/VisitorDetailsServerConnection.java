package nextgenentrycard.nextgenApartment.VisitorDetails;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;

import java.util.HashMap;

import de.greenrobot.event.EventBus;
import nextgenentrycard.nextgenApartment.Database.DBHelper;
import nextgenentrycard.nextgenApartment.EventBus.NewVisitorDetailsUploadEvent;
import nextgenentrycard.nextgenApartment.Models.NewVisitorItem;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 22/10/17.
 */

public class VisitorDetailsServerConnection {
    AQuery aQuery;
    Context context;

    String uploadVisitorDetails="http://www.accesspoint.in/index.php/NewVisitor/uploadVisitorDetails";

    public VisitorDetailsServerConnection(Context context){
        this.context=context;
        aQuery=new AQuery(context);
    }

    public void uploadVisitorDetails(final NewVisitorItem newVisitorItem){

        SharedPreferences preferences1 = context.getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        String sessionID = preferences1.getString("sessionID", null);

        final HashMap<String,Object> newVisitorParams=new HashMap<>();
        newVisitorParams.put("visitorName",newVisitorItem.getName());
        newVisitorParams.put("visitorType",newVisitorItem.getType());
        newVisitorParams.put("purposeOfMeet",newVisitorItem.getPurposeOfMeet());
        newVisitorParams.put("organisation",newVisitorItem.getOrganisation());
        newVisitorParams.put("IDProof",newVisitorItem.getGovtID());
        newVisitorParams.put("IDNum",newVisitorItem.getIDNum());
        newVisitorParams.put("userName",newVisitorItem.getResidentName());
        newVisitorParams.put("userID",newVisitorItem.getResidentID());
        newVisitorParams.put("flatNum", newVisitorItem.getFlatNum());
        newVisitorParams.put("wing", newVisitorItem.getWing());
        newVisitorParams.put("visitorImage",newVisitorItem.getImage());
        newVisitorParams.put("mobNum",newVisitorItem.getMobNum());
        newVisitorParams.put("prebookID",newVisitorItem.getPreBookID());
        newVisitorParams.put("sessionID",sessionID);

        if(newVisitorItem.getIDImage() != null)
            newVisitorParams.put("IDImage",newVisitorItem.getIDImage());

        Log.d("uploadvisitor",newVisitorParams.toString());
        Log.d("IDNum",newVisitorItem.getIDNum());

        aQuery.ajax(uploadVisitorDetails,newVisitorParams, JSONObject.class,new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                String inTime=null;
                int visitorID=0;

                if(object!=null){
                    Log.d("uploadVisitorDetails","object!=null");
                    try{
                        inTime=object.getString("inTime");
                        visitorID=object.getInt("visitorID");
                        newVisitorItem.setInTime(inTime);
                        newVisitorItem.setVisitorID(visitorID);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }else{
                    Log.d("uploadVisitorDetails","object=null status:"+status.getCode());
                    Toast.makeText(context,"Failed to submit visitor Data",Toast.LENGTH_SHORT).show();
                }

                if(status.getCode()==200){
                    DBHelper helper=new DBHelper(context);
                    helper.addVisitor(newVisitorItem);

                    EventBus.getDefault().post(new NewVisitorDetailsUploadEvent(true,newVisitorItem.getVisitorID()));
                }
            }
        });
    }
}
