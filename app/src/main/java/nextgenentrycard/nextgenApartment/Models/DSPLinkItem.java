package nextgenentrycard.nextgenApartment.Models;

import java.io.Serializable;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 13/3/18.
 */

public class DSPLinkItem implements Serializable{
    private int dspID, dspLinkID, residentID;
    private String userName, flatNum, wing, work;

    public int getDspID() {
        return dspID;
    }

    public int getDspLinkID() {
        return dspLinkID;
    }

    public int getResidentID() {
        return residentID;
    }

    public String getUserName() {
        return userName;
    }

    public String getFlatNum() {
        return flatNum;
    }

    public String getWing() {
        return wing;
    }

    public String getWork() {
        return work;
    }

    public DSPLinkItem(int dspID, int dspLinkID, int residentID, String userName, String flatNum, String wing, String work) {

        this.dspID = dspID;
        this.dspLinkID = dspLinkID;
        this.residentID = residentID;
        this.userName = userName;
        this.flatNum = flatNum;
        this.wing = wing;
        this.work = work;
    }
}
