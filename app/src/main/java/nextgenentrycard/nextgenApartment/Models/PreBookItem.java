package nextgenentrycard.nextgenApartment.Models;

import java.io.Serializable;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 18/10/17.
 */

public class PreBookItem implements Serializable{
    private String name;
    private String time;
    private String purposeOfMeet;
    private String organisation;
    private String userName;
    private String IDProof;
    private String IDNum;
    private String type;
    private String mobNum;
    private String date;
    private int prebookID, userID,visitorID;
    private String flatNum, wing;

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {

        return date;
    }

    public void setMobNum(String mobNum) {
        this.mobNum = mobNum;
    }

    public String getMobNum() {

        return mobNum;
    }
    public void setIDProof(String IDProof) {
        this.IDProof = IDProof;
    }

    public void setIDNum(String IDNum) {
        this.IDNum = IDNum;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIDProof() {

        return IDProof;
    }

    public String getIDNum() {
        return IDNum;
    }

    public String getType() {
        return type;
    }
    public String getUserName() {
        return userName;
    }



    public String getName() {
        return name;
    }

    public String getTime() {
        return time;
    }

    public String getPurposeOfMeet() {
        return purposeOfMeet;
    }

    public String getOrganisation() {
        return organisation;
    }

    public int getPrebookID() {
        return prebookID;
    }

    public int getUserID() {
        return userID;
    }

    public int getVisitorID() {
        return visitorID;
    }

    public PreBookItem(String name, String time, String purposeOfMeet, String organisation, int prebookID, int userID, String userName, String IDProof, String IDNum, String type, String mobNum, int visitorID, String date, String flatNum, String wing) {

        this.IDProof=IDProof;
        this.IDNum=IDNum;
        this.type=type;
        this.name = name;
        this.time = time;
        this.purposeOfMeet = purposeOfMeet;
        this.organisation = organisation;
        this.prebookID = prebookID;
        this.userID = userID;
        this.userName = userName;
        this.mobNum=mobNum;
        this.visitorID=visitorID;
        this.date=date;
        this.flatNum = flatNum;
        this.wing = wing;
    }

    public String getFlatNum() {
        return flatNum;
    }

    public String getWing() {
        return wing;
    }
}
