package nextgenentrycard.nextgenApartment.Models;

import java.io.Serializable;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 19/10/17.
 */

public class SecurityItem implements Serializable {
    String securityName;
    int securityID;

    public String getSecurityName() {
        return securityName;
    }

    public int getSecurityID() {
        return securityID;
    }

    public SecurityItem(String securityName, int securityID) {

        this.securityName = securityName;
        this.securityID = securityID;
    }
}
