package nextgenentrycard.nextgenApartment.Models;

import java.io.Serializable;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 19/10/17.
 */

public class ApartmentItem implements Serializable {
    String companyName;
    int companyID;

    public String getCompanyName() {
        return companyName;
    }

    public int getCompanyID() {
        return companyID;
    }

    public ApartmentItem(String companyName, int companyID) {

        this.companyName = companyName;
        this.companyID = companyID;
    }
}
