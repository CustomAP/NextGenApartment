package nextgenentrycard.nextgenApartment.Models;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 26/1/18.
 */

public class HomeItem {
    private String optionName;
    private int image;

    public String getOptionName() {
        return optionName;
    }

    public int getImage() {
        return image;
    }

    public HomeItem(String optionName, int image) {
        this.optionName = optionName;
        this.image = image;
    }
}
