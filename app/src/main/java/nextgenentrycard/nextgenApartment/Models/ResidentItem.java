package nextgenentrycard.nextgenApartment.Models;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 21/10/17.
 */

public class ResidentItem {
    private int residentID;
    private String residentName;
    private String wing, flatNum;

    public ResidentItem(int residentID, String residentName, String wing, String flatNum) {
        this.residentID = residentID;
        this.residentName = residentName;
        this.wing = wing;
        this.flatNum = flatNum;
    }

    public String getWing() {

        return wing;
    }

    public String getFlatNum() {
        return flatNum;
    }

    public int getResidentID() {
        return residentID;
    }

    public String getResidentName() {
        return residentName;
    }

    public void setWing(String wing) {
        this.wing = wing;
    }

    public void setFlatNum(String flatNum) {
        this.flatNum = flatNum;
    }

    public void setResidentID(int residentID) {
        this.residentID = residentID;
    }

    public void setResidentName(String residentName) {
        this.residentName = residentName;
    }
}
