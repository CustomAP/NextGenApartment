package nextgenentrycard.nextgenApartment.Models;

import java.util.ArrayList;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 30/10/17.
 */

public class DailyVisitorDesignationItem {
    private String designation;
    private ArrayList<DSPItem> DSPItems;

    public String getDesignation() {
        return designation;
    }

    public ArrayList<DSPItem> getDSPItems() {
        return DSPItems;
    }

    public DailyVisitorDesignationItem(String designation, ArrayList<DSPItem> DSPItems) {

        this.designation = designation;
        this.DSPItems = DSPItems;
    }
    public DailyVisitorDesignationItem(){}

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public void setDSPItems(ArrayList<DSPItem> DSPItems) {
        this.DSPItems = DSPItems;
    }
}
