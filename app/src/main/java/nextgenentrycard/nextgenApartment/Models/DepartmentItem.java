package nextgenentrycard.nextgenApartment.Models;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 21/10/17.
 */

public class DepartmentItem {
    private int departmentID;
    private String departmentName;

    public int getDepartmentID() {
        return departmentID;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public DepartmentItem(int departmentID, String departmentName) {

        this.departmentID = departmentID;
        this.departmentName = departmentName;
    }
}
