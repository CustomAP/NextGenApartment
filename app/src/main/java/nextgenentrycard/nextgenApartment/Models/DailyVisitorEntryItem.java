package nextgenentrycard.nextgenApartment.Models;

import java.io.Serializable;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 30/10/17.
 */

public class DailyVisitorEntryItem implements Serializable{
    private String visitorName;
    private int dailyVisitorID;
    private int dailyVisitorEntryID;
    private String inTime,outTime,date;

    public String getVisitorName() {
        return visitorName;
    }

    public int getDailyVisitorID() {
        return dailyVisitorID;
    }

    public int getDailyVisitorEntryID() {
        return dailyVisitorEntryID;
    }

   public String getInTime() {
        return inTime;
    }

    public String getOutTime() {
        return outTime;
    }

    public String getDate() {
        return date;
    }

    public DailyVisitorEntryItem(String visitorName, int dailyVisitorID, int dailyVisitorEntryID, String inTime, String date) {

        this.visitorName = visitorName;
        this.dailyVisitorID = dailyVisitorID;
        this.dailyVisitorEntryID = dailyVisitorEntryID;
        this.inTime = inTime;
        this.date = date;
    }

    public DailyVisitorEntryItem(String visitorName, int dailyVisitorID, int dailyVisitorEntryID, String inTime, String outTime, String date) {
        this.visitorName = visitorName;
        this.dailyVisitorID = dailyVisitorID;
        this.dailyVisitorEntryID = dailyVisitorEntryID;
        this.inTime = inTime;
        this.outTime = outTime;
        this.date = date;
    }
}
