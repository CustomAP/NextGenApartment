package nextgenentrycard.nextgenApartment.Models;

import java.io.Serializable;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 29/10/17.
 */

public class DSPItem implements Serializable{
    private int dspID;
    private String name,IDProof,IDNum,mobileNum, address, spouseName, spouseOccupation, familyMembers;

    public void setDspID(int dspID) {
        this.dspID = dspID;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setIDProof(String IDProof) {
        this.IDProof = IDProof;
    }

    public void setIDNum(String IDNum) {
        this.IDNum = IDNum;
    }

    public void setMobileNum(String mobileNum) {
        this.mobileNum = mobileNum;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getDspID() {

        return dspID;
    }

    public String getName() {
        return name;
    }

    public String getIDProof() {
        return IDProof;
    }

    public String getIDNum() {
        return IDNum;
    }

    public String getMobileNum() {
        return mobileNum;
    }

    public String getAddress() {
        return address;
    }

    public DSPItem(){};

    public String getSpouseName() {
        return spouseName;
    }

    public String getSpouseOccupation() {
        return spouseOccupation;
    }

    public String getFamilyMembers() {
        return familyMembers;
    }

    public void setSpouseName(String spouseName) {
        this.spouseName = spouseName;
    }

    public void setSpouseOccupation(String spouseOccupation) {
        this.spouseOccupation = spouseOccupation;
    }

    public void setFamilyMembers(String familyMembers) {
        this.familyMembers = familyMembers;
    }

    public DSPItem(int dspID, String name, String IDProof, String IDNum, String mobileNum, String address, String spouseName, String spouseOccupation, String familyMembers) {

        this.dspID = dspID;
        this.name = name;
        this.IDProof = IDProof;
        this.IDNum = IDNum;
        this.mobileNum = mobileNum;
        this.address = address;
        this.spouseName = spouseName;
        this.spouseOccupation = spouseOccupation;
        this.familyMembers = familyMembers;

    }
}
