package nextgenentrycard.nextgenApartment.Models;

import android.support.annotation.NonNull;

import java.io.Serializable;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 18/10/17.
 */

public class NewVisitorItem implements Serializable, Comparable<NewVisitorItem> {
    private String mobNum;
    private String name;
    private String purposeOfMeet;
    private String organisation;
    private String govtID;
    private String IDImage;
    private String type;
    private String residentName;
    private String inTime;
    private String outTime;

    public void setFlatNum(String flatNum) {
        this.flatNum = flatNum;
    }

    public void setWing(String wing) {
        this.wing = wing;
    }

    private int residentID;
    private String image;
    private int visitorID;
    private String date;
    private int preBookID;
    private String IDNum;
    private String flatNum, wing;

    public NewVisitorItem(String mobNum, String name, String purposeOfMeet, String organisation, String govtID, String IDNum, String type, String residentName, String inTime, String outTime, int residentID, int visitorID, String date, String flatNum, String wing) {
        this.mobNum = mobNum;
        this.name = name;
        this.purposeOfMeet = purposeOfMeet;
        this.organisation = organisation;
        this.govtID = govtID;
        this.IDNum = IDNum;
        this.type = type;
        this.residentName = residentName;
        this.inTime = inTime;
        this.outTime = outTime;
        this.residentID = residentID;
        this.visitorID = visitorID;
        this.flatNum = flatNum;
        this.wing = wing;

    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {

        return date;
    }

    public void setPreBookID(int preBookID) {
        this.preBookID = preBookID;
    }

    public int getPreBookID() {

        return preBookID;
    }

    public void setVisitorID(int visitorID) {
        this.visitorID = visitorID;
    }

    public int getVisitorID() {

        return visitorID;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage() {

        return image;
    }

    public NewVisitorItem() {

    }

    public void setIDNum(String IDNum) {
        this.IDNum = IDNum;
    }

    public String getIDNum() {

        return IDNum;
    }

    public NewVisitorItem(String mobNum, String name, String purposeOfMeet, String organisation, String govtID, String IDNum, String type, String residentName, String inTime, String outTime, int residentID, int visitorID, String flatNum, String wing) {
        this.mobNum = mobNum;
        this.name = name;
        this.purposeOfMeet = purposeOfMeet;
        this.organisation = organisation;
        this.govtID = govtID;
        this.IDNum = IDNum;
        this.type = type;
        this.residentName = residentName;
        this.inTime = inTime;
        this.outTime = outTime;
        this.residentID = residentID;
        this.visitorID = visitorID;
        this.flatNum = flatNum;
        this.wing = wing;
    }

    public String getFlatNum() {
        return flatNum;
    }

    public String getWing() {
        return wing;
    }

    public void setResidentName(String residentName) {

        this.residentName = residentName;
    }

    public void setInTime(String inTime) {
        this.inTime = inTime;
    }

    public void setOutTime(String outTime) {
        this.outTime = outTime;
    }

    public void setResidentID(int residentID) {
        this.residentID = residentID;
    }

    public String getResidentName() {

        return residentName;
    }

    public String getInTime() {
        return inTime;
    }

    public String getOutTime() {
        return outTime;
    }

    public int getResidentID() {
        return residentID;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {

        return type;
    }

    public void setMobNum(String mobNum) {
        this.mobNum = mobNum;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPurposeOfMeet(String purposeOfMeet) {
        this.purposeOfMeet = purposeOfMeet;
    }

    public void setOrganisation(String organisation) {
        this.organisation = organisation;
    }

    public void setGovtID(String govtID) {
        this.govtID = govtID;
    }

    public void setIDImage(String IDImage) {
        this.IDImage = IDImage;
    }

    public String getMobNum() {

        return mobNum;
    }

    public String getName() {
        return name;
    }

    public String getPurposeOfMeet() {
        return purposeOfMeet;
    }

    public String getOrganisation() {
        return organisation;
    }

    public String getGovtID() {
        return govtID;
    }

    public String getIDImage() {
        return IDImage;
    }

    @Override
    public int compareTo(@NonNull NewVisitorItem o) {
        return inTime.compareTo(o.inTime);
    }
}
