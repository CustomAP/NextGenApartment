package nextgenentrycard.nextgenApartment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.daimajia.androidanimations.library.Techniques;
import com.viksaa.sssplash.lib.activity.AwesomeSplash;
import com.viksaa.sssplash.lib.cnst.Flags;
import com.viksaa.sssplash.lib.model.ConfigSplash;

import nextgenentrycard.nextgenApartment.Login.LoginActivity;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 19/10/17.
 */

public class SplashActivity extends AwesomeSplash {
    boolean isLoggedIn;

    @Override
    public void initSplash(ConfigSplash configSplash) {

        //Customize Circular Reveal
        configSplash.setBackgroundColor(R.color.colorPrimary); //any color you want form colors.xml
        configSplash.setAnimCircularRevealDuration(500); //int ms
        configSplash.setRevealFlagX(Flags.REVEAL_RIGHT);  //or Flags.REVEAL_LEFT
        configSplash.setRevealFlagY(Flags.REVEAL_BOTTOM); //or Flags.REVEAL_TOP

        //Customize Logo
        configSplash.setLogoSplash(R.mipmap.ic_launcher); //or any other drawable
        configSplash.setAnimLogoSplashDuration(500); //int ms
        configSplash.setAnimLogoSplashTechnique(Techniques.RollIn); //choose one form Techniques (ref: https://github.com/daimajia/AndroidViewAnimations)

        /*
            removing text view
         */
        configSplash.setTitleSplash("");
        configSplash.setAnimTitleDuration(0);
    }

    @Override
    public void animationsFinished() {
        SharedPreferences preferences = getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        isLoggedIn = preferences.getBoolean("isLoggedIn", false);
        Intent i;
        if (isLoggedIn) {
            i = new Intent(getApplicationContext(), MainActivity2.class);
            startActivity(i);
        } else {
            i = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(i);

        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }
}
