package nextgenentrycard.nextgenApartment.Sync;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import de.greenrobot.event.EventBus;
import nextgenentrycard.nextgenApartment.Database.DBHelper;
import nextgenentrycard.nextgenApartment.EventBus.ResidentsFetchEvent;
import nextgenentrycard.nextgenApartment.Models.DSPItem;
import nextgenentrycard.nextgenApartment.Models.DailyVisitorEntryItem;
import nextgenentrycard.nextgenApartment.Models.DepartmentItem;
import nextgenentrycard.nextgenApartment.Models.ResidentItem;
import nextgenentrycard.nextgenApartment.Models.NewVisitorItem;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 21/10/17.
 */

public class Sync {
    private AQuery aQuery;
    private Context context;
    private ArrayList<DepartmentItem> departmentItems;
    private ArrayList<ResidentItem> residentItems;
    private ArrayList<NewVisitorItem> newVisitorItems;
    private ArrayList<DSPItem> DSPItems;
    private ArrayList<DailyVisitorEntryItem> dailyVisitorEntryItems;

    private String getEmployees = "http://www.accesspoint.in/index.php/Sync/getResidents";
    private String getNewVisitors = "http://www.accesspoint.in/index.php/NewVisitor/getNewVisitorsForSecurity";
    private String getDSP = "http://www.accesspoint.in/index.php/DSP/getDSP";
    private String updateDailyVisitorLogs = "http://www.accesspoint.in/index.php/DSP/getDSPLog";
    private String getParkingCounter = "http://www.accesspoint.in/index.php/Parking/getCounter";

    public Sync(Context context) {
        this.context = context;
        aQuery = new AQuery(context);
    }

    public void getResidents() {
        residentItems = new ArrayList<>();

        final SharedPreferences preferences = context.getSharedPreferences("Employees", Context.MODE_PRIVATE);
        int IDMax = preferences.getInt("IDMax", 0);

        SharedPreferences preferences1 = context.getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        String sessionID = preferences1.getString("sessionID", null);

        HashMap<String, Object> employeeParams = new HashMap<>();
        employeeParams.put("sessionID", sessionID);
        employeeParams.put("IDMax", IDMax);

        aQuery.ajax(getEmployees, employeeParams, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                String employeeName, flatNum, wing;
                int employeeID, departmentID, num = 0;

                if (object != null) {
                    Log.d("employees", "object!=null");
                    try {
                        String employees = object.getString("employees");

                        JSONObject reader = new JSONObject(employees);
                        num = object.getInt("num");

                        for (int i = 0; i < num; i++) {
                            JSONObject obj = reader.getJSONObject(String.valueOf(i));
                            employeeName = obj.getString("residentName");
                            employeeID = obj.getInt("residentID");
                            flatNum = obj.getString("flatNum");
                            wing = obj.getString("wing");

                            residentItems.add(new ResidentItem(employeeID, employeeName, wing, flatNum));
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.d("employees", "object=null status:" +status.getCode());
                    EventBus.getDefault().post(new ResidentsFetchEvent(false));
                }

                if (status.getCode() == 200) {
                    DBHelper helper = new DBHelper(context);
                    for (int i = 0; i < num; i++)
                        helper.addEmployee(residentItems.get(i));
                    if (residentItems.size() != 0) {
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putInt("IDMax", residentItems.get(num - 1).getResidentID());
                        editor.apply();
                    }
                    EventBus.getDefault().post(new ResidentsFetchEvent(true));
                }
            }
        });
    }

    public void getNewVisitors() {
        newVisitorItems = new ArrayList<>();

        SharedPreferences preferences1 = context.getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        String sessionID = preferences1.getString("sessionID", null);

        HashMap<String, Object> newVisitorParams = new HashMap<>();
        newVisitorParams.put("sessionID", sessionID);

        aQuery.ajax(getNewVisitors, newVisitorParams, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                String newVisitors;
                int num = 0, exited;

                String date, inTime, outTime, purposeOfMeet, organisation, IDProof, IDNum, visitorName, type, mobNum, employeeName, flatNum, wing;
                int newVisitorID, employeeID;

                if (object != null) {
                    try {
                        Log.d("newVisitors", "object!=null");
                        newVisitors = object.getString("newVisitors");

                        num = object.getInt("num");

                        JSONObject reader = new JSONObject(newVisitors);

                        for (int i = 0; i < num; i++) {
                            JSONObject obj = reader.getJSONObject(String.valueOf(i));
                            inTime = obj.getString("inTime");
                            outTime = obj.getString("outTime");
                            date = obj.getString("date");
                            purposeOfMeet = obj.getString("purposeOfMeet");
                            organisation = obj.getString("organisation");
                            IDProof = obj.getString("IDproof");
                            IDNum = obj.getString("IDNo");
                            visitorName = obj.getString("visitorName");
                            type = obj.getString("type");
                            mobNum = obj.getString("mobNum");
                            newVisitorID = obj.getInt("newVisitorID");
                            employeeName = obj.getString("userName");
                            employeeID = obj.getInt("userID");
                            flatNum = obj.getString("flatNum");
                            wing = obj.getString("wing");

                            newVisitorItems.add(new NewVisitorItem(mobNum, visitorName, purposeOfMeet, organisation, IDProof, IDNum, type, employeeName, inTime, outTime, employeeID
                                    , newVisitorID, date,flatNum, wing));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.d("newVisitors", "object = null ,status :" + status.getCode());
                }

                if (status.getCode() == 200) {
                    if (num > 0) {

                        DBHelper helper = new DBHelper(context);

                        for (int i = 0; i < newVisitorItems.size(); i++)
                            helper.updateNewVisitorList(newVisitorItems.get(i));
                    }
                }
            }
        });
    }

    public void getDSPs() {
        DSPItems = new ArrayList<>();

        final SharedPreferences preferences = context.getSharedPreferences("DSP", Context.MODE_PRIVATE);
        int IDMax = preferences.getInt("IDMax", 0);

        SharedPreferences preferences1 = context.getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        String sessionID = preferences1.getString("sessionID", null);

        HashMap<String, Object> dailyVisitorParams = new HashMap<>();
        dailyVisitorParams.put("sessionID", sessionID);
        dailyVisitorParams.put("IDMax", IDMax);

        aQuery.ajax(getDSP, dailyVisitorParams, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                int num = 0;
                String dailyVisitor;

                String visitorName, IDProof, IDNum, mobNum, address, spouseName, spouseOccupation, familyMembers;
                int dailyVisitorID;

                if (object != null) {
                    Log.d("getdailyvisitors", "object!=null");
                    try {
                        dailyVisitor = object.getString("dsp");
                        num = object.getInt("num");

                        JSONObject reader = new JSONObject(dailyVisitor);

                        for (int i = 0; i < num; i++) {
                            JSONObject obj = reader.getJSONObject(String.valueOf(i));

                            dailyVisitorID = obj.getInt("dspID");
                            visitorName = obj.getString("name");
                            IDProof = obj.getString("IDProof");
                            IDNum = obj.getString("IDNum");
                            mobNum = obj.getString("mobNum");
                            address = obj.getString("address");
                            spouseName = obj.getString("spouse");
                            spouseOccupation = obj.getString("spouseOccupation");
                            familyMembers = obj.getString("familyMembers");

                            DSPItems.add(new DSPItem(dailyVisitorID, visitorName, IDProof, IDNum, mobNum, address, spouseName, spouseOccupation, familyMembers));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.d("getdailyvisitors", "object = null status:" + status.getCode());
                }

                if (status.getCode() == 200) {

                    if (num > 0) {
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putInt("IDMax", DSPItems.get(num - 1).getDspID());
                        editor.apply();

                        DBHelper helper = new DBHelper(context);

                        for (int i = 0; i < DSPItems.size(); i++)
                            helper.addDailyVisitor(DSPItems.get(i));

                    }
                }
            }
        });
    }

    public void updateDSPLog() {
        dailyVisitorEntryItems = new ArrayList<>();

        SharedPreferences preferences1 = context.getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        String sessionID = preferences1.getString("sessionID", null);

        HashMap<String, Object> dailyVisitorEntryParams = new HashMap<>();
        dailyVisitorEntryParams.put("sessionID", sessionID);

        aQuery.ajax(updateDailyVisitorLogs, dailyVisitorEntryParams, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                String dailyVisitorLogs;
                int num = 0;

                String dspName, inTime, outTime, date;
                int dailyVisitorID, dailyVisitorEntryID;

                if (object != null) {
                    Log.d("updateDailyVisitors", "object != null");
                    try {
                        dailyVisitorLogs = object.getString("dspLog");
                        num = object.getInt("num");

                        JSONObject reader = new JSONObject(dailyVisitorLogs);

                        for (int i = 0; i < num; i++) {
                            JSONObject obj = reader.getJSONObject(String.valueOf(i));

                            dspName = obj.getString("name");
                            inTime = obj.getString("inTime");
                            outTime = obj.getString("outTime");
                            date = obj.getString("date");
                            dailyVisitorEntryID = obj.getInt("dspEntryID");
                            dailyVisitorID = obj.getInt("dspID");

                            dailyVisitorEntryItems.add(new DailyVisitorEntryItem(dspName, dailyVisitorID, dailyVisitorEntryID, inTime, outTime, date));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.d("updateDailyVisitors", "object = null status:" + status.getCode());
                }

                if (status.getCode() == 200) {
                    DBHelper helper = new DBHelper(context);

                    for (int i = 0; i < dailyVisitorEntryItems.size(); i++)
                        helper.addDailyVisitorEntry(dailyVisitorEntryItems.get(i));
                }
            }
        });
    }

    public void getParkingCounter(){
        SharedPreferences sharedPreferences = context.getSharedPreferences("LoginDetails",Context.MODE_PRIVATE);
        String sessionID = sharedPreferences.getString("sessionID", null);

        HashMap<String, Object> parkingParams = new HashMap<>();
        parkingParams.put("sessionID", sessionID);

        aQuery.ajax(getParkingCounter, parkingParams, JSONObject.class, new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                int counter = 0, total = 0;

                if(object != null){
                    try{
                        Log.d("getParkingCounter", "object != null");
                        String parkingCounter = object.getString("parkingCount");

                        JSONObject reader = new JSONObject(parkingCounter);

//                        JSONObject obj = reader.getJSONObject(String.valueOf(0));

                        counter = reader.getInt("count");
                        total = reader.getInt("total");
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }else{
                    Log.d("getParkingCounter", "object = null");
                }

                if(status.getCode() == 200){
                    SharedPreferences preferences = context.getSharedPreferences("Parking", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor  = preferences.edit();
                    editor.putInt("count",counter);
                    editor.putInt("total",total);
                    editor.apply();
                }

            }
        });
    }
}
