package nextgenentrycard.nextgenApartment.DailyVisitor;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;

import nextgenentrycard.nextgenApartment.R;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 28/1/18.
 */

public class DailyVisitorActivity extends AppCompatActivity {

    FrameLayout flDailyVisitor;

    private static FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_visitor);
        setTitle("Daily Service Provider");

        flDailyVisitor = (FrameLayout)findViewById(R.id.flDailyVisitor);

        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.flDailyVisitor, new DailyVisitorFragment());
        fragmentTransaction.commit();
    }
}
