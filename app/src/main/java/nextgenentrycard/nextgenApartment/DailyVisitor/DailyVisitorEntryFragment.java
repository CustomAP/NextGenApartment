package nextgenentrycard.nextgenApartment.DailyVisitor;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import nextgenentrycard.nextgenApartment.Database.DBHelper;
import nextgenentrycard.nextgenApartment.Models.DailyVisitorDesignationItem;
import nextgenentrycard.nextgenApartment.Models.DSPItem;
import nextgenentrycard.nextgenApartment.R;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 30/10/17.
 */

public class DailyVisitorEntryFragment extends Fragment {
    ArrayList<DSPItem> DSPItems;
    RecyclerView rvDSP;
    CardView cvNoDailyVisitors;
    FloatingActionButton fabAddDailyVisitor;
    DailyVisitorEntryAdapter dailyVisitorEntryAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_daily_visitor_entry, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rvDSP = (RecyclerView) view.findViewById(R.id.rvDSP);
        cvNoDailyVisitors = (CardView) view.findViewById(R.id.cvNoDailyVisitors);
        fabAddDailyVisitor = (FloatingActionButton) view.findViewById(R.id.fabAddDailyVisitor_fragmentDailyVisitorEntry);

        DBHelper dbHelper = new DBHelper(getContext());
        DSPItems = dbHelper.getAllDailyVisitors();

        if(DSPItems.size() != 0)
            cvNoDailyVisitors.setVisibility(View.GONE);

        dailyVisitorEntryAdapter = new DailyVisitorEntryAdapter(DSPItems);

        rvDSP.setAdapter(dailyVisitorEntryAdapter);
        rvDSP.setLayoutManager(new LinearLayoutManager(getContext()));
        rvDSP.setItemAnimator(new DefaultItemAnimator());

        fabAddDailyVisitor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), AddDailyVisitorOTPActivity.class);
                startActivity(i);
            }
        });
    }
}
