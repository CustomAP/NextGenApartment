package nextgenentrycard.nextgenApartment.DailyVisitor;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import nextgenentrycard.nextgenApartment.Database.DBHelper;
import nextgenentrycard.nextgenApartment.Models.DailyVisitorEntryItem;
import nextgenentrycard.nextgenApartment.Models.DSPItem;
import nextgenentrycard.nextgenApartment.R;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 11/12/17.
 */

public class DailyVisitorLogAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<DailyVisitorEntryItem> dailyVisitorEntryItems;
    private Context context;

    public DailyVisitorLogAdapter(ArrayList<DailyVisitorEntryItem> dailyVisitorEntryItems){
        this.dailyVisitorEntryItems = dailyVisitorEntryItems;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.daily_visitor_log_single_item,parent,false);
        return new DailyVisitorLogHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final DailyVisitorLogHolder dailyVisitorLogHolder = (DailyVisitorLogHolder)holder;
        context = dailyVisitorLogHolder.tvInTime.getContext();

        dailyVisitorLogHolder.tvInTime.setText(dailyVisitorEntryItems.get(position).getInTime());
        dailyVisitorLogHolder.tvVisitorName.setText(dailyVisitorEntryItems.get(position).getVisitorName());

        final DBHelper dbHelper = new DBHelper(context);

        dailyVisitorLogHolder.cvDailyVisitorLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DSPItem DSPItem = dbHelper.getDailyVisitor(dailyVisitorEntryItems.get(dailyVisitorLogHolder.getAdapterPosition()).getDailyVisitorID());
                Log.d("daily visitor id",dailyVisitorEntryItems.get(dailyVisitorLogHolder.getAdapterPosition()).getDailyVisitorID()+"");

                Intent i = new Intent(context, ViewDailyVisitorLogActivity.class);
                i.putExtra("DSPItem", DSPItem);
                i.putExtra("dailyVisitorEntryItem",dailyVisitorEntryItems.get(dailyVisitorLogHolder.getAdapterPosition()));
                context.startActivity(i);
            }
        });

    }

    @Override
    public int getItemCount() {
        return dailyVisitorEntryItems.size();
    }

    private class DailyVisitorLogHolder extends RecyclerView.ViewHolder {
        TextView tvVisitorName, tvInTime;
        CardView cvDailyVisitorLog;
        public DailyVisitorLogHolder(View itemView) {
            super(itemView);
            tvInTime = (TextView)itemView.findViewById(R.id.tvInTime_dailyVisitorLogSingleItem);
            tvVisitorName = (TextView)itemView.findViewById(R.id.tvVisitorName_dailyVisitorLogSingleItem);
            cvDailyVisitorLog = (CardView)itemView.findViewById(R.id.cvDailyVisitorLog);
        }

    }

    public void update(ArrayList<DailyVisitorEntryItem> dailyVisitorEntryItems){
        this.dailyVisitorEntryItems = dailyVisitorEntryItems;
        this.notifyDataSetChanged();
    }
}
