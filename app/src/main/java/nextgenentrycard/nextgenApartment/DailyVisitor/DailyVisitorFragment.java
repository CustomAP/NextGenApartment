package nextgenentrycard.nextgenApartment.DailyVisitor;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import nextgenentrycard.nextgenApartment.R;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 11/12/17.
 */

public class DailyVisitorFragment extends Fragment {
    private ViewPager vpDailyVisitor;
    private TabLayout tlDailyVisitor;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_daily_visitor,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        vpDailyVisitor=(ViewPager)view.findViewById(R.id.vpDailyVisitor);
        tlDailyVisitor=(TabLayout)view.findViewById(R.id.tlDailyVisitors);

        DailyVisitorPagerAdapter dailyVisitorPagerAdapter = new DailyVisitorPagerAdapter(getChildFragmentManager());

        vpDailyVisitor.setAdapter(dailyVisitorPagerAdapter);
        tlDailyVisitor.setupWithViewPager(vpDailyVisitor);
    }

    private class DailyVisitorPagerAdapter extends FragmentStatePagerAdapter{

        DailyVisitorPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position){
                case 0:
                        return new DailyVisitorEntryFragment();
                case 1:
                        return new DailyVisitorLogsFragment();
            }

            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position){
                case 0:
                    return "Entry";
                case 1:
                    return "Logs";
            }
            return null;
        }
    }
}
