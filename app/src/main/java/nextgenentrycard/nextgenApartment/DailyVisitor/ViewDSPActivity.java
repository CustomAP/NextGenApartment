package nextgenentrycard.nextgenApartment.DailyVisitor;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

import nextgenentrycard.nextgenApartment.Models.DSPItem;
import nextgenentrycard.nextgenApartment.Models.DSPLinkItem;
import nextgenentrycard.nextgenApartment.R;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 12/3/18.
 */

public class ViewDSPActivity extends AppCompatActivity {
    ImageView ivPhoto, ivCall;
    TextView tvName;
    RecyclerView rvDSPLink;

    ArrayList<DSPLinkItem> dspLinkItems;

    DSPItem dspItem;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_dsp);
        setTitle("Daily Service Provider");

        initialize();

    }

    private void initialize() {
        dspItem = (DSPItem)getIntent().getSerializableExtra("dspItem");
        dspLinkItems = (ArrayList<DSPLinkItem>)getIntent().getSerializableExtra("dspLinkItems");

        ivCall = (ImageView)findViewById(R.id.bCall_viewDSP);
        ivPhoto = (ImageView)findViewById(R.id.ivPhoto_viewDSP);
        tvName = (TextView)findViewById(R.id.tvName_viewDSP);
        rvDSPLink = (RecyclerView)findViewById(R.id.rvDSPLink);

        DSPLinkAdapter dspLinkAdapter = new DSPLinkAdapter(dspLinkItems);

        rvDSPLink.setAdapter(dspLinkAdapter);
        rvDSPLink.setItemAnimator(new DefaultItemAnimator());
        rvDSPLink.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        tvName.setText(dspItem.getName());

        Drawable defaultProfPic = getApplicationContext().getResources().getDrawable(R.drawable.generic_profile);
        Glide.with(getApplicationContext()).load("http://www.accesspoint.in/dsp/"+ dspItem.getDspID()+".JPG").asBitmap().centerCrop().error(defaultProfPic).diskCacheStrategy(DiskCacheStrategy.NONE).into(ivPhoto);

        ivCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + dspItem.getMobileNum()));
                startActivity(intent);
            }
        });
    }


}
