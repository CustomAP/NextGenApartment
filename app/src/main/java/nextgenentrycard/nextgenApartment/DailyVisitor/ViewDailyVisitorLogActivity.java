package nextgenentrycard.nextgenApartment.DailyVisitor;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import nextgenentrycard.nextgenApartment.Models.DSPItem;
import nextgenentrycard.nextgenApartment.Models.DailyVisitorEntryItem;
import nextgenentrycard.nextgenApartment.PicDailog;
import nextgenentrycard.nextgenApartment.R;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 11/12/17.
 */

public class ViewDailyVisitorLogActivity extends AppCompatActivity {
    DSPItem DSPItem;
    DailyVisitorEntryItem dailyVisitorEntryItem;

    TextView tvName, tvIntime, tvOutTime, tvCompanyName, tvSecurityName;
    ImageView ivPhoto;
    FloatingActionButton bOkay, bCall;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_daily_visitor_log);
        setTitle("Daily Service Provider");

        dailyVisitorEntryItem = (DailyVisitorEntryItem)getIntent().getSerializableExtra("dailyVisitorEntryItem");
        DSPItem = (DSPItem)getIntent().getSerializableExtra("DSPItem");

         /*
            initializing views
         */
        tvName=(TextView)findViewById(R.id.tvName_viewDailyVisitorLog);
        ivPhoto=(ImageView) findViewById(R.id.ivPhoto_viewDailyVisitorLog);
        bOkay=(FloatingActionButton) findViewById(R.id.bOkay_viewDailyVisitorLog);
        tvIntime = (TextView)findViewById(R.id.tvInTime_viewDailyVisitorLog);
        tvOutTime = (TextView)findViewById(R.id.tvOutTime_viewDailyVisitorLog);
        bCall = (FloatingActionButton)findViewById(R.id.fabCall_dailyVisitorLog);
        tvCompanyName = (TextView)findViewById(R.id.tvCompanyName_viewDSPLog);
        tvSecurityName = (TextView) findViewById(R.id.tvSecurityName_viewDSPLog);

        /*
            setting fields
         */
        tvName.setText(DSPItem.getName());
        tvIntime.setText(dailyVisitorEntryItem.getInTime());

        if(dailyVisitorEntryItem.getOutTime()!=null)
            tvOutTime.setText(dailyVisitorEntryItem.getOutTime());


        /*
            setting on click listener
         */
        bOkay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ivPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), PicDailog.class);
//                i.putExtra("path", Environment.getExternalStorageDirectory().getPath()+"/NextGen/DailyVisitor/");
//                i.putExtra("fileName",DSPItem.getDspID()+".jpg");
                i.putExtra("origin","dailyVisitor");
                i.putExtra("dailyVisitorID", DSPItem.getDspID());
                startActivity(i);
            }
        });

        bCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + DSPItem.getMobileNum()));
                startActivity(intent);
            }
        });

        SharedPreferences sharedPreferences = getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        tvSecurityName.setText(sharedPreferences.getString("securityName", ""));
        tvCompanyName.setText(sharedPreferences.getString("apartmentName", ""));

        loadImage();
    }

    public void loadImage(){
        try {
//            String path = Environment.getExternalStorageDirectory().getPath()+"/NextGen/DailyVisitor/";
//            File f = new File(path, DSPItem.getDspID()+".jpg");
            Drawable defaultProfPic=getResources().getDrawable(R.drawable.generic_profile);
            Glide.with(this).load("http://www.accesspoint.in/dsp/"+ DSPItem.getDspID()+".JPG").asBitmap().centerCrop().error(defaultProfPic)
                    .listener(new RequestListener<String, Bitmap>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                            Log.d("glideException",e.toString());
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            return false;
                        }
                    }).into(ivPhoto);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
