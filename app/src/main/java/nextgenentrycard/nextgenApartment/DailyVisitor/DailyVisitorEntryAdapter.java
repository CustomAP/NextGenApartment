package nextgenentrycard.nextgenApartment.DailyVisitor;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.yarolegovich.lovelydialog.LovelyProgressDialog;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import de.greenrobot.event.EventBus;
import nextgenentrycard.nextgenApartment.Database.DBHelper;
import nextgenentrycard.nextgenApartment.EventBus.DailyVisitorEntryExitEvent;
import nextgenentrycard.nextgenApartment.Models.DSPLinkItem;
import nextgenentrycard.nextgenApartment.Models.DailyVisitorEntryItem;
import nextgenentrycard.nextgenApartment.Models.DSPItem;
import nextgenentrycard.nextgenApartment.R;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 30/10/17.
 */

public class DailyVisitorEntryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<DSPItem> DSPItems;
    private ArrayList<DSPLinkItem> dspLinkItems;
    private Context context;
    private AQuery aQuery;
    private Dialog progressDialog;

    private String inDailyVisitor="http://www.accesspoint.in/index.php/DSP/inDSP";
    private String outDailyVisitor="http://www.accesspoint.in/index.php/DSP/outDSP";
    private String getDSPLink = "http://www.accesspoint.in/index.php/DSP/getDSPLink";

    public DailyVisitorEntryAdapter(ArrayList<DSPItem> DSPItems){
        this.DSPItems = DSPItems;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.daily_visitor_entry_single_item,parent,false);
        return new DailyVisitorHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final DailyVisitorHolder dailyVisitorHolder=(DailyVisitorHolder)holder;
        context=dailyVisitorHolder.bInOut.getContext();
        aQuery=new AQuery(context);
        dspLinkItems = new ArrayList<>();

        /*
            setting text
         */
        dailyVisitorHolder.tvVisitorName.setText(DSPItems.get(dailyVisitorHolder.getAdapterPosition()).getName());

        /*
            getting daily visitor entry ID,
                if zero means a there is no IN entry
                       else there is an IN entry and user should get option to OUT the visitor
         */
        final DBHelper dbHelper=new DBHelper(context);
        dbHelper.getDailyVisitorEntryID(DSPItems.get(dailyVisitorHolder.getAdapterPosition()).getDspID());

        Log.d("dailyVisitorEntryID",dbHelper.getDailyVisitorEntryID(DSPItems.get(dailyVisitorHolder.getAdapterPosition()).getDspID())+"");

        /*
            setting button text
         */
        if(dbHelper.getDailyVisitorEntryID(DSPItems.get(dailyVisitorHolder.getAdapterPosition()).getDspID())==0){
            dailyVisitorHolder.bInOut.setText("IN");
        }else{
            dailyVisitorHolder.bInOut.setText("OUT");
        }

        Log.d("dailyVisitorEntryID", dbHelper.getDailyVisitorEntryID(DSPItems.get(dailyVisitorHolder.getAdapterPosition()).getDspID()) + "");
        /*
            getting shared preferences
         */
        SharedPreferences sharedPreferences=context.getSharedPreferences("LoginDetails",Context.MODE_PRIVATE);
        final String sessionID=sharedPreferences.getString("sessionID",null);


        /*
            setting on click listeners
         */
        dailyVisitorHolder.llDailyVisitor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dspLinkItems.clear();

                progressDialog = new LovelyProgressDialog(context)
                        .setCancelable(true)
                        .setTopColorRes(R.color.colorPrimary)
                        .setIcon(R.drawable.wait)
                        .show();

                HashMap<String, Object> dspLinkParams = new HashMap<>();
                dspLinkParams.put("sessionID", sessionID);
                dspLinkParams.put("dspID", DSPItems.get(dailyVisitorHolder.getAdapterPosition()).getDspID());

                aQuery.ajax(getDSPLink, dspLinkParams, JSONObject.class, new AjaxCallback<JSONObject>(){
                    @Override
                    public void callback(String url, JSONObject object, AjaxStatus status) {
                        String dspLinks, work, userName, flatNum, wing;
                        int num, dspLinkID, residentID, dspID;

                        if(object != null){
                            try{
                                Log.d("getDspLink", "object != null:");
                                dspLinks = object.getString("dspLinks");
                                num = object.getInt("num");

                                JSONObject reader = new JSONObject(dspLinks);

                                for(int i = 0; i < num; i++){
                                    JSONObject obj = reader.getJSONObject(String.valueOf(i));
                                    work = obj.getString("work");
                                    userName = obj.getString("userName");
                                    flatNum = obj.getString("flatNum");
                                    wing = obj.getString("wing");
                                    dspLinkID = obj.getInt("dspLinkID");
                                    residentID = obj.getInt("residentID");
                                    dspID = obj.getInt("dspID");

                                    dspLinkItems.add(new DSPLinkItem(dspID, dspLinkID, residentID, userName, flatNum, wing, work));
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }else{
                            Log.d("getDspLink", "object = null status:" + status.getCode());
                            Toast.makeText(context, "Failed to get Daily Service Provider details", Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                        }

                        if(status.getCode() == 200){
                            progressDialog.dismiss();

                            Intent i = new Intent(context,ViewDSPActivity.class);
                            i.putExtra("dspItem",DSPItems.get(dailyVisitorHolder.getAdapterPosition()));
                            i.putExtra("dspLinkItems", dspLinkItems);
                            context.startActivity(i);
                        }
                    }
                });

            }
        });


        dailyVisitorHolder.bInOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(dbHelper.getDailyVisitorEntryID(DSPItems.get(dailyVisitorHolder.getAdapterPosition()).getDspID())==0){
                    /*
                        if visitor not IN,
                            IN the visitor
                     */
                    Log.d("sessionID",sessionID);
                    Log.d("dailyVisitorID", DSPItems.get(dailyVisitorHolder.getAdapterPosition()).getDspID()+"");

                    HashMap<String,Object> inParams=new HashMap<String, Object>();
                    inParams.put("sessionID",sessionID);
                    inParams.put("dspID", DSPItems.get(dailyVisitorHolder.getAdapterPosition()).getDspID());

                    aQuery.ajax(inDailyVisitor,inParams, JSONObject.class,new AjaxCallback<JSONObject>(){
                        @Override
                        public void callback(String url, JSONObject object, AjaxStatus status) {
                            int dailyVisitorEntryId=0;
                            String time=null;

                            if(object!=null){
                                try{
                                    Log.d("inDailyVisitor","object!=null");
                                    dailyVisitorEntryId=object.getInt("dspEntryID");
                                    time=object.getString("time");
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                            }else{
                                Log.d("inDailyVisitor","object=null "+status.getCode());
                                Toast.makeText(context,"Failed to send IN request",Toast.LENGTH_SHORT).show();
                            }

                            if(status.getCode()==200){
                                /*
                                    getting date
                                 */
                                Date date=new Date();
                                SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
                                String dateString=sdf.format(date);

                                /*
                                    inserting in db
                                 */
                                dbHelper.addDailyVisitorEntry(new DailyVisitorEntryItem(
                                        DSPItems.get(dailyVisitorHolder.getAdapterPosition()).getName(),
                                        DSPItems.get(dailyVisitorHolder.getAdapterPosition()).getDspID(),
                                        dailyVisitorEntryId,
                                        time,
                                        dateString
                                        ));

                               /*
                                    setting text to OUT
                                 */
                                dailyVisitorHolder.bInOut.setText("OUT");

                                EventBus.getDefault().post(new DailyVisitorEntryExitEvent(true));
                            }
                        }
                    });
                }else{
                    /*
                        daily visitor already IN,
                            need to OUT the visitor
                     */
                    Log.d("dailyVisitorEntry", dbHelper.getDailyVisitorEntryID(DSPItems.get(dailyVisitorHolder.getAdapterPosition()).getDspID()) + " aa");

                    HashMap<String,Object> outParams=new HashMap<String, Object>();
                    outParams.put("sessionID",sessionID);
                    outParams.put("dspEntryID",dbHelper.getDailyVisitorEntryID(DSPItems.get(dailyVisitorHolder.getAdapterPosition()).getDspID()));

                    aQuery.ajax(outDailyVisitor,outParams,JSONObject.class,new AjaxCallback<JSONObject>(){
                        @Override
                        public void callback(String url, JSONObject object, AjaxStatus status) {
                            String time=null;
                            if(object!=null){
                                try{
                                    Log.d("outDailyVisitor","object!=null");
                                    time=object.getString("time");
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                            }else{
                                Log.d("outDailyVisitor","object=null");
                                Toast.makeText(context,"Failed to send OUT request",Toast.LENGTH_SHORT).show();
                            }

                            if(status.getCode()==200){
                                /*
                                    updating OUT entry in DB
                                 */
                                DBHelper helper=new DBHelper(context);
                                helper.outDailyVisitor(dbHelper.getDailyVisitorEntryID(DSPItems.get(dailyVisitorHolder.getAdapterPosition()).getDspID()),time);

                                /*
                                    setting text to IN
                                 */
                                dailyVisitorHolder.bInOut.setText("IN");


                                EventBus.getDefault().post(new DailyVisitorEntryExitEvent(true));
                            }
                        }
                    });
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return DSPItems.size();
    }

    public class DailyVisitorHolder extends RecyclerView.ViewHolder {
        TextView tvVisitorName;
        TextView bInOut;
        LinearLayout llDailyVisitor;

        public DailyVisitorHolder(View itemView) {
            super(itemView);
            tvVisitorName=(TextView)itemView.findViewById(R.id.tvVisitorName_dailyVisitorSingleItem);
            bInOut=(TextView) itemView.findViewById(R.id.bInOut_dailyVisitorSingleItem);
            llDailyVisitor=(LinearLayout)itemView.findViewById(R.id.llDailyVisitorSingleItem);
        }
    }
}
