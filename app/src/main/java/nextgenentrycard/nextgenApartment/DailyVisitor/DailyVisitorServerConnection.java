package nextgenentrycard.nextgenApartment.DailyVisitor;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;

import java.util.HashMap;

import de.greenrobot.event.EventBus;
import nextgenentrycard.nextgenApartment.Database.DBHelper;
import nextgenentrycard.nextgenApartment.EventBus.DailyVisitorAddEvent;
import nextgenentrycard.nextgenApartment.Models.DSPItem;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 29/10/17.
 */

public class DailyVisitorServerConnection {
    private AQuery aQuery;
    private Context context;

    String addDailyVisitor="http://www.accesspoint.in/index.php/DSP/addDSP";

    public DailyVisitorServerConnection(Context context){
        this.context=context;
        aQuery=new AQuery(context);
    }

    public void addDailyVisitor(final DSPItem DSPItem, String image){
        SharedPreferences preferences=context.getSharedPreferences("LoginDetails",Context.MODE_PRIVATE);
        String sessionID=preferences.getString("sessionID",null);

        HashMap<String,Object> addDailyVisitorParams=new HashMap<>();
        addDailyVisitorParams.put("name", DSPItem.getName());
        addDailyVisitorParams.put("organisation", DSPItem.getAddress());
        addDailyVisitorParams.put("IDProof", DSPItem.getIDProof());
        addDailyVisitorParams.put("IDNum", DSPItem.getIDNum());
        addDailyVisitorParams.put("mobileNum", DSPItem.getMobileNum());
        addDailyVisitorParams.put("image",image);
        addDailyVisitorParams.put("address", DSPItem.getAddress());
        addDailyVisitorParams.put("spouseName", DSPItem.getSpouseName());
        addDailyVisitorParams.put("spouseOccupation", DSPItem.getSpouseOccupation());
        addDailyVisitorParams.put("familyMembers", DSPItem.getFamilyMembers());
        addDailyVisitorParams.put("sessionID",sessionID);

        aQuery.ajax(addDailyVisitor,addDailyVisitorParams, JSONObject.class,new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                int dailyVisitorID=0;

                if(object!=null){
                    try{
                        Log.d("addDSP","object!=null");
                        dailyVisitorID=object.getInt("dspID");
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }else{
                    Log.d("addDSP","object=null "+status.getCode());
                    Toast.makeText(context,"Failed to add Visitor. Try again",Toast.LENGTH_SHORT).show();
                }

                if(status.getCode()==200){
                    DSPItem.setDspID(dailyVisitorID);
                    DBHelper dbHelper=new DBHelper(context);
                    dbHelper.addDailyVisitor(DSPItem);

                    EventBus.getDefault().post(new DailyVisitorAddEvent(true,dailyVisitorID));
                }
            }
        });
    }
}
