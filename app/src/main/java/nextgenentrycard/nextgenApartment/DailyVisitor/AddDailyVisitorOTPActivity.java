package nextgenentrycard.nextgenApartment.DailyVisitor;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import de.greenrobot.event.EventBus;
import nextgenentrycard.nextgenApartment.EventBus.DailyVisitorAddEvent;
import nextgenentrycard.nextgenApartment.Models.DSPItem;
import nextgenentrycard.nextgenApartment.OTP.OTPFragment;
import nextgenentrycard.nextgenApartment.R;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 2/2/18.
 */

public class AddDailyVisitorOTPActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        setTitle("OTP");

        EventBus.getDefault().register(this);

        DSPItem DSPItem = new DSPItem();
        Bundle args = new Bundle();
        args.putSerializable("DSPItem", DSPItem);
        args.putString("origin", "DailyVisitor");

        OTPFragment otpFragment = new OTPFragment();
        otpFragment.setArguments(args);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.flOTPActivity, otpFragment);
        fragmentTransaction.commit();
    }

    public void onEvent(DailyVisitorAddEvent dailyVisitorAddEvent){
        if(dailyVisitorAddEvent.isDailyVisitorAdded())
            finish();
    }
}
