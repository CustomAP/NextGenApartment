package nextgenentrycard.nextgenApartment.DailyVisitor;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import de.greenrobot.event.EventBus;
import nextgenentrycard.nextgenApartment.Database.DBHelper;
import nextgenentrycard.nextgenApartment.EventBus.DailyVisitorEntryExitEvent;
import nextgenentrycard.nextgenApartment.Models.DailyVisitorEntryItem;
import nextgenentrycard.nextgenApartment.R;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 11/12/17.
 */

public class DailyVisitorLogsFragment extends Fragment {
    RecyclerView rvDailyVisitorsLog;
    ArrayList<DailyVisitorEntryItem> dailyVisitorEntryItems;
    DailyVisitorLogAdapter dailyVisitorLogAdapter;

    private Context context;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_daily_visitor_logs,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        rvDailyVisitorsLog = (RecyclerView)view.findViewById(R.id.rvDailyvisitorLogs);

        DBHelper dbHelper = new DBHelper(getContext());
        EventBus.getDefault().register(this);
        this.context = getContext();

        Date date = new Date();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = simpleDateFormat.format(date);


        dailyVisitorEntryItems = dbHelper.getDailyVisitorsForToday(dateString);

        dailyVisitorLogAdapter = new DailyVisitorLogAdapter(dailyVisitorEntryItems);

        rvDailyVisitorsLog.setAdapter(dailyVisitorLogAdapter);
        rvDailyVisitorsLog.setLayoutManager(new LinearLayoutManager(getContext()));
        rvDailyVisitorsLog.setItemAnimator(new DefaultItemAnimator());
    }

    public void onEvent(DailyVisitorEntryExitEvent dailyVisitorEntryExitEvent){
        if(dailyVisitorEntryExitEvent.isExitedEntered()){
            DBHelper dbHelper = new DBHelper(context);

            Date date = new Date();

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String dateString = simpleDateFormat.format(date);

            dailyVisitorEntryItems = dbHelper.getDailyVisitorsForToday(dateString);

            dailyVisitorLogAdapter.update(dailyVisitorEntryItems);
        }
    }
}
