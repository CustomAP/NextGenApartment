package nextgenentrycard.nextgenApartment.DailyVisitor;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import nextgenentrycard.nextgenApartment.Models.DSPLinkItem;
import nextgenentrycard.nextgenApartment.R;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 13/3/18.
 */

public class DSPLinkAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<DSPLinkItem> dspLinkItems;

    public DSPLinkAdapter(ArrayList<DSPLinkItem> dspLinkItems){
        this.dspLinkItems = dspLinkItems;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.dsp_link_single_item, viewGroup, false);
        return new DSPLinkHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        DSPLinkHolder dspLinkHolder = (DSPLinkHolder)viewHolder;

        dspLinkHolder.tvWork.setText(dspLinkItems.get(dspLinkHolder.getAdapterPosition()).getWork());
        dspLinkHolder.tvFlatNum.setText(dspLinkItems.get(dspLinkHolder.getAdapterPosition()).getWing() + " " + dspLinkItems.get(dspLinkHolder.getAdapterPosition()).getFlatNum());

    }

    @Override
    public int getItemCount() {
        return dspLinkItems.size();
    }

    public class DSPLinkHolder extends RecyclerView.ViewHolder {
        TextView tvFlatNum, tvWork;

        public DSPLinkHolder(View itemView) {
            super(itemView);
            tvFlatNum = (TextView)itemView.findViewById(R.id.tvFlatNum_dspLinkSingleItem);
            tvWork = (TextView)itemView.findViewById(R.id.tvWork_dspLinkSingleItem);
        }
    }
}
