package nextgenentrycard.nextgenApartment.DailyVisitor;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.github.jorgecastilloprz.FABProgressCircle;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;

import de.greenrobot.event.EventBus;
import nextgenentrycard.nextgenApartment.EventBus.DailyVisitorAddEvent;
import nextgenentrycard.nextgenApartment.EventBus.SessionEndEvent;
import nextgenentrycard.nextgenApartment.Models.DSPItem;
import nextgenentrycard.nextgenApartment.R;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 29/10/17.
 */

@RuntimePermissions
public class AddDailyVisitorDetailsActivity extends AppCompatActivity implements View.OnClickListener {
    DSPItem DSPItem;
    ImageView ivPhoto;
    EditText etName, etIDNum, etSpouseName, etSpouseOccupation, etFamilyMembers, etAddress;
    TextView tvIDProof, tvApartmentName, tvSecurityName;
    ImageButton bSubmit;
    Spinner sIDProof;
    String[] govtIDProofs;
    Uri selectedImage = null;
    private static final int CAMERA_REQUEST = 1888;
    FABProgressCircle fpcAddDailyVisitor;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_daily_visitor);
        setTitle("Add Daily Visitor");
        initialize();

    }

    private void initialize() {
        /*
            getting daily visitor item
         */
        DSPItem = (DSPItem) getIntent().getSerializableExtra("DSPItem");

        /*
            initializing views
         */
        ivPhoto = (ImageView) findViewById(R.id.ivPhoto_addDailyVisitor);
        etName = (EditText) findViewById(R.id.etName_addDailyVisitor);
        etIDNum = (EditText) findViewById(R.id.etIDNum_addDailyVisitor);
        tvIDProof = (TextView) findViewById(R.id.tvIDProof_addDailyVisitor);
        bSubmit = (ImageButton) findViewById(R.id.bSubmit_addDailyVisitor);
        sIDProof = (Spinner) findViewById(R.id.sIDProof_addDailyVisitor);
        fpcAddDailyVisitor = (FABProgressCircle) findViewById(R.id.fabProgress_AddDailyVisitor);
        etSpouseName = (MaterialEditText) findViewById(R.id.etSpouseName_addDailyVisitor);
        etSpouseOccupation = (MaterialEditText) findViewById(R.id.etSpouseOccupation_addDailyVisitor);
        etFamilyMembers = (MaterialEditText) findViewById(R.id.etFamilyMembers_addDailyVisitor);
        etAddress = (MaterialEditText) findViewById(R.id.etAddress_addDailyVisitor);
        tvApartmentName = (TextView)findViewById(R.id.tvApartmentName_addDSP);
        tvSecurityName = (TextView)findViewById(R.id.tvSecuritName_addDSP);


        SharedPreferences sharedPreferences = getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        tvApartmentName.setText(sharedPreferences.getString("apartmentName", ""));
        tvSecurityName.setText(sharedPreferences.getString("securityName", ""));

         /*
            setting govt ID proof spinner
         */

        govtIDProofs = new String[]{"Aadhar", "Driving License", "Pan Card", "Passport", "Election Card", "Ration Card"};

        Arrays.sort(govtIDProofs);

        ArrayAdapter<String> govtIDProofsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, govtIDProofs);

        sIDProof.setAdapter(govtIDProofsAdapter);

        tvIDProof.setOnClickListener(this);
        bSubmit.setOnClickListener(this);
        ivPhoto.setOnClickListener(this);

        EventBus.getDefault().register(this);

        Thread focus = new Thread() {
            public void run() {
                try {
                    sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            etName.requestFocus();
                        }
                    });
                }
            }
        };

        focus.start();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.tvIDProof_addDailyVisitor:
                tvIDProof.setVisibility(View.GONE);
                sIDProof.setVisibility(View.VISIBLE);
                sIDProof.performClick();
                break;

            case R.id.bSubmit_addDailyVisitor:

                String name = etName.getText().toString();
                String IDNum = etIDNum.getText().toString();
                String address = etAddress.getText().toString();
                String familyMembers = etFamilyMembers.getText().toString();
                String spouseName = etSpouseName.getText().toString();
                String spouseOccupation = etSpouseOccupation.getText().toString();

                if (!name.equals("")) {
                    if (tvIDProof.getVisibility() == View.GONE) {
                        if (!spouseName.equals("")) {
                            if (!spouseOccupation.equals("")) {
                                if(!familyMembers.equals("")) {
                                    if (!IDNum.equals("")) {
                                        if (selectedImage != null) {
                                            fpcAddDailyVisitor.show();

                                            bSubmit.setEnabled(false);
                                            bSubmit.setClickable(false);
                                            bSubmit.setFocusable(false);

                                            DSPItem.setName(name);
                                            DSPItem.setIDNum(IDNum);
                                            DSPItem.setIDProof(sIDProof.getSelectedItem().toString());
                                            DSPItem.setAddress(address);
                                            DSPItem.setFamilyMembers(familyMembers);
                                            DSPItem.setSpouseName(spouseName);
                                            DSPItem.setSpouseOccupation(spouseOccupation);

                                            try {
                                            /*
                                                encoding image to base64
                                             */
                                                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);


                                                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                                                bitmap.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
                                                String encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);

                                            /*
                                                uploading visitor details
                                             */
                                                DailyVisitorServerConnection dailyVisitorServerConnection = new DailyVisitorServerConnection(getApplicationContext());
                                                dailyVisitorServerConnection.addDailyVisitor(DSPItem, encodedImage);

                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }
                                        } else {
                                            Toast.makeText(this, "Please click a photo", Toast.LENGTH_SHORT).show();
                                        }
                                    } else {
                                        Toast.makeText(getApplicationContext(), "Please enter ID Number", Toast.LENGTH_SHORT).show();
                                    }
                                }else{
                                    Toast.makeText(getApplicationContext(), "Please enter family members", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), "Please enter Spouse Occupation", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "Please enter Spouse Name", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Please select ID Proof", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Please enter Name", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.ivPhoto_addDailyVisitor:
                AddDailyVisitorDetailsActivityPermissionsDispatcher.getPhotoWithPermissionCheck(this);
                break;
        }
    }

    @NeedsPermission({Manifest.permission.WRITE_EXTERNAL_STORAGE})
    public void getPhoto() {
        /*
                    opening camera
                 */
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, CAMERA_REQUEST);
    }

    // Annotate a method which is invoked if the user doesn't grant the permissions
    @OnPermissionDenied(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    void showDeniedForReadContacts() {
        Toast.makeText(this, "Permission was denied\nPlease give the required permissions and restart", Toast.LENGTH_SHORT).show();       //if permission is denied       need to add snackbar
    }

    // Annotates a method which is invoked if the user
    // chose to have the device "never ask again" about a permission
    @OnNeverAskAgain(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    void showNeverAskForReadContacts() {
        Toast.makeText(this, "Please give required Permissions from Settings App", Toast.LENGTH_SHORT).show();       //if never ask again pressed       //need to add snackbar with go to settings
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {         //overriden method for hancling various events after permission dialog
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);                       //common method for both reading contacts and call logs
        // NOTE: delegate the permission handling to generated method
        AddDailyVisitorDetailsActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);      //handled automoatically by the library's auto generated method
    }

    private void showRationaleDialog(@StringRes int messageResId, final PermissionRequest request) {        //showing the dialog to tell the reason of permission
        new AlertDialog.Builder(this)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(@NonNull DialogInterface dialog, int which) {
                        request.proceed();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(@NonNull DialogInterface dialog, int which) {
                        request.cancel();
                    }
                })
                .setCancelable(false)
                .setMessage(messageResId)
                .show();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        /*
            Image received from the camera
         */
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            selectedImage = getImageUri(this, (Bitmap) data.getExtras().get("data"));
            Glide.with(this).load(selectedImage).asBitmap().centerCrop().diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true).into(ivPhoto);
        }
    }

    public void onEvent(DailyVisitorAddEvent dailyVisitorAddEvent) {

        bSubmit.setEnabled(true);
        bSubmit.setClickable(true);
        bSubmit.setFocusable(true);
        fpcAddDailyVisitor.hide();

        if (dailyVisitorAddEvent.isDailyVisitorAdded()) {
            new LovelyStandardDialog(this, R.style.myDialog)
                    .setTopColorRes(R.color.colorPrimary)
                    .setButtonsColorRes(R.color.black)
                    .setIcon(R.mipmap.done)
                    .setTitle(R.string.info)
                    .setMessage(R.string.daily_visitor_add_feedback)
                    .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            finish();
                        }
                    })
                    .show();
            try {
                Bitmap image = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                createDirectoryAndSaveFile(image, dailyVisitorAddEvent.getDailyVisitorID() + "");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void createDirectoryAndSaveFile(final Bitmap imageToSave, final String fileName) {

        Thread run = new Thread() {
            public void run() {


                File direct = new File(Environment.getExternalStorageDirectory() + "/DirName");

                if (!direct.exists()) {
                    File wallpaperDirectory = new File(Environment.getExternalStorageDirectory().getPath() + "/NextGen/DSP/");
                    wallpaperDirectory.mkdirs();
                }

                File file = new File(new File(Environment.getExternalStorageDirectory().getPath() + "/NextGen/DSP/"), fileName + ".jpg");
                if (file.exists()) {
                    file.delete();
                }
                try {
                    FileOutputStream out = new FileOutputStream(file);
                    imageToSave.compress(Bitmap.CompressFormat.JPEG, 100, out);
                    out.flush();
                    out.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        };

        run.run();
    }

    private Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "title", null);
        return Uri.parse(path);
    }

    @Override
    public void onBackPressed() {
         /*
            creating alert
         */
        new LovelyStandardDialog(this, R.style.myDialog)
                .setTopColorRes(R.color.colorPrimary)
                .setButtonsColorRes(R.color.black)
                .setIcon(R.mipmap.alert)
                .setTitle(R.string.sure_to_leave)
                .setMessage(R.string.leave_msg)
                .setPositiveButton(R.string.yes, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        EventBus.getDefault().post(new SessionEndEvent(true));
                        finish();
                    }
                })
                .show();
    }

}
