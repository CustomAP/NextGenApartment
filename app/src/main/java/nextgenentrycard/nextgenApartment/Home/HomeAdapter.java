package nextgenentrycard.nextgenApartment.Home;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import nextgenentrycard.nextgenApartment.DailyVisitor.DailyVisitorActivity;
import nextgenentrycard.nextgenApartment.Models.HomeItem;
import nextgenentrycard.nextgenApartment.Models.NewVisitorItem;
import nextgenentrycard.nextgenApartment.Parking.ParkingActivity;
import nextgenentrycard.nextgenApartment.PreBook.PreBookActivity;
import nextgenentrycard.nextgenApartment.R;
import nextgenentrycard.nextgenApartment.VisitorDetails.VisitorDetailsActivity;


/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 26/1/18.
 */

public class HomeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<HomeItem> homeItems;
    private Context context;

    public HomeAdapter(ArrayList<HomeItem> homeItems){
        this.homeItems = homeItems;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_single_item, parent, false);
        return new HomeHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final HomeHolder homeHolder = (HomeHolder)holder;
        context = homeHolder.ivOptionName.getContext();

        homeHolder.ivOptionName.setImageResource(homeItems.get(position).getImage());
        homeHolder.tvOptionName.setText(homeItems.get(position).getOptionName());

        homeHolder.llHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (homeHolder.getAdapterPosition()){
                    case 0:
                        Intent i1 = new Intent(context, VisitorDetailsActivity.class);
                        i1.putExtra("NewVisitorItem", new NewVisitorItem());
                        i1.putExtra("origin", "NewVisitor");
                        context.startActivity(i1);
                        break;
                    case  1:
                        Intent i = new Intent(context, DailyVisitorActivity.class);
                        context.startActivity(i);
                        break;
                    case 2:
                        i = new Intent(context, PreBookActivity.class);
                        context.startActivity(i);
                        break;
                    case 3:
                        i = new Intent(context, ParkingActivity.class);
                        context.startActivity(i);
                        break;
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return homeItems.size();
    }

    class HomeHolder extends RecyclerView.ViewHolder {
        TextView tvOptionName;
        ImageView ivOptionName;
        LinearLayout llHome;

        public HomeHolder(View itemView) {
            super(itemView);
            tvOptionName = (TextView) itemView.findViewById(R.id.tvOptionName_homeSingleItem);
            ivOptionName = (ImageView) itemView.findViewById(R.id.ivHome_homeSingleItem);
            llHome = (LinearLayout)itemView.findViewById(R.id.llHomeSingleItem);
        }
    }

}
