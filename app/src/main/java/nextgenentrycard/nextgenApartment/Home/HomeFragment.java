package nextgenentrycard.nextgenApartment.Home;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import nextgenentrycard.nextgenApartment.Models.HomeItem;
import nextgenentrycard.nextgenApartment.R;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 28/1/18.
 */

public class HomeFragment extends Fragment {

    RecyclerView rvHome;

    ArrayList<HomeItem> homeItems;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rvHome = (RecyclerView) view.findViewById(R.id.rvHomeFragment);

        homeItems = new ArrayList<>();
        fillHomeItems();

        HomeAdapter homeAdapter = new HomeAdapter(homeItems);
        rvHome.setAdapter(homeAdapter);
        rvHome.setLayoutManager(new GridLayoutManager(getContext(), 2));
        rvHome.setItemAnimator(new DefaultItemAnimator());
        rvHome.setNestedScrollingEnabled(false);
    }

    private void fillHomeItems() {
        homeItems.add(new HomeItem(getResources().getString(R.string.new_visitor),R.drawable.new_visitor));
        homeItems.add(new HomeItem("Daily Service Provider",R.drawable.daily_visitor));
        homeItems.add(new HomeItem(getResources().getString(R.string.pre_booked_visitor),R.drawable.prebook_visitor));
        homeItems.add(new HomeItem(getResources().getString(R.string.parking),R.drawable.parking_management));
    }
}
