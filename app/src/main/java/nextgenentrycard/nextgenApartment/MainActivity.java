package nextgenentrycard.nextgenApartment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;

import nextgenentrycard.nextgenApartment.DailyVisitor.DailyVisitorFragment;
import nextgenentrycard.nextgenApartment.Database.DBHelper;
import nextgenentrycard.nextgenApartment.Models.DSPItem;
import nextgenentrycard.nextgenApartment.Models.NewVisitorItem;
import nextgenentrycard.nextgenApartment.OTP.OTPFragment;
import nextgenentrycard.nextgenApartment.PreBook.PreBookFetchServerConnection;
import nextgenentrycard.nextgenApartment.PreBook.PreBookedVisitorListFragment;
import nextgenentrycard.nextgenApartment.Sync.Sync;
import nextgenentrycard.nextgenApartment.TodaysGuests.TodaysGuestFragment;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    private boolean exit = false;
    Handler mHandler = new Handler();
    final int INTERVAL = 1000 * 30;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*
            Drawer
         */
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        /*
            Navigation view
         */
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        setTitle("New Visitor");

        /*
            creating OTP fragment and passing blank newvisitor arguments, origin
         */
        OTPFragment otpFragment = new OTPFragment();
        NewVisitorItem newVisitorItem = new NewVisitorItem();

        Bundle args = new Bundle();
        args.putSerializable("NewVisitorItem", newVisitorItem);
        args.putString("origin", "NewVisitor");
        otpFragment.setArguments(args);

        /*
            adding otp fragment to activity
         */
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.main_container, otpFragment);
        fragmentTransaction.commit();

        Thread wait = new Thread() {
            public void run() {
                try {
                    sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }finally {

                    mHandlerTask.run();

                    deleteOldGuests.run();
                }
            }
        };

        wait.start();

        //new StartServiceAsync().execute("");
    }

    @Override
    public void onBackPressed() {
        /*
            closing the drawer if open
         */
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (exit) {
            /*
                finish activity
             */
                finish();
            } else {
                Toast.makeText(getApplicationContext(), "Press Back again to exit", Toast.LENGTH_SHORT).show();
                exit = true;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        exit = false;
                    }
                }, 2300);
            }
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_new_visitor) {
            setTitle("New Visitor");
            /*
                passing blank new visitor model and origin
             */
            NewVisitorItem newVisitorItem = new NewVisitorItem();
            Bundle args = new Bundle();
            args.putSerializable("NewVisitorItem", newVisitorItem);
            args.putString("origin", "NewVisitor");

            OTPFragment otpFragment = new OTPFragment();
            otpFragment.setArguments(args);
            replaceFragment(otpFragment);

        } else if (id == R.id.nav_daily_visitor) {
            setTitle("Daily Visitor");
            replaceFragment(new DailyVisitorFragment());

        } else if (id == R.id.nav_prebooked_visitor) {
            setTitle("Pre-Booked Visitor");
            replaceFragment(new PreBookedVisitorListFragment());

        } else if (id == R.id.nav_todays_guests) {
            setTitle("Today's Guests");
            replaceFragment(new TodaysGuestFragment());

        } else if (id == R.id.nav_add_daily_visitor) {
            setTitle("Add Daily Visitor");
            /*
                passing blank daily visitor model and origin
             */
            DSPItem DSPItem = new DSPItem();
            Bundle args = new Bundle();
            args.putSerializable("DSPItem", DSPItem);
            args.putString("origin", "DailyVisitor");

            OTPFragment otpFragment = new OTPFragment();
            otpFragment.setArguments(args);
            replaceFragment(otpFragment);

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private void replaceFragment(Fragment fragment) {
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.main_container, fragment);
        fragmentTransaction.commit();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mHandler.removeCallbacks(mHandlerTask);
    }

    Runnable mHandlerTask = new Runnable() {
        @Override
        public void run() {
            /*
                Fetching prebook visitors, employees, daily visitors
                Updating new visitors, daily visitors
             */

            PreBookFetchServerConnection preBookFetchServerConnection = new PreBookFetchServerConnection(getApplicationContext());
            preBookFetchServerConnection.fetchPreBookVisitorList();

            Sync sync=new Sync(getApplicationContext());
            sync.getResidents();
            sync.getNewVisitors();
            sync.getDSPs();
            sync.updateDSPLog();

            mHandler.postDelayed(mHandlerTask, INTERVAL);
        }
    };


    Thread deleteOldGuests = new Thread() {
        public void run() {
            Date date = new Date();

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String dateString = simpleDateFormat.format(date);

            Log.d("date", dateString);

            DBHelper dbHelper = new DBHelper(getApplicationContext());
            dbHelper.deleteOldGuests(dateString);
        }
    };

    public class StartServiceAsync extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(final String... params) {
            // starting service
            return null;
        }

        @Override
        protected void onPostExecute(final String result) {
            super.onPostExecute(result);
        }
    }
}
