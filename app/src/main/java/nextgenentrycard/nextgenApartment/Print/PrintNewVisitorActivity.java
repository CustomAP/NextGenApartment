package nextgenentrycard.nextgenApartment.Print;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.print.PrintHelper;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import de.greenrobot.event.EventBus;
import nextgenentrycard.nextgenApartment.EventBus.SessionEndEvent;
import nextgenentrycard.nextgenApartment.Models.NewVisitorItem;
import nextgenentrycard.nextgenApartment.R;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 12/12/17.
 */

public class PrintNewVisitorActivity extends AppCompatActivity{

    TextView tvCompanyName, tvSecurityName, tvDate, tvTime, tvVisitorName, tvMobileNum, tvOrganisation, tvPersonToMeet, tvWingFlatNum;
    LinearLayout llPrintNewVisitor;
    ImageView ivPhoto;
    NewVisitorItem newVisitorItem;
    Uri image;
    Bitmap receipt;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_print_new_visitor_details);

        initialize();
    }

    private void initialize() {
        tvCompanyName= (TextView) findViewById(R.id.tvCompanyName_PrintNewVisitor);
        tvSecurityName = (TextView)findViewById(R.id.tvSecurityName_PrintNewVisitor);
        tvDate = (TextView)findViewById(R.id.tvDate_PrintNewVisitor);
        tvTime = (TextView)findViewById(R.id.tvTime_PrintNewVisitor);
        tvVisitorName = (TextView)findViewById(R.id.tvVisitorName_PrintNewVisitor);
        tvMobileNum = (TextView)findViewById(R.id.tvMobileNumber_PrintNewVisitor);
        tvOrganisation = (TextView)findViewById(R.id.tvOrganisationName_PrintNewVisitor);
        tvPersonToMeet = (TextView)findViewById(R.id.tvPersonToMeet_PrintNewVisitor);
        tvWingFlatNum = (TextView)findViewById(R.id.tvWingFlatNum_PrintNewVisitor);
        llPrintNewVisitor= (LinearLayout)findViewById(R.id.llPrintNewVisitor);
        ivPhoto = (ImageView)findViewById(R.id.ivPhoto_PrintNewVisitor);

        newVisitorItem = (NewVisitorItem) getIntent().getSerializableExtra("NewVisitor");
        image = getIntent().getParcelableExtra("Photo");

        SharedPreferences preferences = getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);

        tvCompanyName.setText(preferences.getString("apartmentName",null));
        tvSecurityName.setText(preferences.getString("securityName",null));
        tvDate.setText(newVisitorItem.getDate());
        tvTime.setText(newVisitorItem.getInTime());
        tvVisitorName.setText(newVisitorItem.getName());
        tvMobileNum.setText(newVisitorItem.getMobNum());
        tvOrganisation.setText(newVisitorItem.getOrganisation());
        tvPersonToMeet.setText(newVisitorItem.getResidentName());
        tvWingFlatNum.setText(newVisitorItem.getWing() + " " + newVisitorItem.getFlatNum());

        Glide.with(this).load(image).asBitmap().centerCrop().diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true).into(ivPhoto);


        EventBus.getDefault().post(new SessionEndEvent(true));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.print_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_print:
                File file = saveBitMap(getApplicationContext(), llPrintNewVisitor,newVisitorItem.getVisitorID()+"");    //which view you want to pass that view as parameter
                if (file != null) {
                    Log.i("TAG", "Drawing saved to the gallery!");
                    PrintHelper printHelper = new PrintHelper(PrintNewVisitorActivity.this);
                    printHelper.setScaleMode(PrintHelper.SCALE_MODE_FIT);
                    printHelper.printBitmap("Receipt"+newVisitorItem.getVisitorID(),receipt);
                } else {
                    Log.i("TAG", "Oops! Image could not be saved.");
                }
                break;
        }
        return true;
    }

    private File saveBitMap(Context context, View drawView,String fileName){
        File direct = new File(Environment.getExternalStorageDirectory() + "/DirName");

        if (!direct.exists()) {
            File wallpaperDirectory = new File(Environment.getExternalStorageDirectory().getPath() + "/NextGen/Receipts/");
            wallpaperDirectory.mkdirs();
        }

        File pictureFile = new File(new File(Environment.getExternalStorageDirectory().getPath() + "/NextGen/Receipts/"), fileName + ".jpg");

        if (pictureFile.exists()) {
            pictureFile.delete();
        }

        receipt =getBitmapFromView(drawView);

        try {
            pictureFile.createNewFile();
            FileOutputStream oStream = new FileOutputStream(pictureFile);
            receipt.compress(Bitmap.CompressFormat.PNG, 100, oStream);
            oStream.flush();
            oStream.close();
        } catch (IOException e) {
            e.printStackTrace();
            Log.i("TAG", "There was an issue saving the image.");
        }

        scanGallery( context,pictureFile.getAbsolutePath());
        return pictureFile;
    }

    /*
    create bitmap from view and returns it
     */
    private Bitmap getBitmapFromView(View view) {
        /*
        Define a bitmap with the same size as the view
         */
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(),Bitmap.Config.ARGB_8888);
        /*
        Bind a canvas to it
         */
        Canvas canvas = new Canvas(returnedBitmap);
        /*
        Get the view's background
         */
        Drawable bgDrawable =view.getBackground();
        if (bgDrawable!=null) {
            /*
            has background drawable, then draw it on the canvas
             */
            bgDrawable.draw(canvas);
        }   else{
            /*
            does not have background drawable, then draw white background on the canvas
             */
            canvas.drawColor(Color.WHITE);
        }
        /*
         draw the view on the canvas
          */
        view.draw(canvas);
        /*
        return the bitmap
         */
        return returnedBitmap;
    }
    /*
     used for scanning gallery
      */
    private void scanGallery(Context cntx, String path) {
        try {
            MediaScannerConnection.scanFile(cntx, new String[] { path },null, new MediaScannerConnection.OnScanCompletedListener() {
                public void onScanCompleted(String path, Uri uri) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
