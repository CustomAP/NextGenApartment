package nextgenentrycard.nextgenApartment.TodaysGuests;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import nextgenentrycard.nextgenApartment.Models.NewVisitorItem;
import nextgenentrycard.nextgenApartment.PicDailog;
import nextgenentrycard.nextgenApartment.R;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 24/10/17.
 */

public class ViewVisitorDetailsActivity extends AppCompatActivity {
    TextView tvVisitorName, tvPurposeOfMeeting,tvInTime, tvOutTime, tvOrganisation, tvEmployeeName, tvWingFlatNum, tvApartmentName, tvSecurityName;
    ImageView ivPhoto;
    ImageButton bOkay;
    NewVisitorItem newVisitorItem;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_new_visitors);
        setTitle("Visitor Details");

        /*
            initializing views
         */
        tvVisitorName=(TextView)findViewById(R.id.tvName_viewVisitor);
//        tvVisitorType =(TextView)findViewById(R.id.tvGuestType_viewVisitor);
        tvPurposeOfMeeting=(TextView)findViewById(R.id.tvPurposeOfMeet_viewVisitor);
//        tvIDProof=(TextView)findViewById(R.id.tvIDProof_viewVisitor);
//        tvIDNum=(TextView)findViewById(R.id.tvIDNum_viewVisitor);
        ivPhoto=(ImageView) findViewById(R.id.ivPhoto_viewVisitor);
        bOkay=(ImageButton) findViewById(R.id.bOkay_viewVisitor);
        tvInTime=(TextView)findViewById(R.id.tvInTime_viewVisitor);
        tvOutTime=(TextView)findViewById(R.id.tvOutTime_viewVisitor);
        tvOrganisation=(TextView)findViewById(R.id.tvOrganisationName_viewVisitor);
        tvEmployeeName = (TextView)findViewById(R.id.tvEmployeeName_viewVisitor);
        tvWingFlatNum = (TextView)findViewById(R.id.tvWingFlatNum_viewVisitor);
        tvSecurityName = (TextView)findViewById(R.id.tvSecurityName_viewVisitor);
        tvApartmentName = (TextView)findViewById(R.id.tvApartment_viewVisitor);

        newVisitorItem= (NewVisitorItem) getIntent().getSerializableExtra("newVisitor");


        SharedPreferences sharedPreferences = getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        tvSecurityName.setText(sharedPreferences.getString("securityName", ""));
        tvApartmentName.setText(sharedPreferences.getString("apartmentName", ""));

        /*
            setting values
         */
        tvVisitorName.setText(newVisitorItem.getName());
//        tvVisitorType.setText(newVisitorItem.getType());
        tvPurposeOfMeeting.setText(newVisitorItem.getPurposeOfMeet());
//        tvIDProof.setText(newVisitorItem.getGovtID());
//        tvIDNum.setText(newVisitorItem.getIDNum());
        tvOrganisation.setText(newVisitorItem.getOrganisation());
        tvInTime.setText(newVisitorItem.getInTime());
        tvEmployeeName.setText(newVisitorItem.getResidentName());
        tvWingFlatNum.setText(newVisitorItem.getWing()+" "+newVisitorItem.getFlatNum());

        if(newVisitorItem.getOutTime()!=null)
            tvOutTime.setText(newVisitorItem.getOutTime());

        bOkay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        loadImageFromStorage();

        ivPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), PicDailog.class);
//                i.putExtra("path",Environment.getExternalStorageDirectory().getPath()+"/NextGen/NewVisitor/");
//                i.putExtra("fileName",newVisitorItem.getVisitorID()+".jpg");
                i.putExtra("origin","newVisitor");
                i.putExtra("newVisitorID",newVisitorItem.getVisitorID());
                startActivity(i);
            }
        });

    }

    public void loadImageFromStorage(){
        try {
//            String path = Environment.getExternalStorageDirectory().getPath()+"/NextGen/NewVisitor/";
//            File f = new File(path, newVisitorItem.getVisitorID()+".jpg");
            Drawable defaultProfPic=getResources().getDrawable(R.drawable.generic_profile);
            Glide.with(this).load("http://www.accesspoint.in/newVisitors/"+newVisitorItem.getVisitorID()+".JPG").asBitmap().centerCrop().error(defaultProfPic)
                    .into(ivPhoto);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
