package nextgenentrycard.nextgenApartment.TodaysGuests;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.yarolegovich.lovelydialog.LovelyProgressDialog;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import nextgenentrycard.nextgenApartment.Database.DBHelper;
import nextgenentrycard.nextgenApartment.Models.NewVisitorItem;
import nextgenentrycard.nextgenApartment.R;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 23/10/17.
 */

public class
TodaysGuestAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<NewVisitorItem> newVisitorItems, inVisitors, outVisitors;
    private Context context;
    private AQuery aQuery;
    private String exit = "http://www.accesspoint.in/index.php/NewVisitor/exit";
    private String checkExited = "http://www.accesspoint.in/index.php/NewVisitor/checkExited";

    public TodaysGuestAdapter(ArrayList<NewVisitorItem> newVisitorItems) {
        this.newVisitorItems = newVisitorItems;
        inVisitors = new ArrayList<>();
        outVisitors = new ArrayList<>();
        aQuery = new AQuery(context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.todays_guest_single_item, parent, false);
        return new TodaysGuestHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        final TodaysGuestHolder todaysGuestHolder = (TodaysGuestHolder) holder;
        context = todaysGuestHolder.bExit.getContext();

        /*
            setting texts
         */
        todaysGuestHolder.tvVisitorName.setText(newVisitorItems.get(position).getName());
        todaysGuestHolder.tvinTime.setText(newVisitorItems.get(position).getInTime());

        /*
            setting exit button, outtime and intime text view visibilities
         */
        if (newVisitorItems.get(todaysGuestHolder.getAdapterPosition()).getOutTime() == null) {
            todaysGuestHolder.bExit.setVisibility(View.VISIBLE);
            todaysGuestHolder.tvOutTime.setVisibility(View.INVISIBLE);
        } else {
            todaysGuestHolder.bExit.setVisibility(View.GONE);
            todaysGuestHolder.tvOutTime.setVisibility(View.VISIBLE);
            todaysGuestHolder.tvOutTime.setText(newVisitorItems.get(todaysGuestHolder.getAdapterPosition()).getOutTime());
        }

        todaysGuestHolder.bExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*
                    progress bar for checking if exited by employee
                 */
                final Dialog dialog = new LovelyProgressDialog(context)
                        .setIcon(R.drawable.exit)
                        .setTitle("Exit Visitor")
                        .setTopColorRes(R.color.colorPrimary)
                        .setCancelable(true)
                        .show();

                SharedPreferences preferences1 = context.getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
                final String sessionID = preferences1.getString("sessionID", null);

                if (newVisitorItems.get(todaysGuestHolder.getAdapterPosition()).getResidentID() == 0) {
                    dialog.dismiss();

                    new LovelyStandardDialog(context)
                            .setIcon(R.mipmap.alert)
                            .setTitle("Exit")
                            .setMessage("Resident does not have Next Gen app")
                            .setPositiveButtonColor(context.getResources().getColor(R.color.colorPrimary))
                            .setTopColorRes(R.color.colorPrimary)
                            .setPositiveButton("Exit", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    exitVisitor(todaysGuestHolder, todaysGuestHolder.getAdapterPosition());
                                }
                            })
                            .setNegativeButton("Cancel", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                }
                            })
                            .show();
                } else {


                    HashMap<String, Object> checkExitParams = new HashMap<String, Object>();
                    checkExitParams.put("sessionID", sessionID);
                    checkExitParams.put("visitorID", newVisitorItems.get(todaysGuestHolder.getAdapterPosition()).getVisitorID());

                    aQuery.ajax(checkExited, checkExitParams, JSONObject.class, new AjaxCallback<JSONObject>() {
                        @Override
                        public void callback(String url, JSONObject object, AjaxStatus status) {
                            int result = 0;
                            String exitTime = null;

                            if (object != null) {
                                try {
                                    Log.d("checkExit", "object != null");
                                    result = object.getInt("exited");
                                    exitTime = object.getString("exitByUser");

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                if (status.getCode() == 200) {
                                    dialog.dismiss();
                                    Log.d("check exit", "result = " + result);
                                    if (result == 1) {
                                        /*
                                            Exited by employee and now
                                            sending exit request to server
                                        */


                                        if(exitTime != null){
                                            exitTime = exitTime.substring(exitTime.indexOf(" ")+1);
                                        }

                                        new LovelyStandardDialog(context)
                                                .setIcon(R.mipmap.alert)
                                                .setTitle("Exit")
                                                .setMessage("Resident exited the visitor at " + exitTime)
                                                .setPositiveButtonColor(context.getResources().getColor(R.color.colorPrimary))
                                                .setTopColorRes(R.color.colorPrimary)
                                                .setPositiveButton("Exit", new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        exitVisitor(todaysGuestHolder, todaysGuestHolder.getAdapterPosition());
                                                    }
                                                })
                                                .setNegativeButton("Cancel", new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {

                                                    }
                                                })
                                                .show();
                                    } else {
                                        /*
                                            Not exited by employee yet
                                        */
                                        new LovelyStandardDialog(context)
                                                .setIcon(R.mipmap.alert)
                                                .setTitle("Exit")
                                                .setMessage("Visitor not yet exited by the Resident")
                                                .setPositiveButtonColor(context.getResources().getColor(R.color.colorPrimary))
                                                .setTopColorRes(R.color.colorPrimary)
                                                .setPositiveButton("Exit Anyway", new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        exitVisitor(todaysGuestHolder, todaysGuestHolder.getAdapterPosition());
                                                    }
                                                })
                                                .setNegativeButton("Cancel", new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {

                                                    }
                                                })
                                                .show();
                                    }
                                }
                            } else {
                                dialog.dismiss();
                                Log.d("checkExit", "object = null, status :" + status.getCode());
                                Toast.makeText(context, "Failed to exit", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                }

            }
        });

        todaysGuestHolder.llTodaysGuest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, ViewVisitorDetailsActivity.class);
                i.putExtra("newVisitor", newVisitorItems.get(todaysGuestHolder.getAdapterPosition()));
                context.startActivity(i);
            }
        });

        try {
//            String path = Environment.getExternalStorageDirectory().getPath() + "/NextGen/NewVisitor/";
//            File f = new File(path, newVisitorItems.get(position).getVisitorID() + ".jpg");
            Drawable defaultProfPic = context.getResources().getDrawable(R.drawable.generic_profile);
            Glide.with(context).load("http://www.accesspoint.in/newVisitors/"+newVisitorItems.get(position).getVisitorID()+".JPG").asBitmap().centerCrop().error(defaultProfPic)
                    .into(new BitmapImageViewTarget(todaysGuestHolder.ivPhoto) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    todaysGuestHolder.ivPhoto.setImageDrawable(circularBitmapDrawable);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void exitVisitor(final RecyclerView.ViewHolder holder, int position) {
        final TodaysGuestHolder todaysGuestHolder = (TodaysGuestHolder) holder;

        SharedPreferences preferences1 = context.getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        final String sessionID = preferences1.getString("sessionID", null);


        HashMap<String, Object> exitParams = new HashMap<>();
        exitParams.put("sessionID", sessionID);
        exitParams.put("visitorID", newVisitorItems.get(todaysGuestHolder.getAdapterPosition()).getVisitorID());

        aQuery.ajax(exit, exitParams, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                String outTime = null;
                if (object != null) {
                    Log.d("exit", "object!=null");
                    try {
                        outTime = object.getString("outTime");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.d("exit", "object=null");
                    Toast.makeText(context, "Failed to send Exit request", Toast.LENGTH_SHORT).show();
                }

                if (status.getCode() == 200) {
                    /*
                      updating local cb
                        */
                    DBHelper helper = new DBHelper(context);
                    helper.setOutTime(newVisitorItems.get(todaysGuestHolder.getAdapterPosition()).getVisitorID(), outTime);
                    /*
                       removing and adding item
                     */
                    newVisitorItems.get(todaysGuestHolder.getAdapterPosition()).setOutTime(outTime);
                    removeAndAddItem(todaysGuestHolder.getAdapterPosition());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return newVisitorItems.size();
    }

    private class TodaysGuestHolder extends RecyclerView.ViewHolder {

        TextView tvVisitorName, tvinTime, tvOutTime;
        TextView bExit;
        LinearLayout llTodaysGuest;
        CircularImageView ivPhoto;

        public TodaysGuestHolder(View itemView) {
            super(itemView);
            tvVisitorName = (TextView) itemView.findViewById(R.id.tvVisitorName_todaysGuestSingleItem);
            tvinTime = (TextView) itemView.findViewById(R.id.tvInTime_todaysGuestSingleItem);
            bExit = (TextView) itemView.findViewById(R.id.bExit_todaysGuestSingleItem);
            tvOutTime = (TextView) itemView.findViewById(R.id.tvOutTime_todaysGuestSingleItem);
            llTodaysGuest = (LinearLayout) itemView.findViewById(R.id.llTodaysGuestItem);
            ivPhoto = (CircularImageView) itemView.findViewById(R.id.ivPhoto_todaysGuestSingleItem);
        }
    }

    private void removeAndAddItem(int position) {
        NewVisitorItem newVisitorItem = newVisitorItems.get(position);

        newVisitorItems.remove(position);

        notifyItemRemoved(position);

        newVisitorItems.add(newVisitorItem);

        notifyItemInserted(newVisitorItems.size());
    }
}
