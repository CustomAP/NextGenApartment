package nextgenentrycard.nextgenApartment.TodaysGuests;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Collections;

import nextgenentrycard.nextgenApartment.Database.DBHelper;
import nextgenentrycard.nextgenApartment.Models.NewVisitorItem;
import nextgenentrycard.nextgenApartment.R;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 23/10/17.
 */

public class TodaysGuestFragment extends Fragment {
    RecyclerView rvTodaysGuests;
    ArrayList<NewVisitorItem> newVisitorItems, inVisitors,outVisitors;
    CardView cvNoGuests;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_todays_visitors,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rvTodaysGuests=(RecyclerView)view.findViewById(R.id.rvTodaysGuests);
        cvNoGuests=(CardView)view.findViewById(R.id.cvNoGuests);

        /*
            getting visitors list from db
         */
        DBHelper dbHelper=new DBHelper(getContext());
        newVisitorItems=dbHelper.getAllVisitors();

        /*
            initializing arraylist
         */
        inVisitors=new ArrayList<>();
        outVisitors=new ArrayList<>();

        /*
            separating in and out visitors, sorting them and again adding them to new visitors list
         */
        seperateInAndOutVisitors();
        newVisitorItems.clear();
        sortInAndOutVisitors();
        clubInAndOutVisitors();

        if(newVisitorItems.size()!=0){
            cvNoGuests.setVisibility(View.GONE);
        }


        /*
            setting up recycler view
         */
        TodaysGuestAdapter todaysGuestAdapter=new TodaysGuestAdapter(newVisitorItems);
        rvTodaysGuests.setAdapter(todaysGuestAdapter);
        rvTodaysGuests.setItemAnimator(new DefaultItemAnimator());
        rvTodaysGuests.setLayoutManager(new LinearLayoutManager(getContext()));

    }

    private void clubInAndOutVisitors() {
        newVisitorItems=inVisitors;
        for(int i=0;i<outVisitors.size();i++)
            newVisitorItems.add(outVisitors.get(i));
    }

    private void sortInAndOutVisitors() {
        Collections.sort(inVisitors);
        Collections.sort(outVisitors);
    }

    private void seperateInAndOutVisitors(){
        for(int i=0;i<newVisitorItems.size();i++){
            if(newVisitorItems.get(i).getOutTime()==null){
                inVisitors.add(newVisitorItems.get(i));
            }else{
                outVisitors.add(newVisitorItems.get(i));
            }
        }
    }
}
