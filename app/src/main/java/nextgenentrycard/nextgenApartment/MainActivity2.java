package nextgenentrycard.nextgenApartment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import nextgenentrycard.nextgenApartment.Database.DBHelper;
import nextgenentrycard.nextgenApartment.Home.HomeFragment;
import nextgenentrycard.nextgenApartment.PreBook.PreBookFetchServerConnection;
import nextgenentrycard.nextgenApartment.Sync.Sync;
import nextgenentrycard.nextgenApartment.TodaysGuests.TodaysGuestFragment;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 28/1/18.
 */
public class MainActivity2 extends AppCompatActivity {

    private static FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    LinearLayout llNavigation;

    private Context context;
    private AQuery aQuery;

    private String sos = "http://www.accesspoint.in/index.php/Extras/sos";

    private boolean exit = false;
    Handler mHandler = new Handler();
    final int INTERVAL = 1000 * 15;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    replaceFragment(new HomeFragment());
                    setTitle("Access Point");
                    return true;
                case R.id.navigation_out:
                    replaceFragment(new TodaysGuestFragment());
                    setTitle("Out");
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        aQuery = new AQuery(this);
        context = this;

        llNavigation = (LinearLayout)findViewById(R.id.llNavigation);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.main_container2, new HomeFragment());
        fragmentTransaction.commit();


        Thread wait = new Thread() {
            public void run() {
                try {
                    sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }finally {

                    mHandlerTask.run();

                    deleteOldGuests.run();
                }
            }
        };

        wait.start();

    }


    private void replaceFragment(Fragment fragment) {
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.main_container2, fragment);
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
        if (exit) {
            /*
                finish activity
             */
            finish();
        } else {
            Snackbar.make(llNavigation, "Press Back again to exit", Snackbar.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 2300);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.menu_about) {
            Intent i = new Intent(getApplicationContext(), AboutActivity.class);
            getApplicationContext().startActivity(i);
            return true;
        } else if(item.getItemId() == R.id.action_sos){
            new LovelyStandardDialog(context)
                    .setTopColorRes(R.color.colorPrimary)
                    .setIcon(R.drawable.sos)
                    .setTitle("SOS")
                    .setMessage("Do You want to send SOS Signal?\n\tNote: The signal will be sent to all the residents")
                    .setPositiveButton("Confirm", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            SharedPreferences preferences1 = context.getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
                            String sessionID = preferences1.getString("sessionID", null);

                            HashMap<String, Object> sosParams = new HashMap<>();
                            sosParams.put("sessionID", sessionID);

                            aQuery.ajax(sos, sosParams, JSONObject.class, new AjaxCallback<JSONObject>() {
                                @Override
                                public void callback(String url, JSONObject object, AjaxStatus status) {
                                    if(object == null){
                                        Toast.makeText(getApplicationContext(), "Failed to send SOS Signal", Toast.LENGTH_SHORT).show();
                                    }
                                    if (status.getCode() == 200) {
                                        Toast.makeText(getApplicationContext(), "SOS Signal sent", Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                        }
                    })
                    .show();
        }

        return super.onOptionsItemSelected(item);
    }

    Runnable mHandlerTask = new Runnable() {
        @Override
        public void run() {
            /*
                Fetching prebook visitors, employees, daily visitors
                Updating new visitors, daily visitors
             */

            PreBookFetchServerConnection preBookFetchServerConnection = new PreBookFetchServerConnection(getApplicationContext());
            preBookFetchServerConnection.fetchPreBookVisitorList();

            Sync sync=new Sync(getApplicationContext());
            sync.getResidents();
            sync.getNewVisitors();
            sync.getDSPs();
            sync.updateDSPLog();
            sync.getParkingCounter();

            mHandler.postDelayed(mHandlerTask, INTERVAL);
        }
    };


    Thread deleteOldGuests = new Thread() {
        public void run() {
            Date date = new Date();

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String dateString = simpleDateFormat.format(date);

            Log.d("date", dateString);

            DBHelper dbHelper = new DBHelper(getApplicationContext());
            dbHelper.deleteOldGuests(dateString);
        }
    };

}
