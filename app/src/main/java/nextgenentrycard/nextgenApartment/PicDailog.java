package nextgenentrycard.nextgenApartment;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 2/11/17.
 */

public class PicDailog extends Activity {
    ImageView ivProfilePic;
//    String path,fileName;
    int newVisitorID;
    int dailyVisitorID;
    String url;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_pic);
        initialize();
    }

    private void initialize() {
        /*
            initialize views
         */
        ivProfilePic = (ImageView) findViewById(R.id.ivProfilePicActivity);

        /*
            getting path and file name
         */
        String origin = getIntent().getStringExtra("origin");
        if(origin.equals("newVisitor")) {
            newVisitorID = getIntent().getIntExtra("newVisitorID", 0);
            url = "http://www.accesspoint.in/newVisitors/"+newVisitorID+".JPG";
        }else if(origin.equals("dailyVisitor")) {
            dailyVisitorID = getIntent().getIntExtra("dailyVisitorID", 0);
            url = "http://www.accesspoint.in/dailyVisitors/"+dailyVisitorID+".JPG";
        }
//        path=getIntent().getStringExtra("path");
//        fileName=getIntent().getStringExtra("fileName");

        /*
            load image from storage
         */
        loadImageFromStorage();

    }

    public void loadImageFromStorage() {
        /*
            load image from storage using path
         */
//        File f = new File(path, fileName);

        Drawable defaultProfPic = getResources().getDrawable(R.drawable.generic_profile);
        Glide.with(this).load(url).asBitmap().error(defaultProfPic).into(ivProfilePic);
    }
}
